<ul class="nav nav-pills nav-stacked">
  <li class="active"><a href="#">Categories</a></li>
  <li><a href="item.php?cat=auto">Automobiles</a></li>
  <li><a href="item.php?cat=gadget">Gadgets</a></li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
      Educational <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
      <li><a href="#">Books</a></li>
      <li><a href="#">Projects</a></li>
      <li><a href="#">Instruments</a></li>
      <li class="divider"></li>
      <li><a href="#">Old computers</a></li>
    </ul>
  </li>
</ul>