<?php
require 'inc/Functions.php';
$obj = new Functions();
require 'inc/header.php';
if(empty($_GET['id'])){
 header("Location:index.php");
}
$id = $_GET['id'];
$item = $obj->get_bid_item($id);
$bids = $obj->bids_placed($id);
if(isset($_POST['place_bid'])){
$_SESSION['bid_id'] = $_POST['bid_id'];
$_SESSION['bid_value'] = $_POST['bid_value'];
header("Location:register.php");    
}
elseif (isset($_POST['buy_now'])) {
$_SESSION['auction_id'] =  $_POST['auction_id'];
$_SESSION['product_name'] = $_POST['product_name'];
$_SESSION['price'] = $_POST['buy_now_price'];
header("Location:paypal/index.php");
}
?>
<div class="row">
 <div class="col-md-7">
      <h3><?= $item['title'];?></h3>
      <h2> <span>Rs: <?= $item['opening_price'];?></span></h2> 
    <!-- show latest 10 items here -->     
      <img height="50%" width="50%" src="uploads/<?= $item['image'];?>">      
       <form action="" method="post">
        <input type="hidden" name="bid_id" value="<?= $item['id'];?>">
        <input type="hidden" name="bid_value" value="<?= $item['bid_value'];?>">        
        <input type="number" name="bid_value" placeholder="bid value" min="<?= $item['opening_price'] + $item['incremental_price'];?>" required>
        <input type="submit" id="place_bid" name="place_bid" value="Place bid" class="btn btn-success"> OR        
      </form>
       <form action="" method="post">
        <input type="hidden" name="auction_id" value="<?= $item['id'];?>">
        <input type="hidden" name="product_name" value="<?= $item['title'];?>">
        <input type="hidden" name="buy_now_price" value="<?= $item['buy_now_price'];?>">
         or buy it now for <?=  $item['buy_now_price'];?> rs
         <input type="submit" id="buy_now" name="buy_now" value="Buy"class="btn btn-info">
       </form>
      <span style="color: green">Enter <?= $item['opening_price'] + $item['incremental_price'];?> or more</span>&nbsp; &nbsp;
    <h1>  <span class="label label-primary label-lg">Total bids placed  : <?= $item['bids'];?></span></h1> 
    <br>     
 </div> 
 <div class="col-md-5">
    <ul class="nav nav-tabs">
  <li class="active"><a href="#desc" data-toggle="tab">Description</a></li>
  <li><a href="#bids" data-toggle="tab">Bids Placed</a></li> 
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="desc">
        <?= $item['desc'];?>
        </div>
        <div class="tab-pane fade  in" id="bids">
         <table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>Who placed</th>
      <th>Bid value</th>
      <th>When</th>      
    </tr>
  </thead>
  <tbody>
        <?php while($bid = mysql_fetch_assoc($bids)):?>        
    <tr>
      <td><?= $bid['user_name'];?></td>
      <td><?= $bid['bid_value'];?></td>
      <td><?= $bid['when'];?></td>      
    </tr>  
        <?php endwhile;?>
        </tbody>
        </table>
        </div>
    </div>
        
      <!-- timer-->
      <h1><span class="label label-danger label-lg">
     <?php
      $curr_date = strtotime(date('Y-m-d H:i:s'));     
      $ending_date = strtotime(date($item['ending_date']));
      $differenceInSeconds = $ending_date - $curr_date;    
      ?> 
      <input type="hidden" value="<?= $differenceInSeconds;?>" id= "sec" name="sec"/>
      <span id="countdown" class="timer"></span>    
      </span></h1><br><br>
      <!-- timer-->
      <!--  if item is sold -->
      <?php if($item['status'] == 'closed'):          
      $sold = $obj->sold($item['id']);
      ?>
      <div class="well">
       <?php if(!empty($sold)):?>
       <h1>Item has been sold to :</h1>
    <h3>Name: <?php  echo $sold['user_name'];?> </h3>
     <h4>Email: <?= $sold['email'];?></h4>
     <h2><span class="label label-success">Bid Value: <?= $sold['bid_value'];?>rs</span></h2>
     <?php
     else: echo "<h1>No one purchased this item</h1>";
     endif;?>
      </div>
      <?php endif;?>
      <!--  //if item is sold --->
 </div>
</div>
  
   
     
   

  
<?php include 'inc/footer.php' ;?>
