<?php
require 'inc/Functions.php';
$obj = new Functions();
require 'inc/header.php';
if(!isset($_SESSION['bid_id'])){
  header('Location:index.php');
  print_r($_SESSION);
}
if(isset($_POST['done'])){
  $array =  [
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'phone' => $_POST['phone'],
            'address' => $_POST['address'],
            'bid_id' => $_SESSION['bid_id'],
            'bid_value' => $_SESSION['bid_value']
            ];
  $place_bid = $obj->place_bid($array);
  if($place_bid == 'success'){ ?>	  
    <div class="alert alert-dismissible alert-success">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Success</strong> You successfully Placed the bid <a href="#" class="alert-link">#</a>.
</div>
  <?php } else { ?>
    <div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Oh snap!</strong> <a href="#" class="alert-link">you have already placed a bid</a> dont you dare
</div>
  <?php }
}
?>
<div class="col-md-5">
    
  <form action="" method="post">
    <label> Full Name:</label>
    <input type="text" name="username" class="form-control" required><br>
    <label>Email Address:</label>
    <input type="email" name="email" class="form-control" required><br>
    <label>phone number</label>
    <input type="text" name="phone" class="form-control" required><br>
    <label>Address with picode:</label>
    <textarea name="address" rows="3" class="form-control" required></textarea><br>    
    <input type="submit" value="Place" class="btn btn-default" name="done">
</form>  
</div>


<?php require 'inc/footer.php';?>
