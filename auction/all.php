<?php
require 'inc/Functions.php';
$obj = new Functions();
require 'inc/header.php';

$q = $obj->getAll();
while ($r = mysql_fetch_assoc($q)){ ?>
  <div class="col-md-5">
    <div class="well">
        <img class="img-circle" height="100" width="100" src="uploads/<?= $r['image'];?>">
        <h1><?= $r['title'];?></h1>
        <p><?= $r['desc'];?></p>
        <span>Price: <?= $r['opening_price'];?></span>
        <?php $newDate = date("Y-m-d / h:i A", strtotime($r['ending_date']));?>
       <h3><span class="label label-danger">Closing Time: <?= $newDate;?></span></h3>
        <a href="bid.php?id=<?= $r['id'];?>" class="btn btn-success">View</a>
    </div>
</div>
<?php }

include 'inc/footer.php';