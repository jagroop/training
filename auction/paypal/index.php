<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Auction site by jagroop singh</title>
  <link rel="stylesheet" href="http://localhost/auction/template/css/bootstrap.css">
</head>

<body>
	<?php
	session_start();
	include('../inc/header.php');?>
	<div class="col-md-8">
<div class="well">
	
    <form action="paypal.php" method="post"> <?php // remove sandbox=1 for live transactions ?>
    <input type="hidden" name="action" value="process" />
    <input type="hidden" name="cmd" value="_cart" /> <?php // use _cart for cart checkout ?>
    <input type="hidden" name="currency_code" value="USD" />
    <input type="hidden" name="invoice" value="<?= $_SESSION['auction_id'];?>" />
	<input type="hidden" name="product_name" value="<?= $_SESSION['product_name'];?>" />
	<input type="hidden" name="price" value="<?= $_SESSION['price'];?>" />
	<input type="hidden" name="product_id" value="<?= $_SESSION['auction_id'];?>" />
    <table> 
   
    <tr>
        <td><label>Payer First Name</label></td>
        <td><input type="text" name="payer_fname" class="form-control" required></td>
    </tr>
    <tr>
        <td><label>Payer Last Name</label></td>
        <td><input type="text" name="payer_lname" class="form-control" required></td>
    </tr>
    <tr>
        <td><label>Payer Address</label></td>
        <td><input type="text" name="payer_address" class="form-control" required></td>
    </tr>
    <tr>
        <td><label>Payer City</label></td>
        <td><input type="text" name="payer_city" class="form-control" required></td>
    </tr>
    <tr>
        <td><label>Payer State</label></td>
        <td><input type="text" name="payer_state" class="form-control" required></td>
    </tr>    
    <tr>
        <td><label>Payer Zip</label></td>
        <td><input type="text" name="payer_zip" class="form-control" required></td>
    </tr>
    <tr>
        <td><label>Payer Country</label></td>
        <td><input type="text" name="payer_country" class="form-control" required></td>
    </tr> 
    <tr>
        <td><label>Payer Email</label></td>
        <td><input type="email" name="payer_email" class="form-control" required></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><input type="submit" name="submit" value="Submit" class="btn btn-success" /></td>
    </tr>
    </table>
    </form>
</table>
</div>
	</div>
</body>
</html>