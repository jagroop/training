<html>
    <head>        
        <link rel="stylesheet" href="http://localhost/auction/template/css/bootstrap.css">
        <link rel="stylesheet" href="http://localhost/auction/template/css/datetime.css">
    </head>
    <body>
         
        <?php
        require '../inc/Functions.php';
		echo date('Y-m-d h:i:s');
        $obj = new Functions();
        if(isset($_POST['add_auction'])){
            
            $values = [
                       'title' => $_POST['title'],
                       'desc' =>$_POST['desc'],
                       'category' => $_POST['category'],
                       'image' => [
                                   'name' => $_FILES['image']['name'],
                                   'tmp_name' => $_FILES['image']['tmp_name'],
                                   'size' => $_FILES['image']['size']
                                   ],
                       'opening_price'=>$_POST['opening_price'],
                       'lowest_price'=>$_POST['lowest_price'],
                       'incremental_price'=>$_POST['incremental_price'],
                       'buy_now_price' => $_POST['buy_now_price'],
                       'ending_date' => $_POST['ending_date'],
                       'payment_method'=> $_POST['payment_method']
                       ];
           $foo =  $obj->add_auction($values);
           if($foo == true){
         //echo "<pre>";
         //print_r($_FILES);
         // echo "</pre>";
         $target_dir = "../uploads/";
         $target_file = $target_dir . basename($_FILES["image"]["name"]);
          if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {?>
        <div class="alert alert-dismissible alert-success">
       <button type="button" class="close" data-dismiss="alert">×</button>
       <strong>Success</strong> You successfully added an auction <a href="#" class="alert-link">good to go</a>.
       </div>
  <?php  } else {
        echo "Sorry, there was an error uploading your file.";
    }
           }
        }
        ?>
        <div class="row">
        <div class="col-md-8">
            <div class="well">
        <form id="form1" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
  <fieldset>
    <legend>Add auction</legend>
    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Auction Title</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" name="title" placeholder="Title" >
        <div id="title_error"></div>
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword" class="col-lg-2 control-label">Description</label>
      <div class="col-lg-10">
        <textarea class="form-control" name="desc" placeholder="Description" ></textarea>
        <div id="desc_error"></div>
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword" class="col-lg-2 control-label">category</label>
      <div class="col-lg-10">
        <select name="category">
			<option value="auto"> Automobiles</option>
			<option value="edu">Educational</option>
			<option value="gadget">Gadgets</option>
			<option value="app">Appliances</option>
		</select>
        <div id="desc_error"></div>
      </div>
    </div>
    <div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Image</label>
      <div class="col-lg-10">
        <input type="file" name="image">        
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-2 control-label">Price</label>
      <div class="col-lg-10">
        <div class="radio">
          <label>Openinig price (INR)
            <input type="number" name="opening_price" id="opening_price">
            <div class="help-block with-errors"></div>
            
          </label>
        </div>
        <div class="radio">
          <label>Lowest Price
            <input type="number" name="lowest_price" id="lowest_price">
           <div id="price_error"></div>
            
          </label>
        </div>
        <div class="radio">
          <label> Incremental Price
            <input type="number" name="incremental_price" id="incremental_price" required>
                       
          </label>
        </div>
        <div class="radio">
          <label>Buy Now Price
            <input type="number" name="buy_now_price" id="buy_now_price" required>
            <div class="help-block with-errors"></div>            
          </label>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="select" class="col-lg-2 control-label">Ending Date</label>
      <div class="col-lg-10">
       <input type="text" id="datetimepicker" name="ending_date" >
        <br>       
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <select name="payment_method">
              <option value="paypal">Paypal</option>
              <option value="cheque">Cheque</option>
        </select>
     
        <button type="reset" class="btn btn-default">Cancel</button>
        <button type="submit" name="add_auction" onclick="return validateForm()" class="btn btn-success">Submit</button>
      </div>
    </div>
  </fieldset>
</form>
            </div>
        </div>
        <div class="col-md-4">
            <a class="btn btn-warning" href="index.php">Cancel</a>
        </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
       <script src="http://localhost/auction/template/js/bootstrap.js"></script>
       <script src="http://localhost/auction/template/js/foo.js"></script> 
       <script src="http://localhost/auction/template/js/datetime.js"></script>
       <script>
		   jQuery('#datetimepicker').datetimepicker();
       </script>     
    </body>
</html>
