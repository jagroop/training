-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 15, 2015 at 09:52 AM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shopping`
--

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE IF NOT EXISTS `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `exp_date` date NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `code`, `exp_date`, `discount`) VALUES
(1, 'abc123', '2015-06-27', 10.00);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(80) COLLATE latin1_general_ci NOT NULL,
  `address` varchar(80) COLLATE latin1_general_ci NOT NULL,
  `phone` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `address`, `phone`) VALUES
(1, 'jagroop', 'rooplakhnia@yahoo.in', 'lakhna', '9592111609'),
(2, 'jagroop', 'yaddbhullar@yahoo.in', 'patti', '9465494945'),
(3, 'uop', 'iop@tyj.hk', 'hjkhj', '9855146099'),
(4, 'Amrit', 'amritksandhu047@gmail.com', 'lakhna', '1234567890'),
(5, 'Amrit', 'amritksandhu047@gmail.com', 'lakhna', '1234567890'),
(6, 'News', 'tanu@gmail.com', 'sdfsd', '8699342610'),
(7, 'Amrit', 'amritksandhu047@gmail.com', 'ghctfhj', '8699342610'),
(8, 'hjkh', 'rooplakhnia@yahoo.in', 'lakhna', '952111609'),
(9, 'jagroop', 'email@example.com', 'lakhna', '123456'),
(10, 'yugyu', 'ugkgyfhyu@gmail.com', 'uhygftf', '9877656'),
(11, 'yugyu', 'ugkgyfhyu@gmail.com', 'uhygftf', '9877656'),
(12, 'Kane House', 'malukymo@hotmail.com', 'Facere sit asperiores accusantium labore inventore elit, inventore ratione hic q', '+737-67-7349991'),
(13, 'Zachary Medina', 'vapiha@yahoo.com', 'Hic anim minima velit sed occaecat vel et mollitia rerum anim et fugit.', '+784-21-1829549');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `serial` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `customerid` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `payment_status` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`serial`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1799334672 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`serial`, `date`, `customerid`, `total`, `payment_status`) VALUES
(1431611372, '2015-05-18', 1, 0.00, 0),
(1799334671, '2015-05-18', 2, 0.00, 0),
(331845558, '2015-05-22', 3, 0.00, 0),
(67801239, '2015-05-26', 5, 460.00, 1),
(49107836, '2015-05-26', 6, 5956.00, 0),
(18763094, '2015-05-26', 7, 2250.00, 0),
(25693847, '2015-06-17', 8, 61500.00, 0),
(3956824, '2015-06-18', 9, 10200.00, 0),
(34819075, '2015-06-30', 10, 396.00, 0),
(56034287, '2015-06-30', 11, 396.00, 0),
(32948617, '2015-08-24', 12, 2099.00, 0),
(41893650, '2015-08-24', 13, 461.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE IF NOT EXISTS `order_detail` (
  `orderid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`orderid`, `productid`, `quantity`, `price`) VALUES
(0, 1, 1, 699),
(0, 2, 1, 396),
(1799334671, 1, 1, 699),
(1799334671, 2, 1, 396),
(331845558, 1, 1, 699),
(331845558, 2, 1, 396),
(67801239, 2, 1, 396),
(67801239, 3, 1, 64),
(49107836, 2, 2, 396),
(49107836, 3, 1, 64),
(49107836, 5, 6, 850),
(18763094, 5, 1, 850),
(18763094, 14, 1, 1400),
(25693847, 19, 41, 1500),
(3956824, 15, 4, 1800),
(3956824, 19, 2, 1500),
(34819075, 2, 1, 396),
(56034287, 2, 1, 396),
(32948617, 1, 1, 699),
(32948617, 14, 1, 1400),
(41893650, 4, 1, 461);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `serial` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `price` float NOT NULL,
  `picture` varchar(80) COLLATE latin1_general_ci NOT NULL,
  `cat` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`serial`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`serial`, `name`, `description`, `price`, `picture`, `cat`) VALUES
(1, 'Google Nexus 6', 'The Qualecomm @ snapdragon 805 quard-core processor provides lightning fast multi tasking and the Adreno 420 Gpu gives you brilliant graphics.', 699, 'images/mobile.jpg', 'gadget'),
(2, 'Ipad Air 2', 'Full HD Recording,\nWi-Fi Enabled,\nFaceTime,\n8 MP Primary Camera,\nA8X Chip with M8 Motion Co-processor,\n1.2 MP Secondary Camera,\n9.7-inch LED Touchscreen', 396, 'images/ipad.jpg', 'gadget'),
(3, 'Home Theater', ' Home Theater 4.1 with USB and FM Wired Home Audio Speaker', 64, 'images/sound.jpg', 'gadget'),
(4, 'Samsung Split AC', 'Out Door Unit Stand.\nExtra copper wire if any.\nDrain Pipe extension if any.\nPlumbing and Masonry Work.\nWiring extension from Meter to site, Power point/MCB fitting and any other electrical work,Carpentry work.', 461, 'images/ac.jpg', 'appliance'),
(5, 'Nikon DSLR Camera', '(Black, Body with AF-S DX NIKKOR 18-55mm f/3.5-5.6G VR II Lens) 2 Years Nikon India Warranty and Free Transit Insurance.', 850, 'images/camera.jpg', 'gadget'),
(6, 'Tea maker', 'Morphy Richards 1.5 Ltr - Tea Maker Silver Black', 41, 'images/teamaker.jpg', 'appliance'),
(12, 'T-shirt', 'Blue color t shirt med size cotton', 100, 'images/bi-color-polo-t-shirt-250x250.jpg', 'men'),
(15, 'Jeans', 'Blue jeans large size', 1800, 'images/Menjeansblue_23244135.jpg', 'men'),
(14, 'Shirt', 'Half sleev Check shirt', 1400, 'images/kamro-shirt-16033-225-8xl.jpg', 'men'),
(16, 'T-shirt', 'white color cotton tshirt', 500, 'images/big_sister_tshirt1.jpg', 'women'),
(17, 'Microwave abc', 'none', 20000, 'images/MW25-M619-225x225.jpg', 'appliance'),
(18, 'Panasonic Microwave', 'instant cooking less elecricity consumption', 25000, 'images/Panasonic-NE1024-Photo-1-225x225.jpg', 'appliance'),
(19, 't shirt', 'black cotton full zize', 1500, 'images/hyundai_verna.jpg', 'men');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
