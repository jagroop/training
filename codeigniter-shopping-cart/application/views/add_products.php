<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
        <link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
<style>
    #contents{
        width: 500px;
        float: inherit;
        margin: 100px;
    }
</style>
    <title>Add products</title>
</head>
<body>
<div id="contents">
<a href="<?= base_url();?>index.php/admin/index" class="btn btn-danger btn-lg"><< Exit</a>
<h1>Add products</h1>    
<?php echo form_open_multipart('admin/add_product',array('class','form-horizontal'));?>
Name of the product:<input type="text" class="form-control" name="product_name" required/><br>
Description:<input type="text" class="form-control" name="description" required/><br>
Price:<input type="text" class="form-control" name="price" required/><br/>
Category: <select name="cat">
<option value="men">Men Clothing</option>
<option value="women">Women Clothing</option>
<option value="gadget">Gadget</option>
<option value="appliance">Appliance</option>
</select><br><br><br>
Product Image:<input type="file" name="userfile" size="20" required/>
<br /><br />
<input type="submit" value="Add product" class="btn btn-success" />
<?= form_close();?>
<?= anchor('admin/index','<< cancel',array('class'=>'btn btn-warning'));?>
    </div>
        
</body>
</html>