<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
		"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>shopping cart - jagroop singh</title>
	<link rel="stylesheet" href="<?php echo base_url();?>css/style.css" />
<style>
	#cat {
		float: left;
		width: 300px;
		height: 100%;
	}
	#options {
		float: right;
	}
</style>
	
</head>
<body>
	<div id="options">
		
	<a class="btn btn-primary" href="<?= base_url();?>index.php/cart/viewcart">
		<i class="glyphicon glyphicon-shopping-cart"></i> <span class="badge">Proceed to Checkout (<?= $this->cart->total_items();?>)</span>
	</a>
	<?php echo anchor('cart/destroy','Clear Cart',array('class'=>'btn btn-danger btn-sm glyphicon glyphicon-trash','onclick'=>"return confirm('you really wanna do that');"));
		   
		 ?>
		
	</div>
	<h1>Shopping cart</h1>
	<div id="cat">
		<ul class="nav nav-pills nav-stacked">
  <li class="active"><a href="#">Categories</a></li>
  <li><a href="<?= base_url();?>index.php/cart/get_products/men">Men clothing</a></li>
  <li><a href="<?= base_url();?>index.php/cart/get_products/women">Women clothing</a></li>
  <li><a href="<?= base_url();?>index.php/cart/get_products/appliance">Appliances</a></li>
  <li><a href="<?= base_url();?>index.php/cart/get_products/gadget">Gadgets</a></li>
  
</ul>
	</div>
	<?php if(isset($products)):?>
			<?php foreach($products as $product):?>
			<div class="well" style="float: left;width: 200px;margin: 18px; display: inline-block;">
				<img src="<?php echo base_url();?><?= $product['picture'];?>">
				<h2><a href="<?= base_url();?>index.php/cart/view_product/<?= $product['serial'] ;?>"><?= $product['name'];?></a></h2>											
				<strong>Price:<?= $product['price'];?></strong>
				<?php echo form_open('cart/add');?>
				<input type="hidden" name="id" value="<?= $product['serial'] ;?>" >				
				<input type="hidden" name="name" value="<?php echo $product['name'];?>" />
				<input type="hidden" name="desc" value="<?php echo $product['description'];?>"  />
				<input type="hidden" name="img" value="<?php echo $product['picture'];?>" />				
				<input type="hidden" name="price" value="<?php echo $product['price'];?>" />				
				<input type="submit" class="btn btn-info btn-xs" value="add" />				
				<?= form_close();?>	
			</div>
			<?php endforeach;?>
<?php endif;?>
<?php if(isset($items)):?>
<?php foreach($items as $foo):?>
			<div class="well" style="float: left;width: 200px;margin: 18px">
				<img src="<?php echo base_url();?><?= $foo['picture'];?>">
				<h2><?= $foo['name'];?></h2>								
				<strong>Price:<?= $foo['price'];?></strong>
				<?php echo form_open('cart/add');?>
				<input type="hidden" name="id" value="<?= $foo['serial'] ;?>" >				
				<input type="hidden" name="name" value="<?php echo $foo['name'];?>" />
				<input type="hidden" name="desc" value="<?php echo $foo['description'];?>"  />
				<input type="hidden" name="img" value="<?php echo $foo['picture'];?>" />				
				<input type="hidden" name="price" value="<?php echo $foo['price'];?>" />
				<input type="hidden" name="cat" value="<?php echo $foo['cat'];?>" />
				<input type="submit" class="btn btn-info btn-xs" value="add" />				
				<?= form_close();?>	
			</div>
			<?php endforeach;?>
<?php endif;?>
</body>
</html>