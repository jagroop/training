<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
    <style>
        .container{
            margin: 50px;
        }
        ::-webkit-input-placeholder {
            color: red;
         }
         
         :-moz-placeholder { /* Firefox 18- */
            color: red;  
         }
         
         ::-moz-placeholder {  /* Firefox 19+ */
            color: red;  
         }
         
         :-ms-input-placeholder {  
            color: red;  
         }
    </style>
</head>
<body>    
<div class="container">
    <div class=".col-md-6">
<?= form_open('cart/placeOrder');?>
<?php echo anchor('cart/index','<< continue shoping',array('class'=>'btn btn-default'));?>
<h1>User details</h1>
 <label for="inputEmail" class="col-lg-2 control-label">Name</label>
<input type="text" class="form-control" name="name" placeholder="you name" required/><br>
Email:<input type="email" class="form-control" name="email" placeholder="example@mail.com" /><br>
Phone:<input type="text" class="form-control" name="phone" placeholder=" entr ur phone number here" required/><br>
Address:<textarea class="form-control" rows="3" placeholder="Address" name="address" required></textarea><br>
<h2>Payment Methods</h2>
<input type="submit" class="btn btn-default" name="action" value="Pay with paypal">
OR
<input type="submit" class="btn btn-info" name="action" value="Cash on Delivery" />
    </div>
</div>
</form>
<?= form_open('cart/discount');?>
Apply coupon: <input type="text" name="c_code" onblur="myFunction(this.value)" />
<input type="button" class="btn btn-success btn-sm" value="check" />
<div id="txtHint">
    
</div>
<?php ?>

<?= form_close();?>

<script>
function myFunction(str) {
     if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("POST","http://localhost/ci_cart/index.php/cart/discount/"+str,true);
        xmlhttp.send();
    } 
}
</script>
</body>
</html>
