<link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
<button class="btn btn-primary" type="button">
	
  <i class="glyphicon glyphicon-shopping-cart"></i> <span class="badge"><?= $this->cart->total_items();?></span>
</button>
<button class="btn btn-default" type="button">
	
  <i class="glyphicon glyphicon-usd"></i> <span class="badge"><?= $this->cart->total();?> Rs</span>
</button>
<?php echo anchor('cart/destroy','Clear Cart',array('class'=>'btn btn-danger btn-sm glyphicon glyphicon-trash','onclick'=>"return confirm('you really wanna do that');"));
		   
		 ?>
<br>
  <?php
  
	if($this->cart->contents())
	 { ?>
	 <h1>List of items</h1>				
	 <?php
	 foreach($this->cart->contents() as $item) { ?>
		<div class="col-md-2">
			<?= anchor('cart/removeitem/'.$item['rowid'],'X');?>			
			Name:<?= $item['name'];?><br>			
			Price(INR)<?= $item['price'];?><br>
			<?= form_open('cart/updateItem');?>
			Quantity:<input type="text" onload="foo()" name="qty" value="<?= $item['qty'];?>" /><br>
			<input type="hidden" name="rowid"  value="<?= $item['rowid']?>" />
			<input id="update_item" type="submit" value="update item" />
	<?= form_close();?>
		</div>
	<?php }?>
	
       <?php
	   echo anchor('cart/loadplaceOrder','Place order',array('class'=>'btn btn-warning glyphicon glyphicon-plane'));
	 echo anchor('cart/index','<< continue shoping',array('class'=>'btn btn-default'));
	 }
	 else{
	  redirect('cart/index');
	 }
	 
	 ?>	 