<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
    <title>orders</title>
</head>
<body>
    <h1>orders</h1>
    <a href="<?= base_url();?>index.php/admin/index" class="btn btn-danger btn-lg"><< Exit</a>
    <table class="table table-striped table-hover ">
        <tr>
           <th>Coustomer id</th>
           <th>Name</th>
           <th>Phone</th>
           <th>Email</th>
           <th>Address</th>
           <th>Total price</th>
           <th>Payment Status</th>
        </tr>
        <?php foreach($orders as $order):?>
        <tr>
            <td><?= $order['id']; ?></td>
            <td><?= $order['name'];?></td>
            <td><?= $order['phone'];?></td>
            <td><?= $order['email'];?></td>
            <td><?= $order['address'];?></td>
            <td><?= $order['total'];?></td>
            <td>
                <?php
                if($order['payment_status'] == 0){ ?>
                    <span class="label label-danger">Pending</span>
              <?php  }
              else if($order['payment_status'] == 1){ ?>
                <span class="label label-success">Paid</span>
                
            <?php  }
                ?>
              
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>