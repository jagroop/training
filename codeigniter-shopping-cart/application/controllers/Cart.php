<?php
class Cart extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->library('cart');
		$this->load->helper('string');        
    }
    public function index(){
       $records['products'] = $this->data_model->getAll();
        $this->load->view('mycart',$records);
    }
	public function get_products(){
		$cat = $this->uri->segment(3);
		$records['items'] = $this->data_model->get_products($cat);
		$this->load->view('mycart',$records);
	}
    public function add(){
        
        $data = array(
                      'id' => $this->input->post('id'),
                      'name' => $this->input->post('name'),
                      'qty'=> 1,
                      'price'=>$this->input->post('price')                     
                      );
        $this->cart->insert($data);	
		$cat = $this->input->post('cat');
		header('Location: '.base_url().'index.php/cart/get_products/'.$cat);
    }
    public function viewcart(){
        $this->load->view('viewcart.php');       
    }
	public function view_product(){
		$id = $this->uri->segment(3);
		$data['product'] = $this->data_model->get_product($id);
		$this->load->view('view_product',$data);
	}
    function destroy(){
        $this->cart->destroy();
        $this->index();
    }
    function removeitem(){
        $rowid = $this->uri->segment(3);
        $array = array(
                       'rowid' => $rowid,
                       'qty' =>0
                       );
        $this->cart->update($array);
        $this->viewcart();
    }
    function updateItem(){
        $update = array(
          'rowid' => $this->input->post('rowid'),
          'qty' =>$this->input->post('qty')
        );
        $this->cart->update($update);
        $this->viewcart();
    }
    function loadplaceOrder(){
        $this->load->view('placeorder');

    }
	function foo(){
		$user = array(
          'name' => $this->input->post('name'),
          'email' => $this->input->post('email'),
          'phone' => $this->input->post('phone'),
          'address' => $this->input->post('address')
          );
        //add customer
       $cid = $this->data_model->add_customer($user);
      $serial = random_string('numeric',8);
      $this->session->set_userdata('order_id',$serial);
      $order = array(
        'serial' => $serial,
        'date'=> date('Y-m-d'),
        'customerid' => $cid,
		'total' => $this->cart->total()
      );
      //add order
      $oid = $this->data_model->add_order($order);
      
    if($cart = $this->cart->contents()){
        
        foreach($cart as $item):
        if(isset($_SESSION['discount'])){
			 $order_details = array(
          'orderid' => $oid,
          'productid'=>$item['id'],
          'quantity'=>$item['qty'],
          'price'=>$item['price'] - $_SESSION['discount']
        );
		}
		else {
			 $order_details = array(
          'orderid' => $oid,
          'productid'=>$item['id'],
          'quantity'=>$item['qty'],
          'price'=>$item['price']
        );
		}
        $order_details = array(
          'orderid' => $oid,
          'productid'=>$item['id'],
          'quantity'=>$item['qty'],
          'price'=>$item['price']
        );
        //insert order details
        $this->data_model->insert_order_details($order_details);
        endforeach;
        
      }
	}
    function placeOrder(){
		$action = $this->input->post('action');
		if($action == 'Pay with paypal'){
	
			$this->foo();
			redirect('payment/do_purchase');
		}
		elseif($action == 'Cash on Delivery'){
			
			$this->foo();
			$this->load->view('billing_success');
		}       
        
    }
    public function discount(){
		$code = $this->uri->segment(3);
		$c_code['details'] = $this->data_model->discount($code);
		if(!empty($c_code['details'])){
			foreach($c_code['details'] as $record ){
			$this->session->set_userdata('discount',$record['discount']);
			echo "<h4>you have saved ",$record['discount']," rs</h4>";
		}
		} else {
			echo "<h4>code you entered is not valid</h4>";
		}
			
	}

}
?>
