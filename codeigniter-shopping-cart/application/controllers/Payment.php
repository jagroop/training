<?php
class Payment extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('string');
        $this->load->library('cart');
		//$this->load->model('data_model');
    }
    public function do_purchase(){		
        if ($cart = $this->cart->contents()){			
        $config['business'] 			= 'summarjatt@gmail.com';
		$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
		$config['return'] 				= 'http://localhost/ci_cart/index.php/payment/notify_payment';
		$config['cancel_return'] 		= 'http://localhost/ci_cart/index.php/payment/cancel_payment';
		$config['notify_url'] 			= 'process_payment.php'; //IPN Post
		$config['production'] 			= FALSE; //Its false by default and will use sandbox
		if(isset($_SESSION['discount'])){
		$total = $this->cart->total();
		$discount = $_SESSION['discount'];		
		$discount_precent = ($total - $discount) / $total;
			$config['discount_rate_cart'] = $discount_precent; //discount by percent
		}
		
		$config["invoice"]	= $this->session->userdata('order_id'); //The invoice id		
		$this->load->library('paypal',$config);
		$item_name = implode(",",array_column($cart,'name'));
		$this->paypal->add($item_name,$this->cart->total(),$this->cart->total_items());				
		$this->paypal->pay(); //Proccess the payment 
        }            
    }
    public function notify_payment(){  	
		//echo "<pre>";
		//print_r($_REQUEST);
		$status = $_REQUEST['payment_status'];
		if($status == 'Pending'){
			//$this->data_model->update_payment($this->session->userdata('order_id'));
		 echo "<h1>Your payment is pending...</h1>";
		 echo anchor('cart/index','<< continue shoping',array('class'=>'btn btn-default'));		 
		}
		else if($status == 'Success'){
			$this->data_model->update_payment($this->session->userdata('order_id'));
		}
    }
    public function cancel_payment(){
        //redirect to the index page
        redirect('cart/index');
        
    }
}
?>