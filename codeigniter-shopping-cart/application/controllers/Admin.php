<?php
class Admin extends CI_Controller{
	public function __construct(){
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
	}
    public function index(){
        $this->load->view('admin_pannel');
    }
    public function load_form(){
        $this->load->view('add_products');
    }
    public function add_product(){
    $config['upload_path'] = './images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
            //echo $data['upload_data']['file_name'];
            //die;
            //$data = $this->upload->data();
			$data = [
              'name' => $this->input->post('product_name'),
              'description' => $this->input->post('description'),
              'price' => $this->input->post('price'),
			  'cat' => $this->input->post('cat'),
              'picture' => 'images/'.$data['upload_data']['file_name']
            ];            
            $this->admin_model->add_product($data);
            redirect('admin/load_form');
		}
    }
    public function orders(){
        $records['orders'] = $this->admin_model->get_orders();
        $this->load->view('orders',$records);
    }
   
}
?>
