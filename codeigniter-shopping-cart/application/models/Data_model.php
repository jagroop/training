<?php
class Data_model extends CI_Model{
    public function getAll(){
        $query = $this->db->get('products');
        if($query){
            return $query->result_array();
        }
    }
    public function get_products($cat){
        $query = $this->db->get_where('products',array('cat'=>$cat));
        return $query->result_array();
    }
    public function get_product($id){
        $query = $this->db->get_where('products',array('serial'=>$id));
        return $query->result_array();
    }
    public function discount($code){
       $query =  $this->db->get_where('coupon',array('code' => $code));
       if (!empty($query)){
       return $query->result_array();
       }
       else {
        return null;
       }
       
    }
    public function add_customer($user){
    $this->db->insert('customers',$user);
    $id = $this->db->insert_id();
    return (isset($id)) ? $id:FALSE;
    }
    public function add_order($order){
    $this->db->insert('orders',$order);
    $oid = $this->db->insert_id();
    return (isset($oid)) ? $oid:FALSE;
    }
    public function insert_order_details($details){
        $this->db->insert('order_detail',$details);
    }
    public function paypal_payment($product){
        //$foo = $_SESSION['items_name'];
        //$var  = array_merge($foo,$product);
        echo "<pre>";
        print_r($product);
       
    }
    public function update_payment($order_id){
        $this->db->where('serial',$order_id);
        $data['payment_status'] = 1;
       $update = $this->db->update('orders',$data);
       if($update){
        $this->session->unset_userdata('order_id');
        $this->load->view('billing_success');
       }
    }
}
?>
