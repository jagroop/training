<?php
namespace App\Quotes;
trait AwesomeQuotes {	

	public function getQuotes(){

		return [
		 "Walking on water and developing software from a specification are easy if both are frozen. --Edward V Berard",
		 "It always takes longer than you expect, even when you take into account Hofstadter's Law. --Hofstadter's Law",
		 "Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live. --Rick Osborne",
		 "Some people, when confronted with a problem, think I know, I'll use regular expressions. Now they have two problems. --Jamie Zawinski"
		];	

	}
}