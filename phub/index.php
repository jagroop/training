<?php
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['url'] = $current_link;
if(isset($_GET['page'])){
    $page = (int)$_GET['page'];
    $questions = $obj->questions($page);
}
if(!isset($_GET['page'])) {
    $questions = $obj->questions('1');
}
if(isset($_POST['ask'])){
    $tags = $_POST['multiselect'];
    $tags_str = implode(',',$tags);   
    $data =  [
             'user_id' => $_SESSION['user_id'],
             'title' => $_POST['title'],
             'desc' => $_POST['desc'],
             'tags' => $tags_str
             ];   
    $ask_question = $obj->ask_question($data);
    if($ask_question == 'success'){
        header("Location:index.php");
    }
}
elseif(isset($_POST['ask_question_btn'])){
    if(isset($_SESSION['user_id'])){
        header("Location:index.php#ask_question");
    }
    else{
        header("Location:login.php");
    }
}
?>

<div class="row">
    <div class="col-md-1">
        
    </div>
    
    <div class="col-md-8">
        <?php foreach($questions as $question):?>   
<h5>
    <img class="img-circle" height="40" width="40" src="uploads/<?php if(!empty($question['pic'])){echo $question['pic'];} else{echo "avatar.png";}?>">
    <a style="text-decoration:none;" href="question.php?q=<?= $question['ques_id'];?>"><b><?= $question['title'];?></b></a> | <?= $obj->time_ago($question['date']);?></h5>
By: <a style="text-decoration:none;" href="user.php?user=<?= $question['id'];?>"><?= $question['name'];?></a> |
<i class="fa fa-heart-o"></i> <?= $question['votes'];?> |
<i class="fa fa-comment-o"></i> <?= $question['answers'];?> |
<?php $baz = explode(',',$question['tags']); ?>
<?php foreach($baz as $bux):?>
<a style="text-decoration:none;" href="tag.php?tag_name=<?= $bux;?>"><span class="label label-success"><i class="fa fa-tag"></i> <?= $bux;?></span></a>
<?php endforeach;?>
<br><br>   
<hr>
<?php endforeach;?>

<!--bootstrap pagination-->

    <ul id="pagination-demo" class="pagination"></ul>
    
    <!-- //bootstrap pagination-->
    
<!--ask question div start here-->
<br><br> <br><br> <br><br> 
<?php if(isset($_SESSION['user_id'])):?>
<?php   $skills = $obj->tags();
?>
<div id="ask_question"></div>
<form class="form-horizontal" action="" method="post">
  <fieldset>
    <legend><i class="fa fa-pencil-square-o"></i> Ask a Question</legend>
    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Title</label>
      <div class="col-lg-10">
        <input type="text" name="title" class="form-control" id="inputEmail" placeholder="enter the title of the question here" maxlength="100" required>
        <span class="text-danger">maxlength="100"</span>
      </div>
    </div>   
    <div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Description</label>
      <div class="col-lg-10">
        <script src="assets/tinymce/tinymce.min.js"></script>
            <script type="text/javascript">
                tinymce.init({
                    selector: "#mytextarea",
                    theme: "modern",                    
                    height: 300,
                    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
             ],
                    style_formats: [        
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'},
    ],
                    setup: function (editor) {
                    editor.on('change', function () {
                    editor.save();
                     });
                  }
                });
            </script>
            <span class="text-primary">Be open minded and add <b>code in html code tag</b>..</span>
            <textarea id="mytextarea" name="desc" required></textarea>
      </div>
    </div>   
    <div class="form-group">                
        <div class="col-sm-10">
<span class="label label-success"><i class="fa fa-tags"></i>  Tags</span>
            <select id="example-post" name="multiselect[]" multiple="multiple">
                <?php foreach($skills as $skill):?>
                 <option value="<?= $skill['tag_name'];?>"><?= $skill['tag_name'];?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>       
   
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <button type="reset" class="btn btn-default">Cancel</button>
        <button type="submit" name="ask" class="btn btn-primary">Submit POST</button>
      </div>
    </div>
  </fieldset>
</form>
<?php endif;?>

<!--//ask question div start here-->


<!--LOGIN TO ASK A QUESTION-->

<?php if(!isset($_SESSION['user_id'])):?>
<div class="well">
<h3>Please <a href="login.php">Login</a> first to ask a Question <i class="fa fa-smile-o"></i></h3>
</div>
<?php endif;?>
    </div>
    
    <div class="col-md-3">
        <?php
        include 'app/honkiat.php';
        $top_ten = $obj->top_ten();       
        include 'app/sidebar.php';
        $tags1 = $obj->tags_sidebar();
        include 'app/tag_sidebar.php';
        include 'app/poll.php';
        ?>
        
    </div>
    
</div>




<?php require 'app/footer.php' ;?>
