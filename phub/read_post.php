
<?php
if(empty($_GET['post_id'])){
    header("Location:blog.php");
}
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
// Increment blog view
//$increment_blog_view = $obj->increment_blog_view($_GET['post_id']);

$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//echo $current_link; die();
$_SESSION['url'] = $current_link;
$id = $_GET['post_id'];
$post = $obj->read_post($id);

//echo "<pre>"; print_r($post); die();
if(isset($_SESSION['user_id'])){
    $post_like_status = $obj->post_like_status($_SESSION['user_id'],$id);
}
if(isset($_POST['do_reply'])){
    $fool = $_POST['post_id'];
    $rply = $obj->rply($_POST);
    if($rply){
        header('Location:read_post.php?post_id='.$fool.'#comments');
    }
}
?>
<style>
#nav.affix {
    background-color: rgba(255,255,255,0.95);
    border-bottom: 1px solid #e7e7e7;    
    position: fixed;
    top: 0;
    width: 100%;
    height:7%;
    z-index:10;
}
</style>

<!--Diffrence Between two Dates-->

<?php
   function time_ago($date)

{

if(empty($date)) {

return "No date provided";

}

$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");

$lengths = array("60","60","24","7","4.35","12","10");

$now = time();

$unix_date = strtotime($date);

// check validity of date

if(empty($unix_date)) {

return "Bad date";

}

// is it future date or past date

if($now > $unix_date) {

$difference = $now - $unix_date;

$tense = "ago";

} else {

$difference = $unix_date - $now;
$tense = "from now";}

for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {

$difference /= $lengths[$j];

}

$difference = round($difference);

if($difference != 1) {

$periods[$j].= "s";

}

return "$difference $periods[$j] {$tense}";

}
?>
<!-- //Diffrence Between two Dates-->


    <!-- Fixed Navbar --><div id="nav">
  <div class="navbar navbar-static">
                  
        <div class="row" style="{float:left;}">        
            <div style="color: #999; font-size: 15px;">
            <?php if(empty($post['pic'])):?>
               <img class="img-circle" height="30" width="30" src="uploads/avatar.png" alt="<?= $post['name'];?>">
               <?php endif;?>.
                <?php if(!empty($post['pic'])):?>
               <img class="img-circle" height="30" width="30" src="uploads/<?= $post['pic'];?>" alt="<?= $post['name'];?>">
               <?php endif;?>
           <b><?= $post['name'];?></b> | <?= time_ago($post['date']);?>           
&nbsp;&nbsp;&nbsp;<a class="text-danger" <?php if(!isset($_SESSION['user_id'])){echo "data-toggle='popover' title='You have to login first in order to like that post' data-content='<a href=".'login.php'." style=".'color:black;'."><b>login|register</b></a>' data-html='true' data-placement='bottom'";}?> style="text-decoration: none;" href="<?php if(!isset($_SESSION['user_id'])) {echo '#';} elseif($post_like_status == 0){ echo 'like.php';} else{ echo 'unlike.php';}?>?type=blog&&post_id=<?= $id;?>">
            <i <?php if($post_like_status == 0){echo "class='fa fa-heart-o'";} else{ echo "class='fa fa-heart'";}?>></i>  <?= $post['votes'];?></a> | 
            <i class="fa fa-comment-o"></i> <?= $post['comments'];?>            
            &nbsp;&nbsp;&nbsp;<a style="text-decoration: none;" href="<?php if(!isset($_SESSION['user_id'])) { ; echo 'login.php?bummer&&add_post';  } else{ echo'blog.php?add_post#do_add_post'; } ?>">
          <i class="fa fa-pencil-square-o"></i></a>
          </div> 
    </div>  
</div> 
</div><!--fixed navbar-->

<div class="container-fluid">
 
<div class="row">
    <div class="col-md-1">      
    </div>
    <div class="col-md-8">
        
            <h2 class="text-success"><b><?= $post['title'];?></b></h2>           
           <i class="fa fa-folder-o"></i> Category: <a style="text-decoration: none;" href="blog_cat.php?cat=<?= $post['cat'];?>"> <?= $post['cat'];?></a>           
           <hr>
           <div style="line-height:170%;">
            <style>
                img{
                    max-width: 100%;
                    height: auto;
                }
            </style>
           <p align="justify"><?= $post['desc'];?></p>           
           </div>
    <div class="well">
        <?php
        $comments = $obj->blog_comments($id);        
        ?>
        <h4 id="comments"><span class="label label-success"><i class="fa fa-comments-o"></i> Comments</span></h4><br>
        <?php if(empty($comments)):?>
        <h3>No comments posted yet..</h3>
        <?php endif;?>
        <?php if(!empty($comments)):?>
        
        <?php foreach($comments as $comment):?>
         <?php if(empty($comment['pic'])):?>
         <img height="50" width="50" class="img-circle" src="uploads/avatar.png">
         <?php endif;?>
         <?php if(!empty($comment['pic'])):?>
         <img height="50" width="50" class="img-circle" src="uploads/<?= $comment['pic'];?>">
         <?php endif;?>
         
         <a style="text-decoration: none;" href="user.php?user=<?= $comment['id'];?>"><?= $comment['name'];?></a> | <i class="fa fa-calendar"></i> <?= $comment['comment_date'];?>
         <?php if(isset($_SESSION['user_id'])):?>
         <?php if($comment['id'] == $_SESSION['user_id']):?>
         
         | <a href="del.php?type=blog_comment&&user_id=<?=$comment['id'];?>&&comment_id=<?=$comment['comment_id'];?>" onclick="return confirm('Are you Sure ??');"><i class="fa fa-trash-o"></i></a>         |
         <?php endif;?>
         <?php endif;?>
         <br><br>
         <p><?= $comment['comment'];?></p>
         <hr>
        <?php endforeach;?>
        <?php endif;?>
        
       
    </div>
    <?php if(isset($_SESSION['user_id'])):?>
    <h4><i class="fa fa-reply"></i> Leave a reply</h4>
    <form action="" method="post">
        <input type="hidden" name="user_id" value="<?= $_SESSION['user_id'];?>">
        <input type="hidden" name="post_id" value="<?= $id;?>">
        <textarea class="form-control" name="reply" rows="7" required></textarea><br>
        <input type="submit" name="do_reply" class="btn btn-primary" value="Post Comment">
    </form>
    <?php
    else: echo "<h2><b><a style='text-decoration:none;' class='btn btn-info' href='login.php?add_comment'>Please Login to Comment on this post</a></b></h2>";
    endif;?>
        
    </div>
    <div class="col-md-3">
        <?php $counting = $obj->cat_count();
              
        ?>
        <?php include 'app/blog_sidebar.php'; ?>
    </div>
    
</div>
</div>
<?php include 'app/footer.php'  ;?>
