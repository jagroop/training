<?php
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
if(isset($_POST['register'])){
  if(!empty($_FILES['pic']['name'])) {
  $rand_val = date('YMDHIS') . rand(11111, 99999);
  $unique = md5($rand_val);
}
     $target_dir = "uploads/";
     $target_file = $target_dir . basename($unique.$_FILES["pic"]["name"]);
     $error = 0;
     $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
     if ($_FILES["pic"]["size"] > 500000) {
     echo "Sorry, your file is too large.";
     $error = 1;
     }
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $error = 1;
  }
  if($error == 1){?>
 <div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Failure !!</strong> <a href="#" class="alert-link">please select an image</a> or try submitting again.
</div>
  <?php }
  else{
    move_uploaded_file($_FILES["pic"]["tmp_name"], $target_file);
        $skills = $_POST['multiselect'];
    $skills_str = implode(',',$skills);
     $data = array(
                  'name'=> $_POST['name'],
                  'pic' => $unique.$_FILES['pic']['name'],
                  'email' => $_POST['email'],
                  'password' => $_POST['password'],
                  'country' => $_POST['country'],
                  'state' => $_POST['state'],
                  'skills' => $skills_str
                  );
       $register = $obj->register($data);
       if($register == 'success'){ ?>
        <div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Success</strong>&nbsp;&nbsp;<b><u>You are successfully Registered !!</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
      <?php }
       elseif($register == 'failure'){ ?>
        <div class="alert alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Oh snap!</strong> <a href="#" class="alert-link">something went wrong</a> and try submitting again.
      </div>
       <?php }
       elseif($register == 'already'){ ?>
        <div class="alert alert-dismissible alert-info">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Email alredy registerd</strong> Try <a href="#" class="alert-link">another one</a>
</div>
       <?php }
     }
}
elseif(isset($_POST['login'])){
   
    $login = $obj->login($_POST['email'],$_POST['password']);
    if($login == 'wrong'){ ?>
    
         <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>ERROR</strong>&nbsp;&nbsp;<b><u>Wrong email or password</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>
    <?php }
    elseif($login == 'blocked'){ ?>
      <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Oops!</strong>&nbsp;&nbsp;<b><u>You are blocked by Admin</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>
   <?php }
}
?>
<div class="row">
    <div class="col-md-2">        
    </div>    
    <div class="col-md-8">
      <?php
      if(isset($_GET['add_post'])){
        $_SESSION['url'] = $_SESSION['url'].'#do_add_post';
      }
      elseif(isset($_GET['add_comment'])){
        $_SESSION['url'] = $_SESSION['url'].'#comments';
      }
      elseif(isset($_GET['ask_question'])){
        $_SESSION['url'] = 'index.php#ask_question';
      }
      ?>
       <?php if(isset($_GET['bummer'])):?>
       <h4 class=text-danger><b>You have to login first in order to do that !</b></h4>
       <p class="text-danger">you will be redirected to the same page <i class="fa fa-smile-o"></i></p>
       <?php endif;?>
       <ul class="nav nav-tabs">
  <li class="active"><a href="#login" data-toggle="tab">login</a></li>
  <li><a href="#register" data-toggle="tab">register</a></li>    
</ul>
<div id="myTabContent" class="tab-content">
  <div class="tab-pane fade active in" id="login">
    <br><br>
    <div class="row">
    <div class="col-md-5">
    <form action="" method="post">
        <input type="email" name="email" class="form-control" placeholder="email@example.com" required><br><br>
        <input type="password" name="password" class="form-control" placeholder="password" required><br><br>
        <input type="submit" name="login" class="btn btn-success" value="Login">
    </form>
    </div>    
    <div class="col-md-7">
        <div class="well">
            <h4><u>or sign in with</u></h4><br>
            <a href="#" class="btn btn-info"><i class="fa fa-facebook"> login using facebook &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></a> <br><br>
            <a href="#" class="btn btn-danger"><i class="fa fa-google-plus"> login using google plus</i></a>
        </div>
    </div>
    </div>
  </div>
  <div class="tab-pane fade" id="register">
    <br><br>
    <div class="row">
    <div class="col-md-5">
    <form data-toggle="validator" role="form" action="" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="inputName" class="control-label">Name</label>
    <input type="text" name="name" class="form-control" id="inputName" placeholder="name" required>
  </div>
  <div class="form-group">
    <label for="inputPic" class="control-label">Profile Pic</label>
    <input type="file" name="pic" class="btn btn-default" id="inputPic">    
  </div>
    <div class="form-group">
    <label for="inputEmail" class="control-label">Email</label>
    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" data-error="oops, that email address is invalid" required>
    <div class="help-block with-errors"></div>
  </div>    
    <div class="form-group">
        <label for="inputPassword" class="control-label">Password</label>
        <div class="form-group">
            <input type="password" name="password" data-minlength="6" class="form-control" id="inputPassword" placeholder="Password" required>
      <span class="help-block">Minimum of 6 characters</span>
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, password does not match" placeholder="Confirm Password" required>
      <div class="help-block with-errors"></div>
        </div>
    </div>    
    <!--country and state section-->    
    <script src="assets/js/countries.js"></script>
    <div class="form-group">
         <label for="inputPassword" class="control-label"> Select Country:</label>
         <div class="form-group">
      <select onchange="print_state('state', this.selectedIndex);" id="country" name ="country" class="form-control" required></select>
         </div>
         <div class="form-group">
            <label for="inputPassword" class="control-label">City/District/State:</label>
     <select name ="state" id ="state" class="form-control" required></select>
         </div>
    </div>
    <script language="javascript">print_country("country");</script>        
        <!-- //country and state section-->        
       <!-- skills | tags-->
       <?php
       $tags = $obj->tags();
       ?>      
    <div class="form-group">
        <label class="col-sm-2 control-label">Skills</label>
        <div class="col-sm-10">
            <select id="example-post" name="multiselect[]" multiple="multiple">                
                 <?php foreach($tags as $tag):?>
                 <option value="<?= $tag['tag_name'];?>"><?= $tag['tag_name'];?></option>
                 <?php endforeach;?>
            </select>
            <br>
    <br>
        </div>
    </div>    
    <div class="form-group">
    <input type="submit" name="register" class="btn btn-success" value="Register">
    </div>
    </form>   
    </div>    
    <div class="col-md-7">
        <div class="well">
            <h4><u>or login with</u></h4><br>
            <a href="#" class="btn btn-info"><i class="fa fa-facebook"> login using facebook &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></a> <br><br>
            <a href="#" class="btn btn-danger"><i class="fa fa-google-plus"> login using google plus</i></a>
        </div>
    </div>
    </div>
  </div>  
</div> 
    </div>    
    <div class="col-md-2">        
    </div>
</div>
<?php include 'app/footer.php';?>
