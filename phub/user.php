<?php
if(empty($_GET['user'])){
    header("Location:index.php");
}
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['url'] = $current_link;
$user_id = $_GET['user'];

$user_data = $obj->user_data($user_id);
$ques_by_user = $obj->ques_by_user($user_id);
$answers_by_user = $obj->answers_by_user($user_id);

if(empty($user_data)){
    header("Location:index.php");
}
?>
<div class="row">
    <div class="col-md-2">
        
    </div>
    
    <div class="col-md-8">
        <div class="well">
			
			<div class="row">
				<div class="col-sm-4">
					<?php if(empty($user_data['pic'])):?>
            <img class="img-circle" height="70" width="70" src="uploads/avatar.png">
            <?php endif;?>
            <?php if(!empty($user_data['pic'])):?>
            <img class="img-circle" height="200" width="200" src="uploads/<?= $user_data['pic'];?>">
            <?php endif;?>
				</div>
				<div class="col-sm-8">
					<h2><i class="fa fa-user"></i> <?= $user_data['name'];?> | <i class="fa fa-map-marker"></i> <?= $user_data['country'];?>, <?= $user_data['state'];?></h2>
					<br>
					
					<i class="fa fa-code"></i>	<?php $baz = explode(',',$user_data['skills']); ?>
<?php foreach($baz as $bux):?>
<a style="text-decoration:none;" href="tag.php?tag_name=<?= $bux;?>"><span class="label label-primary"><i class="fa fa-tag"></i> <?= $bux;?></span></a>
<?php endforeach;?>
					
				</div>
			</div>		
            <hr>
            <?php if(empty($ques_by_user)):?>
            <h3>No. questions by this user..</h3>
            <?php endif;?>
            <?php if(!empty($ques_by_user)):?>
            <h3>
                Questions by this user:
            </h3>
            <?php foreach($ques_by_user as $question):?>
            <h4><a style="text-decoration:none;" href="question.php?q=<?= $question['ques_id'];?>"><?= $question['title'];?></a></h4>
		<span class="label label-default"><i class="fa fa-heart-o"></i>  <?= $question['votes'];?></span>
		<span class="label label-default"><i class="fa fa-comment-o"></i>  <?= $question['answers'];?></span>
                 &nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;
	    <?php $foo = explode(',',$question['tags']); ?>
	    <?php foreach($foo as $bar):?>
	<a href="tag.php?tag_name=<?= $bar;?>"><span class="label label-primary"><i class="fa fa-tag"></i> <?= $bar;?></span></a>
	    <?php endforeach; ?>
	    <hr>
            <?php endforeach;?>
            
            <br>
             <?php endif;?>
            
            
        </div>
    </div>
    <div class="col-md-2">
        
    </div>
</div>
<?php include 'app/footer.php';?>




