<?php
require 'app/Functions.php';
$obj = new Functions;
if(empty($_GET['type'])){
    header("Location:index.php");
}
if(empty($_SESSION['user_id'])){    
    header("Location:login.php?bummer");
    exit();
}
$type = $_GET['type'];

if($type == 'question'){
   if(isset($_GET['ques_id'])){
    $q_id = $_GET['ques_id'];
    $u_id = $_SESSION['user_id'];
    $like = $obj->like('question',$q_id,$u_id);
    header("Location:question.php?q=$q_id");
}
else{
    header("Location:index.php");
} 
}
elseif($type == 'answer'){
    if(isset($_GET['ans_id'])){
    $a_id = $_GET['ans_id'];
    $q_id = $_GET['ques_id'];
    $u_id = $_SESSION['user_id'];
    $like = $obj->like('answer',$a_id,$u_id);
    header("Location:question.php?q=$q_id#answers");
}
else {
    header("Location:index.php");
}
}
elseif($type == 'blog'){
    if(isset($_GET['post_id'])){
    $p_id = $_GET['post_id'];
    //$q_id = $_GET['ques_id'];
    $u_id = $_SESSION['user_id'];
    $like = $obj->like('blog',$p_id,$u_id);
    header("Location:read_post.php?post_id=$p_id");
}
else {
    header("Location:blog.php");
}
}

