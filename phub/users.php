<?php
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['url'] = $current_link;
$get_all_users = $obj->get_all_users();
?>
<div class="row">
  <div class="col-md-1">
  
  </div>
  
  <div class="col-md-10">
  <div class="panel panel-default">
  <div class="panel-body">
     <?php foreach($get_all_users as $user):?>
    <div style="float: left; margin-left:10px" class="well well-sm">
        <?php if(empty($user['pic'])):?>
            <img class="img-circle" height="40" width="40" src="uploads/avatar.png">
            <?php endif;?>
            <?php if(!empty($user['pic'])):?>
            <img class="img-circle" height="40" width="40" src="uploads/<?= $user['pic'];?>">
            <?php endif;?>
        <a href="user.php?user=<?= $user['id'];?>"><?= $user['name'];?></a><br>
        <?= $user['country'];?>, <?= $user['state'];?><br>
        <?php $baz = explode(',',$user['skills']); ?>
<?php foreach($baz as $bux):?>
<a style="text-decoration:none;" href="tag.php?tag_name=<?= $bux;?>"><span class="label label-primary"><i class="fa fa-tag"></i> <?= $bux;?></span></a>
<?php endforeach;?>
        
    </div>
    <?php endforeach;?>
  </div>
</div>
   
 
  </div>
  
  
  <div class="col-md-1">
    
  <?php
  //$top_ten = $obj->top_ten();
  //include 'app/sidebar.php' ;
  ?>
  </div>
</div>
