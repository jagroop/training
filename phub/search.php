<?php
if(empty($_GET['search'])){
    header("Location:index.php");
}
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
$search = $_GET['search'];
if((isset($search)) || (isset($_GET['do_search']))) {
    $do_search = $obj->search_engine($search);
    //echo "<pre>"; print_r($do_search); echo "</pre>";
}
?>
<div class="row">
  <div class="col-md-1">
  
  </div>
  
  <div class="col-md-8">
  <!--search form -->
  <form action="" method="get">
    <div class="col-md-8">
    <input type="text" name="search" value="<?= $search;?>" class="form-control" required>    
    </div>
    <button type="submit" name="do_search" class="btn btn-default"><i class="fa fa-search"></i> </button>   
  </form>
  <!-- //search form -->
  <br><br>
  <div class="well well-lg">
   <?php if(empty($do_search)):?>
   <h3 class="text-danger">No. results found related to <?= $search;?></h3>
   <?php endif;?>
   
   <?php if(!empty($do_search)):?>
    <?php foreach($do_search as $question):?>
    <h2><a style="text-decoration: none;" href="question.php?q=<?= $question['ques_id'];?>"><?= $question['title'];?></a></h2>
    <?php $baz = explode(',',$question['tags']); ?>
<?php foreach($baz as $bux):?>
<a style="text-decoration:none;" href="tag.php?tag_name=<?= $bux;?>"><span class="label label-success"><i class="fa fa-tag"></i> <?= $bux;?></span></a>
<?php endforeach;?>
    <hr>
    <?php endforeach;?>
   <?php endif;?>
  </div>
  
  </div>
  
  <div class="col-md-3">
  
  </div>
</div>