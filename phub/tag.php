<?php
if(empty($_GET['tag_name'])){
    header("Location:tags.php");
}
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['url'] = $current_link;
$tag_name = $_GET['tag_name'];
?>
<div class="row">
  <div class="col-md-1">
  
  </div>
  
  <div class="col-md-8">
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Questions related to: <?= $tag_name; ?> </h3>
  </div>
  <div class="panel-body">
    <?php $questions = $obj->tag_questions($tag_name);?>
    <?php if(!empty($questions)):?>
    <?php foreach($questions as $question):?>
    
    <h3><a style="text-decoration:none;" href="question.php?q=<?= $question['ques_id'];?>"><?= $question['title'];?></a></h3>
    <span class="label label-default"><i class="fa fa-heart-o"></i>  <?= $question['votes'];?></span>
    <span class="label label-default"><i class="fa fa-comment-o"></i>  <?= $question['answers'];?></span> | <i class="fa fa-code"></i>
    <?php $baz = explode(',',$question['tags']); ?>
<?php foreach($baz as $bux):?>
<a style="text-decoration:none;" href="tag.php?tag_name=<?= $bux;?>"><span class="label label-primary"><i class="fa fa-tag"></i> <?= $bux;?></span></a>
<?php endforeach;?><br>
<hr>
    <?php endforeach;?>
    <?php endif;?>
    
    <?php if(empty($questions)):?>
    <h2 class="text-danger"> No. questions found related to <?= $tag_name;?></h2>
    <?php endif;?>
  </div>
</div>
  
  
  <div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">Users having <?= $tag_name ;?> skill</h3>
  </div>
  <div class="panel-body">
    <?php $users = $obj->tag_users($tag_name);?>
    <?php if(!empty($users)):?>
    <?php foreach($users as $user):?>
    <div style="float: left; margin-left:10px" class="well well-sm">
        <?php if(empty($user['pic'])):?>
            <img class="img-circle" height="40" width="40" src="uploads/avatar.png">
            <?php endif;?>
            <?php if(!empty($user['pic'])):?>
            <img class="img-circle" height="40" width="40" src="uploads/<?= $user['pic'];?>">
            <?php endif;?>
        <a href="user.php?user=<?= $user['id'];?>"><?= $user['name'];?></a><br>
       <i class="fa fa-map-marker"></i> <?= $user['country'];?>, <?= $user['state'];?><br>
        <i class="fa fa-code"></i> <?php $a = explode(',',$user['skills']); ?>
<?php foreach($a as $b):?>
<a style="text-decoration:none;" href="tag.php?tag_name=<?= $b;?>"><span class="label label-primary"><i class="fa fa-tag"></i> <?= $b;?></span></a>
<?php endforeach;?>
        
    </div>
    <?php endforeach;?>
    <?php endif;?>
    
    <?php if(empty($users)):?>
    <h2 class="text-danger"> Nobody have <?= $tag_name;?> skill.</h2>
    <?php endif;?>
  </div>
</div>
  </div>
  
  <div class="col-md-3">
  <?php
        $top_ten = $obj->top_ten();       
        include 'app/sidebar.php';?>
  </div>
</div>

<?php include 'app/footer.php';?>