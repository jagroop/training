<?php session_start();?>
<html>
    <head>
        <title>Programmers HuB</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">		
        <link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/model.css">
        <link rel="stylesheet" href="assets/font_awesome/css/font-awesome.css">
        <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css">		
        <style>
            header {
	            height:100px;
                    }
        </style>
    </head>
    <body>		 
        <header>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><i class="fa fa-code"></i> <b>Programmers HuB</b></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        <?php $a = $_SERVER[REQUEST_URI];
              $file = basename($a);
        ?>
        <li <?php if($file == 'index.php'){echo "class='active'"; }?> ><a href="index.php"><i class="fa fa-stack-overflow"></i> <b>Forum</b> <span class="sr-only">(current)</span></a></li>
        <li <?php if($file == 'blog.php'){echo "class='active'"; }?> ><a href="blog.php"><i class="fa fa-rss"></i> <b>Blog</b></a></li>         
  <li <?php if($file == 'tags.php'){echo "class='active'"; }?> ><a href="tags.php"><i class="fa fa-tags"></i> <b>Tags</b></a></li>
  <li <?php if($file == 'users.php'){echo "class='active'"; }?> ><a href="users.php"><i class="fa fa-users"></i> <b>Users</b></a></li>
  <?php if(isset($_SESSION['user_id'])):?>
  <li><a href="index.php#ask_question"><i class="fa fa-comment"></i> <b>Ask Question</b></a></li>
  <?php endif;?>
  <li><a href="#"><i class="fa fa-square-o"></i> <b>Unanswered Questions</b></a></li>
      </ul>
      <form action="search.php" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group">
          <input type="text" name="search" class="form-control" value="<?php if(isset($_GET['search'])) {echo $_GET['search'];}?>" placeholder="Search" required>
        </div>
        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> </button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <?php
        if(empty($_SESSION['user_id'])){?>
           <li><a href="login.php"><i class="fa fa-sign-in"></i> <b>login | register</b></a></li>
          
        <?php }
        else { ?>            
             <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> <?= $_SESSION['user_name'];?> <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">edit profile</a></li>            
          </ul>
        </li>
            <?php if(!empty($_SESSION['user_pic'])) {
            ?>
            
            <li><img class="img-circle" height="40" width="40" src="uploads/<?= $_SESSION['user_pic'];?>"></li>
            <li><a href="logout.php"><i class="fa fa-sign-out"></i> <b>logout</b></a></li> 
        <?php }
        
        else{ ?>
        
            <li><img class="img-circle" height="40" width="40" src="uploads/avatar.png"></li>
            <li><a href="logout.php"><i class="fa fa-sign-out"></i> <b>logout</b></a></li>
        <?php }
        
        }
        ?>        
        
      </ul>
    </div>
  </div>
</nav>
        </header>   