<div class="well">
<b class="text-success">Poll of the weak</b>
    <div class="progress">
  <div class="progress-bar progress-bar-info" style="width: 20%"><b>Windows<b></div>
</div>

<div class="progress">
  <div class="progress-bar progress-bar-success" style="width: 40%">Mac</div>
</div>

<div class="progress">
  <div class="progress-bar progress-bar-warning" style="width: 60%">Tizen</div>
</div>

<div class="progress">
  <div class="progress-bar progress-bar-danger" style="width: 80%">Linux</div>
</div>
</div>