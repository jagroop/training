<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="assets/js/model.js"></script>
<script src="assets/js/validator.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/paginator.js"></script>
<script src="assets/js/bootstrap-multiselect.js"></script>

<script>
       $('#nav').affix({
      offset: {
        top: 20        
      }
});	
</script>

<!-- Initialize the plugin: -->
<script type='text/javascript'>
       $('#pagination-demo').twbsPagination({
        totalPages: <?= $_SESSION['total_pages'] ;?>,
        visiblePages: 7,
        href: '?page={{number}}',
        onPageClick: function (event, page) {
            $('#page-content').text('Page ' + page);
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example-post').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
    });
</script>
</body>
</html>