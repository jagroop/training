<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">Tags:</h3>
  </div>
  <div class="panel-body">
    <div class="container-fluid">
     <?php foreach($tags1 as $tag1):?>
    <a style="text-decoration:none;display: inline-block;" href="tag.php?tag_name=<?= $tag1['tag_name'];?>"><span class="label label-primary"><i class="fa fa-tag"></i> <?= $tag1['tag_name'];?></span></a>
    <?php endforeach;?>
    </div>
  </div>
</div>
