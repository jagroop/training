<?php
require_once 'DB_Controller.php';

class Functions extends DB_Controller{
		 public function time_ago($date)
		 
		 {
		 
		 if(empty($date)) {
		 
		 return "No date provided";
		 
		 }
		 
		 $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		 
		 $lengths = array("60","60","24","7","4.35","12","10");
		 
		 $now = time();
		 
		 $unix_date = strtotime($date);
		 
		 // check validity of date
		 
		 if(empty($unix_date)) {
		 
		 return "Bad date";
		 
		 }
		 
		 // is it future date or past date
		 
		 if($now > $unix_date) {
		 
		 $difference = $now - $unix_date;
		 
		 $tense = "ago";
		 
		 } else {
		 
		 $difference = $unix_date - $now;
		 $tense = "from now";}
		 
		 for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
		 
		 $difference /= $lengths[$j];
		 
		 }
		 
		 $difference = round($difference);
		 
		 if($difference != 1) {
		 
		 $periods[$j].= "s";
		 
		 }
		 
		 return "$difference $periods[$j] {$tense}";
		 
		 }
    public function login($email,$password){
        $q = "SELECT id,name,pic,email,password,status
		     FROM users
			 WHERE email='$email' AND password='$password'";
        $sql = mysql_query($q);
        $rows = mysql_num_rows($sql);
        if($rows > 0){
            $user = mysql_fetch_assoc($sql);
			if($user['status'] == 0){
				  return 'blocked';
			}
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['user_name'] = $user['name'];
            $_SESSION['user_pic'] = $user['pic'];
            if(isset($_SESSION['url'])){
				header('Location:'.$_SESSION['url']);
			}
			else {
				header("Location:index.php");
			}
        }
        else {
            return 'wrong';
        }
    }
    public function get_all_users(){
        $q = "SELECT * FROM users";
        $sql = mysql_query($q);
        while($r = mysql_fetch_assoc($sql)){
            $users[] = $r;
        }
        return $users;
    }
    
    ############## TOP 10 #########################
    public function top_ten(){
   $q = "SELECT questions.ques_id,questions.title,
         questions.tags,users.name,users.id
		 FROM questions
		 INNER JOIN users
		 ON questions.user_id=users.id
		 ORDER BY questions.votes DESC
		 LIMIT 10";
        $sql = mysql_query($q);
        while ($r = mysql_fetch_assoc($sql)){
            $top_ten[] = $r;
        }
        return $top_ten;
    }
    
     ##############  /// TOP 10 #########################
    
    ########### To get the tags from db ################
    public function tags(){
        $sql = mysql_query("SELECT * FROM tags");
        while($r = mysql_fetch_assoc($sql)){
            $questions[] = $r;
        }
        return $questions;
    }
	public function tags_sidebar(){
        $sql = mysql_query("SELECT id,tag_name FROM tags LIMIT 20");
        while($r = mysql_fetch_assoc($sql)){
            $questions[] = $r;
        }
        return $questions;
    }
	public function tag_questions($tag_name){
		//select * from shirts where find_in_set('1',colors) <> 0
		$q = "select * from questions where find_in_set('$tag_name',tags)";
		$sql = mysql_query($q);
		while($r = mysql_fetch_assoc($sql)){
			$questions[] = $r;
		}
		return $questions;
	}
	public function tag_users($tag_name){
		$q = "select id,name,pic,country,state,skills
		      from users
			  where find_in_set('$tag_name',skills)";
		$sql = mysql_query($q);
		while($r = mysql_fetch_assoc($sql)){
			$users[] = $r;
		}
		return $users;
	}
    
    ############### USER DATA | questions by user | answers by User ########################
    public function user_data($user_id){
        $q = "SELECT * FROM users WHERE id='$user_id'";
        $sql = mysql_query($q);
        $result = mysql_fetch_assoc($sql);        
        return $result;       
    }
    public function ques_by_user($user_id){
        $q = "SELECT * FROM questions WHERE user_id='$user_id'";
        $sql = mysql_query($q);
        while($result = mysql_fetch_assoc($sql)){
            $questions[] = $result;
        }
        return $questions;
    }
    public function answers_by_user($id){
        $q = "SELECT * FROM answers WHERE user_id='$id'";
        $sql = mysql_query($q);
        while($result = mysql_fetch_assoc($sql)){
            $answers[] = $result;
        }
        return $answers;
    }
    ############### USER DATA | questions by user | answers by User  ########################
    
    public function questions($page){
		$results_per_page = "10";
		$a = mysql_query("SELECT ques_id FROM questions");
		$rows = mysql_num_rows($a);
		$total_pages = ceil($rows/$results_per_page);
		$_SESSION['total_pages'] = $total_pages;
		if($page == 1 || $page == "" || $page > $rows || !is_int($page)){			
			$limit = "LIMIT 0,{$results_per_page}";
		}
		else{			
			$foo = ($page*$results_per_page)-$results_per_page;				
			$limit = "LIMIT {$foo},{$results_per_page}";			
		}
		$q = "SELECT questions.ques_id,questions.title,
			  questions.tags,questions.votes,questions.answers,questions.date,
              users.id,users.name,users.pic
              FROM questions
              INNER JOIN users
              ON questions.user_id=users.id
			  ORDER BY ques_id DESC {$limit}";
	    //echo $q; die();
        $sql = mysql_query($q);		
        while($r = mysql_fetch_assoc($sql)){
            $questions[] = $r;
        }	
		return $questions;
        
    }
    public function get_question($id){
        $q = "SELECT questions.ques_id,questions.title,questions.desc,questions.votes,
		      questions.answers,questions.date,users.id,users.name,users.pic
              FROM questions
              INNER JOIN users
              ON questions.user_id=users.id
              WHERE ques_id = '$id'";
        $sql = mysql_query($q);
        $result = mysql_fetch_assoc($sql);
        return $result;
    }
	public function del_question($qid,$user_id){	
		$delques = "DELETE FROM questions WHERE ques_id = '$qid' AND user_id = '$user_id'";								
		$sqlques = mysql_query($delques);
		$delans = "DELETE FROM answers WHERE ques_id = '$qid'";
		$sqlans = mysql_query($delans);
		$dellks = "DELETE FROM ques_likes WHERE ques_id = '$qid'";
		$sqllks = mysql_query($dellks); 
	}
    public function get_answers($id){
        $q = "SELECT * FROM answers INNER JOIN users ON answers.user_id=users.id WHERE ques_id = '$id'";
        $sql = mysql_query($q);
        while($r = mysql_fetch_assoc($sql)){
            $answers[] = $r;
        }
        return $answers;
    }
    public function post_answer($id,$answer,$user_id){
        $date_time = date('Y-m-d h:i:s');
        $q = "INSERT INTO answers VALUES ('','$id','$answer','$user_id','$date_time','0')";
        $sql = mysql_query($q);
        if($sql){
            $foo = "UPDATE questions SET answers=answers+1 WHERE ques_id = '$id'";
            $bar = mysql_query($foo);
			$update = mysql_query("UPDATE users SET ans=ans+1 WHERE id=$user_id");
        }
    }
	public function post_like_status($uid,$pid){
		$q = "SELECT post_id FROM blog_likes WHERE post_id = '$pid' AND user_id = '$uid'";
		$sql = mysql_query($q);
		$row = mysql_num_rows($sql);
		return $row;
	}
    public function blog_posts($page){		
		$results_per_page = "10";
		$a = mysql_query("SELECT post_id FROM blog");
		$rows = mysql_num_rows($a);
		//echo $rows; die();
		$total_pages = ceil($rows/$results_per_page);
		$_SESSION['total_pages'] = $total_pages;		
		if($page == 1 || $page == "" || $page > $rows || !is_int($page)){			
			$limit = "LIMIT 0,{$results_per_page}";
		}
		else {			
			$foo = ($page*$results_per_page)-$results_per_page;			
			$limit = "LIMIT {$foo},{$results_per_page}";			
		}
        $q = "SELECT blog.post_id,blog.title,blog.desc,
		      blog.cat,blog.date,blog.votes,blog.comments,
		      users.name,users.pic
			  FROM blog
			  INNER JOIN users ON blog.user_id=users.id			  
              ORDER BY post_id DESC {$limit}";
			 //echo $q; exit();
        $sql = mysql_query($q);
        while($r = mysql_fetch_assoc($sql)){
            $posts[] = $r;
        }
        return $posts;
    }
	public function blog_comments($id){
		$q = "SELECT blog_comments.comment_id,blog_comments.comment,
		      blog_comments.comment_date,users.id,users.name,users.pic
		      FROM blog_comments
			  INNER JOIN users
			  ON blog_comments.user_id=users.id
			  WHERE post_id='$id'";
		$sql = mysql_query($q);
		while($r = mysql_fetch_assoc($sql)){
			$comments[] = $r;			
		}
		return $comments;
	}
	public function add_post($post_data){
		extract($post_data);
		$date = date("Y-m-d H:i:s");
		$q = "INSERT INTO blog VALUES('','$user_id','$title','$desc','$cat','$date','0','0','0')";
		$sql = mysql_query($q);
		if($sql){
			$update = mysql_query("UPDATE users SET posts=posts+1 WHERE user_id=$user_id");
			return 'success';
		}
	}
	public function rply($data){
		extract($data);
		$date = date("Y-m-d");
		$q = "INSERT INTO blog_comments VALUES('','$user_id','$post_id','$reply','$date')";
		$sql = mysql_query($q);
		if($sql){
			$a = "UPDATE blog SET comments=comments+1 WHERE post_id='$post_id'";
			$b = mysql_query($a);
			if($b){
				return 'success';
			}
		}
	}
	public function cat_count(){
		$q = "select cat from blog";
		$sql = mysql_query($q);
		while($r = mysql_fetch_assoc($sql)){
			$cat[] = $r;
		}
		return $cat;
	}
	public function blog_cat($cat){
		$q = "SELECT blog.post_id,blog.title,blog.desc,
		      blog.cat,blog.date,blog.votes,blog.comments,
		      users.name
			  FROM blog
			  INNER JOIN users
			  ON blog.user_id = users.id
			  WHERE cat = '$cat'";
		$sql= mysql_query($q);
		while($r = mysql_fetch_assoc($sql)){
			$posts[] = $r;
		}
		return $posts;
	}
	public function del_comment($cid,$uid){
		$q = "DELETE FROM blog_comments WHERE comment_id = '$cid' AND user_id = '$uid'";
		$sql = mysql_query($q);
	}
    
    ############# LIKE QUESTION && ANSWER ##########
    
    public function like($type,$q_id,$u_id){        
        if($type == 'question'){
			$a = "SELECT ques_id,user_id FROM ques_likes WHERE ques_id='$q_id' AND user_id='$u_id'";
        $b = mysql_query($a);
        $rows = mysql_num_rows($b);
        //echo $rows ; die();
        if($rows >= 1){
            return 'already_liked';
        }
        else {
            $q = "INSERT INTO ques_likes VALUES ('','$q_id','$u_id')";
            $sql = mysql_query($q);
            if($sql){
                $set = "UPDATE questions SET votes=votes+1 WHERE ques_id='$q_id'";
                $update = mysql_query($set);
                if($update){
                    return 'liked';
                }
            }
        }
		}
		elseif($type == 'answer'){
			//confusion in selecting ans or question id from the table ans likes
		$a = "SELECT ans_id,user_id FROM ans_likes WHERE ans_id='$q_id' AND user_id='$u_id'";
        $b = mysql_query($a);
        $rows = mysql_num_rows($b);
        //echo $rows ; die();
        if($rows >= 1){
            return 'already_liked';
        }
        else {
            $q = "INSERT INTO ans_likes VALUES ('','$q_id','$u_id')";
            $sql = mysql_query($q);
            if($sql){
                $set = "UPDATE answers SET votes=votes+1 WHERE ans_id='$q_id'";
                $update = mysql_query($set);
                if($update){
                    return 'liked';
                }
            }
        }
		}
		elseif($type == 'blog'){
			//confusion in selecting ans or question id from the table ans likes
		$a = "SELECT post_id,user_id FROM blog_likes WHERE post_id='$q_id' AND user_id='$u_id'";
        $b = mysql_query($a);
        $rows = mysql_num_rows($b);
        //echo $rows ; die();
        if($rows >= 1){
            return 'already_liked';
        }
        else {
            $q = "INSERT INTO blog_likes VALUES ('','$q_id','$u_id')";
            $sql = mysql_query($q);
            if($sql){
                $set = "UPDATE blog SET votes=votes+1 WHERE post_id='$q_id'";
                $update = mysql_query($set);
                if($update){
                    return 'liked';
                }
            }
        }
		}
    }
	public function qlike_status($uid,$qid){
		$q = "SELECT id FROM ques_likes WHERE ques_id='$qid' AND user_id='$uid'";
		$sql = mysql_query($q);
		$rows = mysql_num_rows($sql);
		return $rows;
	}
    
     ############# // LIKE QUESTION ##########
	 
	 ########## UNLIKE question answer blogpost######
	 public function unlike($type,$q_id,$u_id){        
        if($type == 'question'){
			$a = "SELECT ques_id,user_id FROM ques_likes WHERE ques_id='$q_id' AND user_id='$u_id'";
        $b = mysql_query($a);
        $rows = mysql_num_rows($b);
        //echo $rows ; die();
        if($rows == 0){
            return 'already_unliked';
        }
        else {
            $q = "DELETE FROM ques_likes WHERE ques_id='$q_id' AND user_id='$u_id'";
            $sql = mysql_query($q);
            if($sql){
                $set = "UPDATE questions SET votes=votes-1 WHERE ques_id='$q_id'";
                $update = mysql_query($set);
                if($update){
                    return 'unliked';
                }
            }
        }
		}
		elseif($type == 'answer'){
			//confusion in selecting ans or question id from the table ans likes
		$a = "SELECT ans_id,user_id FROM ans_likes WHERE ans_id='$q_id' AND user_id='$u_id'";
        $b = mysql_query($a);
        $rows = mysql_num_rows($b);
        //echo $rows ; die();
        if($rows >= 1){
            return 'already_liked';
        }
        else {
            $q = "INSERT INTO ans_likes VALUES ('','$q_id','$u_id')";
            $sql = mysql_query($q);
            if($sql){
                $set = "UPDATE answers SET votes=votes+1 WHERE ans_id='$q_id'";
                $update = mysql_query($set);
                if($update){
                    return 'liked';
                }
            }
        }
		}
		elseif($type == 'blog'){
			
		$a = "SELECT post_id,user_id FROM blog_likes WHERE post_id='$q_id' AND user_id='$u_id'";
        $b = mysql_query($a);
        $rows = mysql_num_rows($b);        
        if($rows == 0){
            return 'already_unliked';
        }
        else {
			$q = "DELETE FROM blog_likes WHERE post_id='$q_id' AND user_id='$u_id'";
            //$q = "INSERT INTO blog_likes VALUES ('','$q_id','$u_id')";
            $sql = mysql_query($q);
            if($sql){
                $set = "UPDATE blog SET votes=votes-1 WHERE post_id='$q_id'";
                $update = mysql_query($set);
                if($update){
                    return 'unliked';
                }
            }
        }
		}
    }
    
	########## UNLIKE question answer blogpost######
	
    public function register($data){
        extract($data);
        $a = "SELECT email FROM users WHERE email = '$email'";
        $b = mysql_query($a);
        $rows = mysql_num_rows($b);
        if($rows > 0){
            return 'already';        
        }
        $q = "INSERT INTO users VALUES('','$name','$pic','$email','$password','$country','$state','$skills','0','0','0','1')";
        $sql = mysql_query($q);
        if($sql) {
            return 'success';
        }
        else {
            return 'failure';
        }
    }
    public function ask_question($data){		
        extract($data);
		$date = date('Y-m-d H:i:s');
        $q = "INSERT INTO questions VALUES('','$user_id','$title','$desc','$tags','0','0','$date')";
        $sql = mysql_query($q);
        if($sql) {
			$update = mysql_query("UPDATE users SET ques=ques+1 WHERE id=$user_id");
            return 'success';
        }
        else {
            return 'error';
        }
    }
	public function read_post($id){
		$q = "SELECT blog.post_id,blog.title,blog.desc,blog.cat,
		      blog.date,blog.votes,blog.comments,blog.views,
		      users.id,users.name,users.pic
		      FROM blog
			  INNER JOIN users
			  ON blog.user_id=users.id
			  WHERE post_id='$id'";
		$sql = mysql_query($q);
		$post = mysql_fetch_assoc($sql);
		return $post;
	}
	public function increment_blog_view($pid){
		$update = "UPDATE blog SET views=views+1 WHERE post_id='$pid'";
		$sql = mysql_query($update);
	}
	public function search_engine($search){
		$q = "SELECT ques_id,title,tags
		      FROM questions
			  WHERE title
			  LIKE '%{$search}%'";
		$sql = mysql_query($q);
		while($r = mysql_fetch_assoc($sql)){
			$results[] = $r;
		}
		return $results;
	}
}
