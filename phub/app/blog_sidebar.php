<?php
 $count = array();
        foreach ($counting as $key => $value){
            foreach ($value as $key2 => $value2){
                $index = $value2;
                if (array_key_exists($index, $count)){
                    $count[$index]++;
                } else {
                    $count[$index] = 1;
                }
            }
        }        
?>
<h4><i class="fa fa-folder-o"></i> Categories</h4>
<ul class="list-group">
  <li class="list-group-item">
    <span class="badge"><?= $count['Coding'];?></span>
    <h5><a style="text-decoration: none;" href="blog_cat.php?cat=Coding"><b>Coding</b></a></h5>    
  </li>
   <li class="list-group-item">
    <span class="badge"><?= $count['Design'];?></span>
    <h5><a style="text-decoration: none;" href="blog_cat.php?cat=Design"><b>Design</b></a></h5>    
  </li>
   <li class="list-group-item">
    <span class="badge"><?= $count['Wordpress'];?></span>
    <h5><a style="text-decoration: none;" href="blog_cat.php?cat=Wordpress"><b>Wordpress</b></a></h5>    
  </li>
   <li class="list-group-item">
    <span class="badge"><?= $count['Ecommerce'];?></span>
    <h5><a style="text-decoration: none;" href="blog_cat.php?cat=Ecommerce"><b>Ecommerce</b></a></h5>    
  </li>
   <li class="list-group-item">
    <span class="badge"><?= $count['Freelance'];?></span>
    <h5><a style="text-decoration: none;" href="blog_cat.php?cat=Freelance"><b>Freelance</b></a></h5>    
  </li>
  <li class="list-group-item">
    <span class="badge"><?= $count['Technology'];?></span>
    <h5><a style="text-decoration: none;" href="blog_cat.php?cat=Technology"><b>Technology</b></a></h5>
  </li>
  <li class="list-group-item">
    <span class="badge"><?= $count['Inspiration'];?></span>
    <h5><a style="text-decoration: none;" href="blog_cat.php?cat=Inspiration"><b>Inspiration</b></a></h5>    
  </li>
  
  
 
</ul>