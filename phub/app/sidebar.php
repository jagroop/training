<div class="panel panel-default">
  <div class="panel-heading">Top 10 rated questions</div>
  <div class="panel-body">    
      <?php 
$counter = 1 ;
foreach($top_ten as $top):?>      
      <a style="text-decoration: none;" href="question.php?q=<?= $top['ques_id'];?>"><?= $counter++ ; ?>. <?= $top['title'];?></a>
      By: <span><a style="text-decoration: none;" href="user.php?user=<?= $top['id']?>"><?= $top['name'];?></a></span>
      <?php
      $foo  = explode(',',$top['tags']);     
      ?>
      <?php foreach($foo as $bar):?>
      <a style="text-decoration: none;" href="tag.php?tag_name=<?= $bar;?>"><span class="label label-default"><i class="fa fa-tag"></i> <?= $bar;?></span></a>
      <?php endforeach;?>
      <hr>
      <?php endforeach;?>    
  </div>
</div>
