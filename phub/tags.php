<?php
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['url'] = $current_link;
$tags = $obj->tags();
?>
<div class="row">
  <div class="col-md-2">
  
  </div>
  
  <div class="col-md-8">
    
    <?php foreach($tags as $tag):?>
  <div style="width: 100px; float: left;margin-left: 10px;" class="well well-sm">
    <h4><a style="text-decoration: none;" href="tag.php?tag_name=<?= $tag['tag_name'];?>"><i class="fa fa-tags"></i> <?= $tag['tag_name'];?></a></h4>
    <p><?= $tag['tag_desc'];?></p>
  </div>
  <?php endforeach;?>
    
  </div>
  
  <div class="col-md-2">
  
  </div>
</div>