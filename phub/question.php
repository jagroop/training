<?php
if(empty($_GET['q'])){
    header("Location:index.php");
}
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['url'] = $current_link;
$id = $_GET['q'];
$question = $obj->get_question($id);
if(empty($question)){
    header("Location:index.php");
}
if(isset($_SESSION['user_id'])){
   $qlike_status = $obj->qlike_status($_SESSION['user_id'],$id);   
}
if(isset($_POST['answer'])){
    
    $id = $_POST['ques_id'];
    $answerq = $_POST['answerq'];
    $user_id = $_POST['user_id'];
    $post_answer = $obj->post_answer($id,$answerq,$user_id);
}
?>
<style>
#nav.affix {
    background-color: #fafafa;
    border-bottom: 1px solid #e7e7e7;    
    position: fixed;
    top: 0;
    width: 100%;
    height:9%;
    z-index:10;
}
</style>
<div id="nav">
  <div class="navbar navbar-static">                  
        <div class="row" style="{float:left;}">
            <div style="color: #999; font-size: 15px;">               
                <?php if(empty($question['pic'])):?>
               <img class="img-circle" height="50" width="50" src="uploads/avatar.png" alt="<?= $question['name'];?>">
               <?php endif;?>.
                <?php if(!empty($question['pic'])):?>
               <img class="img-circle" height="50" width="50" src="uploads/<?= $question['pic'];?>" alt="<?= $question['name'];?>">
               <?php endif;?>                   
               <b><?= $question['name'];?></b> | <?= $obj->time_ago($question['date']);?>
    | <a class="text-danger" <?php if(!isset($_SESSION['user_id'])){echo "data-toggle='popover' title='You have to login first in order to like that question' data-content='<a href=".'login.php'." style=".'color:black;'."><b>login|register</b></a>' data-html='true' data-placement='bottom'";}?> style="text-decoration: none;" href="<?php if(!isset($_SESSION['user_id'])) {echo '#';} elseif($qlike_status == 0){ echo 'like.php';} else{ echo 'unlike.php';}?>?type=question&&ques_id=<?= $question['ques_id'];?>"><i <?php if($qlike_status == 0){echo "class='fa fa-heart-o'";} else{ echo "class='fa fa-heart'";}?>></i> <?= $question['votes'];?></a>&nbsp;
    | <a style="text-decoration: none;" href="#answers"><i class="fa fa-comments"></i> <?= $question['answers'];?></a>            
            <?php if(isset($_SESSION['user_id'])):?>
            <?php if($question['id'] == $_SESSION['user_id']):?>
    | <a style="text-decoration: none;" href="del.php?type=question&&user_id=<?=$question['id'];?>&&ques_id=<?=$question['ques_id'];?>" onclick="return confirm('Are you Sure ??');"><i class="fa fa-trash-o"></i></a>
    | <a href="#" style="text-decoration: none;" ><i class="fa fa-pencil"></i></a>
            <?php endif;?>
            <?php endif;?>      
    | <a style="text-decoration: none;" href="<?php if(!isset($_SESSION['user_id'])) { ; echo 'login.php?bummer&&ask_question';  } else{ echo'index.php#ask_question'; } ?>">
          <i class="fa fa-question"></i></a>               
            </div>
        </div>  
    </div>  
</div> <!--fixed navbar-->


<div class="row">
    <div class="col-md-1">
        
    </div>
    
   <!-- question-->
    
    <div class="col-md-10">
       <h2 class="text-primary"><b><?= $question['title'];?></b>&nbsp;&nbsp;<a href="#" data-toggle="tooltip" title="Found this question inappropriate vote here to close it"><span class="text-danger"><i class="fa fa-ban"></i></span></a>
       | <a href="#" data-toggle="tooltip" title="report abuse"><span class="text-info"><i class="fa fa-bullhorn"></i></span></a></h2><br>
       <p align="justify"><?= $question['desc'];?></p>
       
       <div class="well">
        <?php
        $answers = $obj->get_answers($id);        
        ?>
        
        <!--answers of a perticular question-->
        <div id="answers"></div>       
        <h4><span class="label label-success">Answers:</span></h4>
         <br>
       <?php if(empty($answers)):?>
       <h2>No answers posted yet <i class="fa fa-frown-o"></i></h2>
       <?php endif;?>
       <?php if(!empty($answers)):?>
        <?php foreach($answers as $answer):?>
        <div class="row">
            <div class="col-sm-2">
            <a style="text-decoration: none;" href="like.php?type=answer&&ques_id=<?= $question['ques_id'];?>&&ans_id=<?= $answer['ans_id'];?>"><i class="fa fa-thumbs-o-up"></i>&nbsp;<?= $answer['votes'];?></a>&nbsp;

            </div>
            <div class="col-md-10">
                 By:<?php if(!empty($answer['pic'])){ ?>        
        <img height="40" width="40" class="img-circle" src="uploads/<?= $answer['pic'];?>">
        <?php }
        else { ?>
            <img height="40" width="40" class="img-circle" src="uploads/avatar.png">
        <?php }
        ?>
       <a href="user.php?user=<?= $answer['id'];?>"><?= $answer['name'];?></a>  | <?= $answer['date_time'];?>

        <h4><?= $answer['answer'];?></h4> 
            </div>
        </div>
              
        <hr>
        <?php endforeach;?>
       <?php endif;?>
       </div>
       <?php if(isset($_SESSION['user_id'])){ ?>
        <div id="reply"></div>
        <h2>Answer This Question</h2>
       <form id="ck_cool" action="" method="post">
        <input type="hidden" name="ques_id" value="<?= $question['ques_id'];?>">
        <input type="hidden" name="user_id" value="<?= $_SESSION['user_id'];?>">
        <script src="assets/tinymce/tinymce.min.js"></script>
            <script type="text/javascript">
                tinymce.init({
                    selector: "#mytextarea",
                    theme: "modern",                    
                    height: 300,
                    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
             ],
                    style_formats: [        
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'},
    ],
                    setup: function (editor) {
                    editor.on('change', function () {
                    editor.save();
                     });
                  }
                });
            </script>
            Required*:
            <textarea id="mytextarea" name="answerq" required></textarea><br>
        <input type="submit" name="answer" class="btn btn-primary" value="Post Answer">
       </form>
       <?php }
       else { ?>
        <h3>please login to answer this question <a href="login.php">Login</a></h3>
       <?php }
       ?>
       
       <!--Edit the question-->       
       <?php if($question['id'] == $_SESSION['user_id']):?>
       <?php if(isset($_GET['edit_ques'])):?>
       <section id="edit_ques">
        <div class="well">
       holle
       </div>
       </section>
       <?php endif;?>
       <?php endif;?>
      <!--Edit the question-->
       
       
    </div>
    
    <div class="col-md-1">
        
    </div>
</div>
<?php include 'app/footer.php' ;?>
