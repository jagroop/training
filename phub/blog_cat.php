<?php
if(empty($_GET['cat'])){
    header("Location:blog.php");
}
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['url'] = $current_link;
$cat = $_GET['cat'];
$posts = $obj->blog_cat($cat);
if(empty($posts)){
    header("Location:blog.php");
}
function truncate($input, $maxWords, $maxChars)
{
    $words = preg_split('/\s+/', $input);
    $words = array_slice($words, 0, $maxWords);
    $words = array_reverse($words);

    $chars = 0;
    $truncated = array();

    while(count($words) > 0)
    {
        $fragment = trim(array_pop($words));
        $chars += strlen($fragment);

        if($chars > $maxChars) break;

        $truncated[] = $fragment;
    }

    $result = implode($truncated, ' ');

    return $result . ($input == $result ? '' : '...');
}
if(isset($_POST['add_post'])){
    //print_r($_POST); die();
    $post_data = [
                  'user_id' => $_POST['user_id'],
                  'title' => $_POST['title'],
                  'desc' => $_POST['desc'],
                  'cat' => $_POST['cat']
                  ];
    $add_post = $obj->add_post($post_data);
    if($add_post == 'success'){
        header("Location:blog.php");
    }
}
?>
<div class="row">
    <div class="col-md-1">
        
        <div style="position: fixed;">
        <h4><b><a style="text-decoration: none;" href="<?php if(!isset($_SESSION['user_id'])) { $_SESSION['url'] = 'http://localhost/phub/blog.php?add_post#do_add_post'; echo 'login.php?bummer';  } else{ echo'blog.php?add_post#do_add_post'; } ?>"><i class="fa fa-pencil-square-o"></i> Add Post</a></b></h4>
       </div>
        
    </div>
    <div class="col-md-8">
        <h2 class="text-primary"><b><u><?= $cat;?> :</u></b></h2>
        <div class="well">
           <?php foreach($posts as $post):?>
           <a style="text-decoration: none;" href="read_post.php?post_id=<?= $post['post_id'];?>"><h1 class="text-success"><b><?= $post['title'];?></b></h1></a>
           <i class="fa fa-user"></i> <?=$post['name'];?> | <i class="fa fa-calendar"></i> <?= $post['date'];?> | <i class="fa fa-folder-o"></i> <?= $post['cat'];?> | <i class="fa fa-heart-o"></i> <?= $post['votes'];?> | <i class="fa fa-comment-o"></i> <?= $post['comments'];?>
           <hr>
           <p><?= truncate($post['desc'],50,100);?></p>
           <a href="read_post.php?post_id=<?= $post['post_id'];?>" class="btn btn-primary">Continue Reading..</a>
           <br><br><br>
           <?php endforeach;?>
        </div>        
    </div>
    <div class="col-md-3">
        <?php $counting = $obj->cat_count();              
        ?>
        
        <?php include 'app/blog_sidebar.php'; ?>
    </div>    
</div>
