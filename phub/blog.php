<?php
ob_start();
require 'app/header.php';
require 'app/Functions.php';
$obj = new Functions;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['url'] = $current_link;

if(isset($_GET['page'])){
    $page = (int)$_GET['page'];
    $posts = $obj->blog_posts($page);
}
if(!isset($_GET['page'])) {
    $posts = $obj->blog_posts('1');
}
//function to display time ago post was added
function time_ago($date)

{

if(empty($date)) {

return "No date provided";

}

$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");

$lengths = array("60","60","24","7","4.35","12","10");

$now = time();

$unix_date = strtotime($date);

// check validity of date

if(empty($unix_date)) {

return "Bad date";

}

// is it future date or past date

if($now > $unix_date) {

$difference = $now - $unix_date;

$tense = "ago";

} else {

$difference = $unix_date - $now;
$tense = "from now";}

for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {

$difference /= $lengths[$j];

}

$difference = round($difference);

if($difference != 1) {

$periods[$j].= "s";

}

return "$difference $periods[$j] {$tense}";

}
function truncate($input, $maxWords, $maxChars)
{
    $words = preg_split('/\s+/', $input);
    $words = array_slice($words, 0, $maxWords);
    $words = array_reverse($words);

    $chars = 0;
    $truncated = array();

    while(count($words) > 0)
    {
        $fragment = trim(array_pop($words));
        $chars += strlen($fragment);

        if($chars > $maxChars) break;

        $truncated[] = $fragment;
    }

    $result = implode($truncated, ' ');

    return $result . ($input == $result ? '' : '...');
}
if(isset($_POST['add_post'])){
    //print_r($_POST); die();
    $post_data = [
                  'user_id' => $_POST['user_id'],
                  'title' => $_POST['title'],
                  'desc' => $_POST['desc'],
                  'cat' => $_POST['cat']
                  ];
    $add_post = $obj->add_post($post_data);
    if($add_post == 'success'){
        header("Location:blog.php");
    }
}
?>
<style>
#nav.affix {
    background-color: rgba(255,255,255,0.95);
    border-bottom: 1px solid #e7e7e7;    
    position: fixed;
    top: 0;
    width: 100%;
    height:8%;
    z-index:10;
}
</style>
    <!-- Fixed Navbar --><div id="nav">
  <div class="navbar navbar-static">
                  
        <div class="row">        
            <div class="col-md-2">
                
            </div>
            <div class="col-md-8">
                
            </div>
            <div class="col-md-2">
                <h3><b><a style="text-decoration: none;" href="<?php if(!isset($_SESSION['user_id'])) { ; echo 'login.php?bummer&&add_post';  } else{ echo'blog.php?add_post#do_add_post'; } ?>">
          &nbsp;&nbsp;<i class="fa fa-pencil-square-o"></i> Add Post</a></b></h3>
            </div>
    </div>  
</div> 
</div><!--fixed navbar-->
<div class="row">
    <div class="col-md-1">       
        
    </div>
    <div class="col-md-8">
        <div class="well">
           <?php foreach($posts as $post):?>
           <a style="text-decoration: none;" href="read_post.php?post_id=<?= $post['post_id'];?>"><h1 class="text-success"><b><?= $post['title'];?></b></h1></a>
           <i class="fa fa-user"></i> <?=$post['name'];?> |
           <i class="fa fa-clock-o"></i> <?= time_ago($post['date']);?> |
           <a style="text-decoration: none;" href="blog_cat.php?cat=<?= $post['cat'];?>"><i class="fa fa-folder-o"></i> <?= $post['cat'];?></a> |
           <i class="fa fa-heart-o"></i> <?= $post['votes'];?> |
           <i class="fa fa-comment-o"></i> <?= $post['comments'];?>
           <hr>
           <p><?= truncate($post['desc'],50,100);?></p>
           <br><br>
           <a href="read_post.php?post_id=<?= $post['post_id'];?>" class="btn btn-primary">Continue Reading..</a>
           <br><br><br>
           <?php endforeach;?>
          <ul id="pagination-demo" class="pagination"></ul>
        </div>
        
        <!--add post form-->
        <?php if(isset($_GET['add_post'],$_SESSION['user_id'])):?>
        <div id="do_add_post" class="well">
            <h3><i class="fa fa-pencil-square-o"></i> Add Post</h3><br>
        <form action="" method="post">
            <input type="hidden" name="user_id" value="<?= $_SESSION['user_id'];?>">            
            Category:
            <div class="form-group">
            <select class="form-control" name="cat">                
                <option value="Coding">Coding</option>
                <option value="Design">Design</option>
                <option value="Wordpress">Wordpress</option>
                <option value="Technology">Technology</option>
                <option value="Insipiration">Insipiration</option>
                <option value="Ecommerce">Ecommerce</option>
                <option value="Freelance">Freelance</option>
            </select></div><br>
            Title:
            <input name="title" class="form-control" placeholder="Post title" required><br>
            <script src="assets/tinymce/tinymce.min.js"></script>
            <script type="text/javascript">
                tinymce.init({
                    selector: "#mytextarea",
                    content_css: "assets/css/bootstrap.css",
                    theme: "modern",                    
                    height: 300,
                    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
             ],
                    style_formats: [        
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ],
                    setup: function (editor) {
                    editor.on('change', function () {
                    editor.save();
                     });
                  }
                });
            </script>
            Post Content:
            <p class="text-danger">Please do not add images of more than 700x390 resolution</p>
            <textarea id="mytextarea" name="desc" required></textarea><br>
            <input type="submit" class="btn btn-primary" name="add_post" value="POST">
        </form>
        </div>
        <?php endif;?>
        <!-- //add post form-->
    </div>
    <div class="col-md-3">
        
        <!--twitter timeline-->
        
        <a class="twitter-timeline" href="https://twitter.com/539794848a5b451" data-widget-id="622696085211910144">Tweets by @539794848a5b451</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        
         <!--twitter timeline-->
       
        <?php $counting = $obj->cat_count();
              
        ?>
        
        <?php include 'app/blog_sidebar.php'; ?>
    </div>    
</div>
<?php include 'app/footer.php' ;?>
