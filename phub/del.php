<?php
require 'app/Functions.php';
$obj = new Functions;
$type = $_GET['type'];
$user_id = $_GET['user_id'];
if((empty($type)) || (empty($user_id))) {
    header('Location:'.$_SESSION['url']);
}
if($type == 'question'){
    $qid = $_GET['ques_id'];
    if(empty($qid)){
        header('Location:'.$_SESSION['url']);
    }
    $del_ques = $obj->del_question($qid,$user_id);
    header("Location:index.php");
}
if($type == 'blog_comment'){
    $comment_id = $_GET['comment_id'];
    if(empty($comment_id)){
        header('Location:'.$_SESSION['url']);
    }
    $del_comment = $obj->del_comment($comment_id,$user_id);
    header('Location:'.$_SESSION['url'].'#comments');
}