<?php
require 'app/Crud.php';        
$admin = new Crud;
if(empty($_SESSION['admin_id'])){
    header("Location:index.php");
}
if(isset($_POST)){
    $type = $_POST['type'];    
    if($type == 'ban' || $type == 'activate'){
    $userOpId = $_POST['id'];
    $operation = $admin->user_operations($type,$userOpId);
    if($operation == 'banned'){
        echo 'banned';
    }
    elseif($operation == 'activated'){
        echo 'activated';
    }
    elseif($operation == 'error'){
        echo 'error';
    }
    }
    elseif ($type == 'verified' || $type == 'unverified'){
        $blogOpId = $_POST['post_id'];
        $blog_ver = $admin->blog_verification($type,$blogOpId);
         if($blog_ver == 'verified'){
        echo 'verified';
    }
    elseif($blog_ver == 'unverified'){
        echo 'unverified';
    }
    elseif($blog_ver == 'error'){
        echo 'error';
    }
    }
    elseif($type == 'add_tag'){        
        extract($_POST);        
        $addTag = $admin->add_tag($tag_name,$desc);
        if($addTag == 'success'){
            echo "success";
        }
        elseif($addTag == "error"){
            echo "error";
        }
    }
}