<?php session_start();

if(isset($_GET['logout'])){
    unset($_SESSION['admin_id']);
    header("Location:index.php");
    }?>
<html>
    <head>
        <title>Programmers HuB</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/font_awesome/css/font-awesome.css">
        <link rel="stylesheet" href="../assets/css/datatable.css">
        <link rel="stylesheet" href="../assets/css/dataTable.responsive.css">
        <link rel="stylesheet" href="../assets/css/switch.css">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Programmers HUB</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#users"><i class="fa fa-users"></i> Users <span class="sr-only">(current)</span></a></li>
        <li><a href="#blog"><i class="fa fa-rss-square"></i> Blog posts</a></li>
        <li><a href="#addTag"><i class="fa fa-tag"></i> Add Tag</a></li>
      </ul>
      
        <div class="navbar-right">       
        <a href="?logout" class="btn btn-default">Logout</a>
        </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>
      </ul>
    </div>
  </div>
</nav>