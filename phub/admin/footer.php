<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/bootstrap.js"></script>
<script src="../assets/js/datatable.min.js"></script>
<script src="../assets/js/dataTable.responsive.js"></script>
<script>           
    $(document).ready(function() {
    $('table.display').dataTable({
         responsive: true
        });
    });
</script>
<script>      
function userOperations(type,id,btnId) {   
   $.ajax({
      url: 'operations.php',
      cahe: false,
      type: 'post',
      data: {'type': type, 'id': id},     
      success: function(data){
      data = $.trim(data);       
       if (data == 'activated') {
       $("#"+btnId).prop('name', 'ban');
       $("#"+btnId).prop('class', 'btn btn-success btn-sm');
       $("#"+btnId).html("<b><i class='fa fa-check'></i></b>");
       }
       if (data == 'banned') {
       $("#"+btnId).prop('name', 'activate');
       $("#"+btnId).prop('class', 'btn btn-primary btn-sm');
       $("#"+btnId).html("<b><i class='fa fa-times'></i></b>");
       }       
      }
   });
   return false;
   }
   function blog_verification(type,btnId,postId){
       $.ajax({
      url: 'operations.php',
      cahe: false,
      type: 'post',
      data: {'type': type, 'post_id': postId},     
      success: function(data){
      data = $.trim(data);       
       if (data == 'verified') {
       $("#"+btnId).prop('name', 'unverified');
       $("#"+btnId).prop('class', 'btn btn-success btn-md');
       $("#"+btnId).html("Verified");
       }
       if (data == 'unverified') {
       $("#"+btnId).prop('name', 'verified');
       $("#"+btnId).prop('class', 'btn btn-danger btn-md');
       $("#"+btnId).html("Unverified");
       }
       if (data == 'error') {
              alert('error');
       }
      }
   });
   return false;
   }
    function tag_validation(){
       var counter=0;
       if ($("#tagInput").val() == '') {
              counter =1;
       }
       if ($("#tagDesc").val() == '') {
              counter=1;
       }
       if (counter == 1) {
              alert("Please fill all fields");
              return false;
       }
       else{
              add_tag();
       }
    }
   function add_tag(){       
       var dataString = $(".formClass").serialize();
       $.ajax({
      url: 'operations.php',
      type: 'post',
      data: 'type=add_tag&'+dataString,     
      success: function(data){       
      data = $.trim(data);      
       if (data == 'success') {
       alert('success');
       }
       if (data == 'error') {
       alert("error");
       }       
      }
   });
   return false;
   }
</script>
    </body>
</html>