<?php
require 'header.php';
require 'app/Crud.php';        
$admin = new Crud;
if(empty($_SESSION['admin_id'])){
    header("Location:index.php");
}
$counting = $admin->counting();
$users = $admin->all_users();
?>
<div class="container">
    <div class="row">
   <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><b><i class="fa fa-users"></i> Total Users</b></th>
                <th><b><i class="fa fa-bullhorn"></i> Total Questions</b></th>
                <th><b><i class="fa fa-meh-o"></i> Unans-Questions</b></th>
                <th><b><i class="fa fa-tags"></i> Total Tags</b></th>
                <th><b><i class="fa fa-rss-square"></i> Blog Posts</b></th>
                <th><b><i class="fa fa-star-half"></i> Unverfied posts</b></th>
            </tr>
        </thead>
        
        <tbody>
            <tr>
                <td><h3><?= $counting['users'];?></h3></td>
                <td><h3><?= $counting['questions'];?></h3></td>
                <td><h3><?= $counting['un_ans'];?></h3></td>
                <td><h3><?= $counting['tags'];?></h3></td>
                <td><h3><?= $counting['posts'];?></h3></td>
                <td><h3><?= $counting['unverified_posts'];?></h3></td>
            </tr>
        </tbody>
    </table>
</div>
</div>

    <div class="row">              
        <div class="col-md-12">
          <div class="panel panel-primary">
  <div class="panel-heading"><b><i class="fa fa-users"></i>  Users</b></div>
  <div class="panel-body">
    <div id="response"></div>
    <table id="id="users"" class="display table table-striped table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
            <th>#</th>
            <th>Image</th>
            <th>Name</th>
            <th>Email</th>
            <th>Country,State</th>
            <th>Skills</th>
            <th>Ques</th>
            <th>Ans</th>
            <th>Posts</th>
            <th>Actions</th>
            </tr>
        </thead>
        
        <tbody>
           <?php foreach($users as $user):?>           
            <tr>
            <td><?= ++$counter ;?></td>
            <td>
                <?php if(empty($user['pic'])):?>                
                <img src="../uploads/avatar.png" class="img-circle" height="21" width="21" alt="avatar.png">
                <?php endif;?>
                <?php if(!empty($user['pic'])):?>                
                <img src="../uploads/<?= $user['pic'];?>" class="img-circle" height="21" width="21" alt="<?= $user['name'];?>">
                <?php endif;?>
            </td>
            <td><?= $user['name'];?></td>
            <td><?= $user['email'];?></td>
            <td><?= $user['country'];?>, <?= $user['state'];?></td>
            <td><?= $user['skills'];?></td>
            <td><?= $user['ques'];?></td>
            <td><?= $user['ans'];?></td>
            <td><?= $user['posts'];?></td>
            <td> 
           <!--activate || deactivate--> <button name="<?php if($user['status'] == 0){echo "activate";} elseif($user['status'] == 1){echo "ban";}?>" id="<?= ++$id ;?>" value="<?= $user['id'];?>" onclick="userOperations(this.name,this.value,this.id);" class="<?php if($user['status'] == 0){echo "btn btn-primary btn-sm";} elseif($user['status'] == 1){echo"btn btn-success btn-sm";}?>">
            <b><?php if($user['status'] == 0){echo "<i class='fa fa-times'></i>";} elseif($user['status'] == 1){echo"<i class='fa fa-check'></i>";}?></b>
            </button><!--activate || deactivate-->
            <button class="btn btn-danger btn-sm" type="submit" value="<?php echo $user['id'];?>" onclick="return confirm('Are you sure?');" name="del"><b><i class="fa fa-trash"></i></b></button>                
            </td>
            </tr>
           <?php endforeach;?>
        </tbody>
    </table>
  </div>
</div>
        </div>       
    </div>
    <hr>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-rss-square"></i> Blogs</h3>
  </div>
  <div class="panel-body">
    <table id="blog" class="display table table-striped table-hover" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Title</th>
            <th>Category</th>
            <th>Date</th>
            <th>User</th>            
            <th>Action</th>
        </tr>    
        </thead>
        
        <tbody>
            <?php
            $posts = $admin->posts();
            foreach($posts as $post):
            ?>
            <tr>
               <td><?= $post['title'];?></td>
               <td><?= $post['cat'];?></td>
               <td><?= $post['date'];?></td>
               <td><?= $post['name'];?></td>
               <td>
                <!-- post status --><button name="<?php if($post['status'] == 0) {echo "unverified";} elseif($post['status'] == 1){echo "verified";}?>" id="<?= ++$counter;?>" value="<?= $post['post_id'];?>" onclick="blog_verification(this.name,this.id,this.value)" class="<?php if($post['status'] == 0){ echo "btn btn-danger btn-md";}elseif($post['status'] == 1){echo "btn btn-success btn-md";}?>">
                    <?php if($post['status'] == 0){ echo "Unverified";} elseif($post['status'] == 1){ echo "Verified";}?>
                </button><!-- post status -->
                <button name="del" id="" class="btn btn-primary btn-md">Delete</button>
               </td>               
            </tr>
            <?php endforeach;?>
        </tbody>
        
        
    </table>
  </div>
</div>
    </div>
</div>
<div class="container-fluid">
<div class="row">
    <div class="col-md-4">
        <form id="myform" class="formClass" method="post">
        <h3><i class="fa fa-tags"></i> Add Tag</h3>      
        Tag Name: <input id="tagInput" type="text" name="tag_name" class="form-control"><br><br>
        Description: <textarea id="tagDesc" name="desc" class="form-control" rows="6"></textarea><br><br>
        <button type="submit" class="btn btn-primary" onclick="tag_validation()" name="add">Add</button>
        </form>     
    </div>
    
    <div class="col-md-4">
        
    </div>
    
    <div class="col-md-4">
        
    </div>
</div>
</div>
<?php include 'footer.php';?>