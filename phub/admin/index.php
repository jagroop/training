<?php
require 'header.php';
require 'app/Crud.php';        
$admin = new Crud;
if(!empty($_SESSION['admin_id'])){
    header("Location:admin.php");
}
if(isset($_POST['login'])){
    $login = $admin->login($_POST['name'],$_POST['password']);
    if($login == 'failure'){ ?>
        <div class="col-md-3">
            <div class="alert alert-dismissible alert-danger">  
  <strong>Wrong</strong>&nbsp;Username or password !
</div>
        </div>
   <?php }
}
?>
<div class="well">
    <h2 align="center"><b>Programmers HuB</b></h2>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            
        </div>
        
        <div class="col-md-6">
            
            
            <form class="form-horizontal" action="" method="post">
  <fieldset>
    <legend>Login</legend>
    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Email</label>
      <div class="col-lg-10">
        <input type="text" name="name" class="form-control" id="inputEmail" placeholder="Email">
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword" class="col-lg-2 control-label">Password</label>
      <div class="col-lg-10">
        <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password"><br>
        <input type="submit" name="login" value="Login" class="btn btn-primary">
        </div>      
    </div>    
         </fieldset>   
            </form>
        </div>
        
        <div class="col-md-3">
            
        </div>
    </div>
</div>
