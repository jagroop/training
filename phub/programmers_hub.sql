-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 05, 2015 at 11:57 PM
-- Server version: 5.6.24-0ubuntu2
-- PHP Version: 5.6.4-4ubuntu6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `programmers_hub`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
`ans_id` int(100) NOT NULL,
  `ques_id` int(50) NOT NULL,
  `answer` text NOT NULL,
  `user_id` int(100) NOT NULL,
  `date_time` datetime NOT NULL,
  `votes` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`ans_id`, `ques_id`, `answer`, `user_id`, `date_time`, `votes`) VALUES
(1, 1, '<p>i dont know</p>\r\n', 2, '2015-07-02 06:27:42', '2'),
(2, 1, '<p>test</p>\r\n', 2, '2015-07-03 09:43:50', '2'),
(3, 1, '<p>counting test..</p>\r\n', 2, '2015-07-03 05:24:20', '2'),
(4, 3, '<p>no bro you will find it on google easly&nbsp;<img alt="fgh" src="http://www.unixstickers.com/image/cache/data/stickers/python/python.sh-600x600.png" style="float:right; height:100px; width:100px" /></p>\r\n', 2, '2015-07-03 05:48:05', '0'),
(5, 1, '<p>bummer</p>\r\n', 1, '2015-07-05 06:29:13', '2'),
(6, 2, '<p>bhj</p>\r\n', 1, '2015-07-05 06:54:41', '1'),
(11, 2, '<p>taset</p>\r\n', 1, '2015-07-05 07:05:46', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ans_likes`
--

CREATE TABLE IF NOT EXISTS `ans_likes` (
`id` int(100) NOT NULL,
  `ans_id` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ans_likes`
--

INSERT INTO `ans_likes` (`id`, `ans_id`, `user_id`) VALUES
(1, '1', '1'),
(2, '3', '1'),
(3, '2', '1'),
(4, '5', '1'),
(5, '11', '1'),
(6, '6', '1'),
(7, '1', '4'),
(8, '5', '4'),
(9, '3', '4'),
(10, '2', '4');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
`post_id` int(30) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `cat` varchar(100) NOT NULL,
  `date` varchar(30) NOT NULL,
  `votes` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`post_id`, `user_id`, `title`, `desc`, `cat`, `date`, `votes`) VALUES
(1, '1', 'Top misconceptions about programming lanuages.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam tincidunt mauris eu risus. Vestibulum auctor dapibus neque. Nunc dignissim risus id metus. Cras ornare tristique elit. Vivamus vestibulum nulla nec ante. Praesent placerat risus quis eros. Fusce pellentesque suscipit nibh. Integer vitae libero ac risus egestas placerat. Vestibulum commodo felis quis tortor.', 'Inspiration', '2015-07-01', '0'),
(2, '2', 'How to Choose a Programming Language', 'How you should choose a programming language: don’t. Wait . . . what about the high paying tech jobs and flexible work schedules out there? Isn’t this the best time to learn how to code? It is, and you should. Many people …', 'Programming', '2015-07-01', '0');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments`
--

CREATE TABLE IF NOT EXISTS `blog_comments` (
`id` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `post_id` int(50) NOT NULL,
  `comment` text NOT NULL,
  `comment_date` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_comments`
--

INSERT INTO `blog_comments` (`id`, `user_id`, `post_id`, `comment`, `comment_date`) VALUES
(1, '1', 1, 'good ... tanks for posting !!', '12/12/2015'),
(2, '1', 2, 'post id 2 test', '2015-07-05'),
(3, '4', 2, 'by kk', '2015-07-05'),
(4, '4', 1, 'my pleasure', '2015-07-05');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
`ques_id` int(100) NOT NULL,
  `user_id` int(30) NOT NULL,
  `title` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `tags` varchar(200) NOT NULL,
  `votes` varchar(10) NOT NULL,
  `answers` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`ques_id`, `user_id`, `title`, `desc`, `tags`, `votes`, `answers`) VALUES
(1, 2, 'what is the default life time of session in php ??', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'php,apache', '2', 4),
(2, 1, 'what ob_start() means in php ?', 'Dsed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\nLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\n\nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.\n\nUt wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\n\nNam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\n\nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.\n\nAt vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.', 'php,javascript', '1', 6),
(3, 2, 'what are tuples in python ??', '<blockquote>\r\n<p>what are tuples in python??</p>\r\n</blockquote>\r\n\r\n<pre>\r\ni have googled alot but did nt get the correct answer </pre>\r\n\r\n<p><del>please rply as soon as possible</del></p>\r\n\r\n<p><del><span class="marker">jagroop singh</span></del></p>\r\n', 'pyhton', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ques_likes`
--

CREATE TABLE IF NOT EXISTS `ques_likes` (
`id` int(50) NOT NULL,
  `ques_id` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ques_likes`
--

INSERT INTO `ques_likes` (`id`, `ques_id`, `user_id`) VALUES
(1, '1', '1'),
(2, '2', '1'),
(3, '1', '4');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
`id` int(100) NOT NULL,
  `tag_name` varchar(50) NOT NULL,
  `tag_desc` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag_name`, `tag_desc`) VALUES
(1, 'php', 'PHP is a server-side scripting language created in 1995 and designed for web development but also used as a general-purpose programming language.'),
(2, 'javascript', 'JavaScript is the programming language of HTML and the Web.'),
(3, 'pyhton', 'Python is a widely used general-purpose, high-level programming language. Its design philosophy emphasizes code readability,'),
(4, 'html', 'HyperText Markup Language, commonly referred to as HTML, is the standard markup language used to create web pages.'),
(5, 'apache', 'The Apache Software Foundation is an American non-profit corporation to support Apache software projects,'),
(6, 'linux', 'Linux is a Unix-like and mostly POSIX-compliant computer operating system (OS) assembled under the model of free and open-source software......'),
(7, 'jquery', 'jQuery is a cross-platform JavaScript library designed to simplify the client-side scripting of HTML'),
(8, 'bootstrap', 'Bootstrap, a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.'),
(9, 'angularjs', 'AngularJS is what HTML would have been, had it been designed for building web-apps. Declarative templates with data-binding,');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pic` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `skills` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `pic`, `email`, `password`, `country`, `state`, `skills`) VALUES
(1, 'Jagroop singh', 'geek.jpg', 'admin@admin.com', 'rooplakhnia', 'India', 'Punjab', 'php,javascript,jquery'),
(2, 'Yadwinder singh', 'stock-vector-geek-character-171742319.jpg', 'yad@gmail.com', 'yadbhullar', 'India', 'Punjab', 'php,jquery'),
(3, 'Vijay Dhiman', 'User.png', 'vijay@gmail.com', '123456', 'India', 'Haryana', 'php'),
(4, 'Krishan Kumar', '', 'kk@gmail.com', 'kk', 'india', 'punjab', 'php,jquery');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
 ADD PRIMARY KEY (`ans_id`);

--
-- Indexes for table `ans_likes`
--
ALTER TABLE `ans_likes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
 ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `blog_comments`
--
ALTER TABLE `blog_comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
 ADD PRIMARY KEY (`ques_id`);

--
-- Indexes for table `ques_likes`
--
ALTER TABLE `ques_likes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
MODIFY `ans_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ans_likes`
--
ALTER TABLE `ans_likes`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
MODIFY `post_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `blog_comments`
--
ALTER TABLE `blog_comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
MODIFY `ques_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ques_likes`
--
ALTER TABLE `ques_likes`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
