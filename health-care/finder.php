<?php
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
session_start();
include 'app/header.php';
$location = $_SESSION['location'];
if(empty($location)){
   header("Location:index.php");
   return;
}
if($current_link == "http://localhost/ihealth/finder.php"){
unset($_SESSION['current_url']);
unset($_SESSION['ExploreEmptyRedirect']);
}
$_SESSION['current_url'] = $current_link;
// calculate the distance by using this function ==>>
function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

// <<== calculate the distance by using this function
?>
<style>
    body{
        background-color: #F6F6F6;
    }
</style>
<div class="container-fluid">
    <!--search form-->
    <div class="row">
        
        <div class="col-md-2">
            
        </div>
        
        <div class="col-md-8">
            <form action="" method="get">
            <div class="row">
                <div class="col-sm-10">
                    <input id="findwhat" class="form-control" type="text" name="q" value="<?php if(isset($_GET['q'])){echo $_GET['q'];}?>" autofocus="on" autocomplete="off" placeholder="What Are you Looking For ?" required/>
                </div>
                <div class="col-sm-2">
            <button type="submit" onclick="ifinder()" name="getResults" value="RadarSearch" class="btn btn-success"><i id="ifinder" class="fa fa-search"></i> <b>Search</b></button>
            </div>
            </div>
            </form>
        </div>
        
        <div class="col-md-2">
            
        </div>
        
    </div>
    <!--//search form-->
    <br/><br/>
    <div class="row">
        <div class="col-md-2">
        <div class="row">
         <div class="col-xs-5">
            <?php
            if(!empty($_SESSION['location'])){
               ?>
               <!-- refresh Location -->
            <a onclick="refreshLocation();" href="javascript:void(0)" data-toggle="tooltip" data-placement="right" title="" data-original-title="Click here to Refersh your Location" style="text-decoration: none;">
                <div class="panel2 mini-box2">
                  <span id="refreshLocation" class="box-icon bg-info">
                    <i class="text-danger fa fa-refresh fa-2x"></i>
                  </span>
                </div>
            </a>
             <!-- refresh Location -->
               <?php
            } else{
               ?>
               <h4 id="geolocation"><button onclick="getLocation()" class="label label-danger"><i class="fa fa-map-marker"></i> Can't identify Your Location</button></h4>
               <?php
            }
            ?>
         </div>
        </div>
        </div>
        <!--swatches starts here-->
        <div class="col-md-8">
           <div class="row">
            <!--swatch-->
             <div class="col-sm-4">
            <a onclick="spin('hpNearby');" href="?lookupfor=hospital#nearby" style="text-decoration: none;">
                <div class="panel mini-box">
                <span id="hpNearby" class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/hp.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Hospitals</p>
                    <p class="text-muted"><span data-i18n="New users">Find Hospitals Nearby</span></p>
                </div>
              </div>
            </a>
             </div>
             <!--//swatch-->
             
             <!--swatch-->
             <div class="col-sm-4">
            <a onclick="spin('medsNearby');" href="?lookupfor=pharmacy#nearby" style="text-decoration: none;">
                <div class="panel mini-box">
                <span id="medsNearby" class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/meds.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Medical Stores</p>
                    <p class="text-muted"><span data-i18n="New users">Find Med Stores Nearby</span></p>
                </div>
              </div>
            </a>
             </div>
             <!--//swatch-->
             
             <!--swatch-->
             <div class="col-sm-4">
            <a onclick="spin('DentistNearBy');" href="?lookupfor=dentist#nearby" style="text-decoration: none;">
                <div class="panel mini-box">
                <span id="DentistNearBy" class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/dentist.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Dentists</p>
                    <p class="text-muted"><span data-i18n="New users">Find Dentists Nearby</span></p>
                </div>
              </div>
            </a>
             </div>
             <!--//swatch-->
             
             <!--swatch-->
             <div class="col-sm-4">
            <a onclick="spin('docNearBY');" href="?lookupfor=doctor#nearby" style="text-decoration: none;">
                <div class="panel mini-box">
                <span id="docNearBY" class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/doc.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Doctors</p>
                    <p class="text-muted"><span data-i18n="New users">Find Doctors Nearby</span></p>
                </div>
              </div>
            </a>
             </div>
             <!--//swatch-->
             
             <!--swatch-->
             <div class="col-sm-4">
            <a onclick="spin('physiotherapist');" href="?lookupfor=physiotherapist#nearby" style="text-decoration: none;">
                <div class="panel mini-box">
                <span id="physiotherapist" class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/pysio.jpg" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Physiotherapist</p>
                    <p class="text-muted"><span data-i18n="New users">physiotherapist Nearby</span></p>
                </div>
              </div>
            </a>
             </div>
             <!--//swatch-->
             
             <!--swatch-->
             <div class="col-sm-4">
            <a onclick="spin('foodNearBy');" href="?lookupfor=food#nearby" style="text-decoration: none;">
                <div class="panel mini-box">
                <span id="foodNearBy" class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/foodicon.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Food</p>
                    <p class="text-muted"><span data-i18n="New users">Find Food Nearby</span></p>
                </div>
              </div>
            </a>
             </div>
             <!--//swatch-->
             
             <!--swatch-->
             <div class="col-sm-4">
            <a onclick="spin('spa');" href="?lookupfor=spa#nearby" style="text-decoration: none;">
                <div class="panel mini-box">
                <span id="spa" class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/spa.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Spa</p>
                    <p class="text-muted"><span data-i18n="New users">Spa Nearby</span></p>
                </div>
              </div>
            </a>
             </div>
             <!--//swatch-->
             
             <!--swatch-->
             <div class="col-sm-4">
            <a onclick="spin('gymNearBy');" href="?lookupfor=gym#nearby" style="text-decoration: none;">
                <div class="panel mini-box">
                <span id="gymNearBy" class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/ex.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Gym</p>
                    <p class="text-muted"><span data-i18n="New users">Gym Nearby</span></p>
                </div>
              </div>
            </a>
             </div>
             <!--//swatch-->
             
             <!--swatch-->
             <div class="col-sm-4">
            <a onclick="spin('petCareID');" href="?lookupfor=veterinary_care#nearby" style="text-decoration: none;">
                <div class="panel mini-box">
                <span id="petCareID" class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/petcare.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Pet Care</p>
                    <p class="text-muted"><span data-i18n="New users">Veterinary care Nearby</span></p>
                </div>
              </div>
            </a>
             </div>
             <!--//swatch-->            
           </div>
        </div>
        <!--swatches ends here-->
        <div class="col-md-2">        
        <a href="<?php echo (isset($_SESSION['user_id'])) ? "MyFav.php?uid={$_SESSION['user_id']}" : "javascript:void(0);" ;?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo (!isset($_SESSION['user_id'])) ? "You have to Login First!" : "Your Favorites Places at One Place" ;?>">                
                    <i class="fa fa-heart-o"> My favorites</i>              
         </a>      
        </div>
    </div>    
    <!--search results-->
    <div class="row">
        <div class="col-md-2">
         
        </div>        
        <div class="col-md-8">
            <?php
            if(isset($_REQUEST['lookupfor']) && !empty($_REQUEST['lookupfor']) || isset($_REQUEST['q']) && !empty($_REQUEST['q'])){
               $radius = 5000;
               if(isset($_REQUEST['exploreMore']) && !empty($_REQUEST['exploreMore'])){$radius = 50000;}
               if(isset($_REQUEST['lookupfor'])){
                $lookupfor = htmlentities(htmlspecialchars(strip_tags($_REQUEST['lookupfor'])));
                $type = array('hospital','pharmacy','dentist','food','doctor','spa','gym','veterinary_care','physiotherapist');
            if(!in_array($lookupfor,$type)){
                 die("No results Found");              
            }            
            $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={$location}&radius={$radius}&types=".$lookupfor."&key=API_KEY";
               }
            if(isset($_REQUEST['q'])){
                $q = htmlentities(htmlspecialchars(strip_tags($_REQUEST['q'])));                            
                $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?location={$location}&radius=5000&types=food|cafe|hospital|gym|health|pharmacy|dentist|doctor|veterinary_care&query=".urlencode($q)."&key=API_KEY";
                
                }
            $getJsonResponse = file_get_contents($url);
            $results = json_decode($getJsonResponse,TRUE);            
            if(empty($results['results'])){
               if(isset($_REQUEST['exploreMore']) && !empty($_SESSION['ExploreEmptyRedirect'])){
                  header("Location:".$_SESSION['ExploreEmptyRedirect']);
                  }
                ?>
                <!--No results Found-->
                <div id="SearchResults"></div>
                <div class="panel mini-box">
                <span class="box-icon bg-info">
                    <span class="text-danger"><i class="fa fa-thumbs-o-down fa-2x"></i></span>
                </span>
                <div class="box-info">
                    <p class="size-h2">No Results Found</p>
                    <p class="text-muted"></p>
                    <br>
                    <b><h5 class="text-primary">Possible reasons:</h5></b>
                    <ul>
                     <?php if(isset($_REQUEST['q'])):?>
                     <li class="list-group-item"><i class="fa fa-search"></i> Unable to find Results for <span class="text-primary"><u><?php echo $_REQUEST['q'];?></u></span></li>
                     <?php endif;?>
                     <li class="list-group-item"><i class="fa fa-globe"></i> No internet connection</li>
                     <li class="list-group-item"><i class="fa fa-ban"></i> You Deneid the request of geolocation</li>                     
                     <li class="list-group-item"><i class="fa fa-map-marker"></i> Unable to Detect Your Location</li>
                     <li class="list-group-item"><i class="fa fa-clock-o"></i> Request Timed Out</li>                     
                    </ul>
                </div>
              </div>            
                <!--//No results Found-->
                <?php
            }
            else{
               if(isset($_REQUEST['lookupfor']) && !empty($_REQUEST['lookupfor']) || isset($_REQUEST['q']) && !empty($_REQUEST['q']))
               {
                  $_SESSION['ExploreEmptyRedirect'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
               }             
                ?>
                <div id="SearchResults"></div>
                <!--search results here-->
                
                <h3><span class="label label-info"><?php echo count($results['results']);?> <?php echo (isset($_REQUEST['exploreMore'])) ? '<u>more</u>' : '';?> Search results for&nbsp;<?php if(isset($lookupfor)) echo $lookupfor;?> <?php if(isset($_REQUEST['q'])) echo $_REQUEST['q'];?></span></h3>
                <div style="float:right;">
                  <span class="label label-default"><i class="fa fa-sort-amount-desc"></i> by popularity</span>
                </div>
                <br/>
                <?php foreach($results['results'] as $result):?>
                <!--swatch-->             
                <a href="?place_details=<?php echo $result['place_id'];?>&<?= (isset($_REQUEST['lookupfor'])) ? "searchType=lookupfor&favo=".$_REQUEST['lookupfor'] : "searchType=q&favo=".$_REQUEST['q'] ;?>" style="text-decoration: none;">
                <div class="panel mini-box">
                <span class="box-icon bg-info">
                    <span class="text-<?php if($lookupfor == 'hospital'){echo 'success';}elseif($lookupfor == 'pharmacy'){echo "primary";}elseif($lookupfor == 'dentist'){echo "info";}elseif($lookupfor == 'food'){echo "warning";}elseif($lookupfor == 'doctor'){echo "danger";}elseif($lookupfor == 'veterinary_care'){echo "success";}elseif($lookupfor == 'health'){echo "info";}elseif(isset($_REQUEST['q'])){echo "primary";}elseif($lookupfor == 'physiotherapist'){echo "danger";}elseif($lookupfor == 'spa'){echo "primary";}?>">
                    <?php if($lookupfor == 'gym'):?>
                    <img class="img-circle" src="assets/PNG/ex.png" height="70" width="70" alt="">&nbsp;&nbsp;
                    <?php endif;?>
                    <i class="fa fa-<?php if($lookupfor == 'hospital'){echo 'hospital-o';} elseif($lookupfor == 'pharmacy'){echo "medkit";}elseif($lookupfor == 'dentist'){echo "smile-o";}elseif($lookupfor == 'food'){echo "cutlery";}elseif($lookupfor == 'doctor'){echo "user-md";}elseif($lookupfor == 'veterinary_care'){echo "paw";}elseif($lookupfor == 'health'){echo "child";}elseif(isset($_REQUEST['q'])){echo "map-marker";}elseif($lookupfor == 'physiotherapist'){echo "wheelchair";}elseif($lookupfor == 'spa'){echo "scissors";}?>">                        
                    </i></span>
                </span>
                <div class="box-info">
                    <p class="size-h2"><?php echo $result['name'];?></p>
                    <div style="float: right;">
                    <?php if(isset($result['opening_hours']) && $result['opening_hours']['open_now'] == "1"):?>
                    <p class="text-muted"><span class="text-success" data-i18n="New users"><b><i class='fa fa-thumbs-o-up'></i> Open now</b></span></p>
                    <?php endif;?>
                     <?php if(isset($result['permanently_closed']) && $result['permanently_closed'] == "true"):?>
                    <p class="text-muted"><span class="text-danger" data-i18n="New users"><b><i class='fa fa-thumbs-o-down'></i> Permanently Closed</b></span></p>
                    <?php endif;?>
                    <i class="fa fa-road"></i>
                    <?php
                    $LocDistance = explode(',',$location);
                    echo round(distance($LocDistance[0],$LocDistance[1],$result['geometry']['location']['lat'],$result['geometry']['location']['lng'],"K"),2);?> KM
                    </div>
                    <?php if(isset($result['types']) && !empty($result['types'])):?>
                    <p class="text-muted"><span data-i18n="New users"><i class="fa fa-tags"></i>&nbsp;<?php echo ($result['types'][1] == "point_of_interest") ? $result['types'][0] : $result['types'][0].", ".$result['types'][1] ;?></span></p>
                    <?php endif;?>                    
                    <?php if(isset($result['vicinity']) && !empty($result['vicinity'])):?>
                    <p class="text-muted"><span data-i18n="New users"><i class="fa fa-map-marker"></i> &nbsp;&nbsp;<?php echo $result['vicinity'];?></span></p>
                    <?php endif;?>
                </div>
              </div>
            </a>             
             <!--//swatch-->
                <?php endforeach;?>                
                <?php if (isset($_REQUEST['lookupfor']) && !isset($_REQUEST['exploreMore']) || isset($_REQUEST['q'])  && !isset($_REQUEST['exploreMore'])){
                  ?>                  
                  <!-- explore More => LookupFor -->
                 <a onclick="exploreMore();" href="http://localhost/ihealth/finder.php?<?php echo (isset($_REQUEST['lookupfor'])) ? 'lookupfor' : 'q';?>=<?php echo (isset($_REQUEST['lookupfor'])) ? $_REQUEST['lookupfor'] : $_REQUEST['q'];?>&exploreMore=SearchResults" style="text-decoration: none;">
                     <center>
                        <div id="exploreMore" class="panel mini-box">                     
                         <img class="img-circle" src="assets/PNG/search.png" height="70" width="70" alt="">                    
                     <div class="box-info">
                         <p class="size-h2">Explore More</p>                         
                     </div>
                   </div>
                     </center>
                 </a>
                  <?php
                } ?>
                <!--//search results here-->
                <?php
            }
            }
            //place details
            elseif(isset($_REQUEST['place_details']) && !empty($_REQUEST['place_details'])){
               /*
                * check user Favorates Here ==>
                * By using User Id & Place ID
                */
                require 'app/ihealth.php';
                $ihealth = new ihealth;
                $checkFav = $ihealth->checkUserFav($_REQUEST['place_details'],$_SESSION['user_id']);                
                //<<== User Favorite check ends//
                $place_id = trim($_REQUEST['place_details']);
                $url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$place_id."&key=API_KEY";
                $getJsonResponse = file_get_contents($url);
                $detail = json_decode($getJsonResponse,TRUE);                
                if(!empty($detail['result'])){
                    ?>
                    <!--No detalis found-->
                    <div id="SearchResults"></div>
                    <div class="panel mini-box">
                     <div style="float: right;">
                        <!--add to fav-->
                        <?php if(isset($_REQUEST['searchType']) && !empty($_REQUEST['searchType']) && isset($_REQUEST['favo']) && !empty($_REQUEST['favo'])):
                        $resultNmae = urlencode($detail['result']['name']); 
                        ?>
                        <a id="anchoeAction" name="<?= ($checkFav == "added") ? 'del' : 'add' ;?>" href="javascript:void(0)" <?php if (isset($_SESSION['user_id'])):?>onclick='addtoFavorates("<?= $_SESSION['user_id']; ?>","<?php echo $resultNmae;?>","<?php echo urlencode($detail['result']['vicinity']);?>","<?php echo (isset($detail['result']['international_phone_number']) && !empty($detail['result']['international_phone_number'])) ? urlencode($detail['result']['formatted_phone_number']) : "";?>","<?= $_REQUEST['place_details']; ?>","<?= $_REQUEST['searchType']; ?>","<?= $_REQUEST['favo'];?>",this.name);'<?php endif;?> data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php if(!isset($_SESSION['user_id'])){ echo "Please Login First";} elseif ($checkFav == "added") {echo "Remove From Favorite";} else {echo "Add to favorite";}?>" style="text-decoration: none;">
                        <i id="addTofavId" class="text-danger fa <?= ($checkFav == "added") ? 'fa-heart fa-2x' : 'fa-heart-o fa-2x' ;?>"></i>
                        </a>
                        <?php endif;?>
                        <!--//add to fav-->
                     </div>
                        <span class="box-icon bg-info">
                            <span class="text-success"><i class="fa fa-info fa-lg"></i></span>
                        </span>
                        <div class="box-info">
                            <p class="size-h2"><?php echo $detail['result']['name'];?></p>
                            <p class="text-muted"><span data-i18n="New users"><?php echo $detail['result']['vicinity'];?></span></p>
                            <h4>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                       <?php if(!empty($detail['result']['formatted_phone_number']) || !empty($detail['result']['international_phone_number'])):?>
                                       <i class="fa fa-mobile"></i> |&nbsp;<?php echo $detail['result']['formatted_phone_number'].", ".$detail['result']['international_phone_number'];?><br><br>
                                       <?php endif;?>
                                       <?php if(isset($detail['result']['website']) && !empty($detail['result']['website'])):?>
                                        <i class="fa fa-globe"></i> |&nbsp;<a style="text-decoration: none;" href="<?php echo $detail['result']['website'];?>" target="_blank"><?php echo $detail['result']['website'];?></a><br><br>
                                       <?php endif;?>
                                      <i class="fa fa-map-marker"></i> |&nbsp;<?php echo $detail['result']['formatted_address'];?><br>
                                    </div>
                                  </div>                           
                            </h4>                            
                        </div>
                        <!--Photos-->
                    <div class="row">                        
                            <?php if(!empty($detail['result']['photos'])){
                                foreach($detail['result']['photos'] as $photo){
                                    $placePhotosUrl = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=".$photo['width']."&photoreference=".$photo['photo_reference']."&key=API_KEY";
                                    ?>                                    
                                        <div class="col-sm-5">
                                            <img class="img-responsive" src="<?php echo $placePhotosUrl;?>">
                                        </div>                                   
                                    <?php                                    
                                }
                            }                            
                            ?>                            
                             </div>                     
                            <!--//Photos-->          
                            <!--Reviews-->                            
                            <?php if(!empty($detail['result']['reviews'])):?>
                            <h3><span class="label label-primary"><i class="fa fa-bullhorn"></i> Reviews</span></h3>
                            <hr size="3"/>
                            <?php
                            foreach($detail['result']['reviews'] as $review) {
                            ?>                            
                            <blockquote>
                             <b><i class="fa fa-user"></i> <a style="text-decoration: none;" href="<?php echo $review['author_url'];?>"><?php echo $review['author_name'];?></a></b><br>
                             <?php
                             for($i=1;$i<=$review['rating'];$i++){
                              echo "<span class='text-warning'><i class='fa fa-star'></i></span>&nbsp;";
                             }
                             ?><br>
                             <?php if(!empty($review['text'])):?>
                             <p><i class="fa fa-quote-left fa-2x fa-pull-left fa-border"></i><?php echo $review['text'];?></p>
                             <?php endif;?>
                            </blockquote>
                            <hr size="3"/>
                            <?php } endif;?>
                            <!--//Reviews-->
                     </div>
                    
                    
                    <!--//No detalis found-->
                    <?php
                }
                else
                {
                   ?>
                   <!--Show details here-->
                   <div class="panel mini-box">
                        <span class="box-icon bg-info">
                            <span class="text-danger"><i class="fa fa-thumbs-o-down fa-2x"></i></span>
                        </span>
                        <div class="box-info">
                            <p class="size-h2">No Details Found</p>
                            <p class="text-muted"><span data-i18n="New users"></span></p>
                        </div>
                     </div>
                   <!--Show details here-->
                   <?php
                }
            }
            ?>
        </div>
        
        <div class="col-md-2">
            
        </div>
    </div>
    
    
    <!--//search results-->
</div>
<?php if (isset($_REQUEST['lookupfor']) || isset($_REQUEST['q']) || isset($_REQUEST['place_details'])):?>
<script>
   $("#SearchResults").ready(function(){
    $("html, body").delay(100).animate({
        scrollTop: $('#SearchResults').offset().top 
    }, 2000);
});
</script>
<script>
   function addtoFavorates(userToken,Pname,addr,phone,PLaceId,searchType,FavoWhat,action) {
      //alert("Userid= "+userToken+"Address = "+addr+"Phone ="+phone+", PlaceID= "+PLaceId+", SearchType= "+searchType+", WhatsFavo="+FavoWhat);
      $.ajax({
      url: 'ops.php',
      cache: false,
      type: 'post',
      data: {'uid': userToken, 'pname':Pname,'address': addr,'phone':phone, 'pid': PLaceId, 'stype':searchType, 'favo':FavoWhat, 'action':action},     
      success: function(data){        
       data = $.trim(data);       
       if (data == 'added') {       
       $("#addTofavId").prop('class', 'text-danger fa fa-heart fa-2x');
       $("#anchoeAction").prop('name', 'del');      
           $("#anchoeAction").tooltip('hide')          
          .tooltip('fixTitle')
          .tooltip('hide')
          .attr('data-original-title', 'Remove from favorite');
       }
       if (data == 'deleted') {       
       $("#addTofavId").prop('class', 'text-danger fa fa-heart-o fa-2x');
       $("#anchoeAction").prop('name', 'add');
       $("#anchoeAction").tooltip('hide')          
          .tooltip('fixTitle')
          .tooltip('hide')
          .attr('data-original-title', 'Add to favorite');       
       }
       if (data == 'error') {
         alert('error');
       }
      }
   });
   //return false;
   }
</script>
<?php endif;?>
<?php include 'app/footer.php';?>
