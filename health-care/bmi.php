<?php
include 'app/header.php';
require 'app/ihealth.php';
$ihealth = new ihealth;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['current_url'] = $current_link;
$firsttext="Your Body Mass Index (BMI) is <span class='label label-primary'> %%BMI%% </span>. This means your weight is within the %%BMIMSG%% range.";
$normaltext="You seem to keep your weight normal. Well done!";
$lowertext="Your current BMI is lower than the recommended range of <strong>18.5</strong> to <strong>24.9</strong>. <br>To be within the right range for your height, you should weigh between <strong>%%LOWERLIMIT%% lbs</strong> / <strong>%%LOWERLIMITKG%% kg</strong> and <strong>%%UPPERLIMIT%% lbs</strong> / <strong>%%UPPERLIMITKG%% kg</strong>";
$uppertext="Your current BMI is greater than the recommended range of <strong>18.5</strong> to <strong>24.9</strong>. <br>To be within the right range for your height, you should weigh between <strong>%%LOWERLIMIT%% lbs</strong> / <strong>%%LOWERLIMITKG%% kg</strong> and <strong>%%UPPERLIMIT%% lbs</strong> / <strong>%%UPPERLIMITKG%% kg</strong>";
if(isset($_POST['action_eng']) || isset($_POST['action_metric'])){
    if(isset($_POST['action_eng']))
    {
    //if eng
    $height=$_POST['hfeet']*12+$_POST['hinch'];
    $bmi=($_POST['weight']*703) / ($height*$height);
	echo "BMI =".$bmi; die;
    } elseif(isset($_POST['action_metric']))
    {        
     // if metric
     $height=$_POST['cm']/100;
     $bmi=$_POST['weight'] / round(($height*$height),2);      
    }    
    $bmi=number_format($bmi,1,".","");
	
	// prepare message for the user
	if($bmi<=18.5)
	{
		$bmimsg="Underweight";
	}
	
	if($bmi>18.5 and $bmi<=24.9)
	{
		$bmimsg="Normal";	
	}
	
	if($bmi>=25 and $bmi<=29.9)
	{
		$bmimsg="Overweight";			
	}
	
	if($bmi>=30)
	{
		$bmimsg="Obese";		
	}
	
	// what is the weight range?
	if($bmimsg!='Normal')
	{
        if(isset($_POST['action_eng']))
        {            
            $lowerlimit=number_format(( 18.5 * ($height*$height) ) / 703);
    		$lowerlimitkg=number_format($lowerlimit*0.453,1,".","");
    		
    		$upperlimit=number_format(( 24.9 * ($height*$height) ) / 703);
    		$upperlimitkg=number_format($upperlimit*0.453,1,".","");    
        }
		else
        {
            $lowerlimit=number_format( 18.5 * ($height*$height) * 2.204 );
    		$lowerlimitkg=number_format(18.5 * ($height*$height),1,".","");
    		
    		$upperlimit=number_format( 24.9 * ($height*$height) * 2.204 );
    		$upperlimitkg=number_format(24.9 * ($height*$height),1,".","");    
        }
	}
    //prepare texts
	$firsttext=str_replace("%%BMI%%",$bmi,$firsttext);
	$firsttext=str_replace("%%BMIMSG%%",$bmimsg,$firsttext);
	$lowertext=str_replace("%%LOWERLIMIT%%",$lowerlimit,$lowertext);
	$lowertext=str_replace("%%LOWERLIMITKG%%",$lowerlimitkg,$lowertext);
	$lowertext=str_replace("%%UPPERLIMIT%%",$upperlimit,$lowertext);
	$lowertext=str_replace("%%UPPERLIMITKG%%",$upperlimitkg,$lowertext);
	$uppertext=str_replace("%%LOWERLIMIT%%",$lowerlimit,$uppertext);
	$uppertext=str_replace("%%LOWERLIMITKG%%",$lowerlimitkg,$uppertext);
	$uppertext=str_replace("%%UPPERLIMIT%%",$upperlimit,$uppertext);
	$uppertext=str_replace("%%UPPERLIMITKG%%",$upperlimitkg,$uppertext);
}

elseif(isset($_POST['submit_bmi'])){	
	$bmi_val = $_POST['bmi_value'];
	$save = $ihealth->save_bmi($bmi_val);
	if($save == 'success'){ ?>
		         <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Your</strong>&nbsp;&nbsp;<b><u>BMI successfully saved</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>

<?php } elseif($save == 'already'){ ?>
	         <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-primary">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>You have</strong>&nbsp;&nbsp;<b><u>already saved your BMI for today</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>

 <?php } elseif($save == 'error'){
	?>
		         <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Erorr</strong>&nbsp;&nbsp;<b><u>occured while saving the BMI</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>

	<?php
 }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            
        </div>
        
        <div class="col-md-6">
            <div class="well">
 <ul class="nav nav-tabs">
  <li class=""><a href="#eng" data-toggle="tab" aria-expanded="false">English</a></li>
  <li class="active"><a href="#metric" data-toggle="tab" aria-expanded="true">Metric</a></li>  
</ul>
 <br><br>
<div id="myTabContent" class="tab-content">
  
  <!--English--><div class="tab-pane fade" id="eng">
<form class="form-horizontal" action="" method="post">
  <fieldset>        
    <div class="form-group">
      <label for="inputPassword" class="col-lg-2 control-label">Height</label>
      <div class="row">
      <div class="col-xs-4">
        <input type="number" name="hfeet" class="form-control" id="inputHeight" placeholder="Feet" max="6" min="4" required>
      </div>
      <div class="col-xs-4">
        <input type="number" name="hinch" class="form-control" id="inputHeight" max="11" min="0" placeholder="Inches">
      </div>
      </div>
    </div>
    <div class="form-group">
      <label for="select" class="col-lg-2 control-label">Weight</label>
      <div class="col-lg-10">
        <div class="row">
            <div class="col-md-10">
                <input type="number" name="weight" class="form-control" id="inputWeight" placeholder="Pounds" required>
            </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">        
        <button type="submit" class="btn btn-success" name="action_eng">Check</button>
      </div>
    </div>
  </fieldset>
</form>

  </div><!--English-->
  <!--metric--><div class="tab-pane fade active in" id="metric">
   <form class="form-horizontal" action="" method="post">
  <fieldset>        
    <div class="form-group">
      <label for="inputPassword" class="col-lg-2 control-label">Height</label>
      <div class="row">
      <div class="col-xs-4">
        <input type="number" name="cm" class="form-control" id="inputHeight" placeholder="Centimeter" required>
      </div>      
      </div>
    </div>
    <div class="form-group">
      <label for="select" class="col-lg-2 control-label">Weight</label>
      <div class="col-lg-10">
        <input type="number" name="weight" class="form-control" id="inputWeight" placeholder="Kg" required>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">        
        <button type="submit" class="btn btn-success" name="action_metric">Check</button>
      </div>
    </div>
  </fieldset>
</form>

  </div><!--metric-->
</div>
            </div>
        </div>
        
        <div class="col-md-3">
            <?php if(!empty($_POST)):?>
            <form action="" method="post">
            <input type="hidden" name="bmi_value" value="<?php echo $bmi;?>">
            <button type="submit" name="submit_bmi" class="btn btn-primary"><i class="fa fa-floppy-o"> Save </i></button>
            </form>
            <?php endif;?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            
        </div>
        
        <div class="col-md-8">
            <?php if(isset($_POST['action_eng']) || isset($_POST['action_metric'])):?>
<div class="well">
	<p><?=$firsttext?></p>
	<?php	
	switch($bmimsg)
    {	
       case 'Normal':       
        
     		
       break;
       
       case 'Underweight':
         ?>
         <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Lower Weight</h3>
  </div>
  <div class="panel-body">
    <?php echo $lowertext;?>
  </div>
</div>
         <?php
         
       		//echo $lowertext;
       break;
                                         	
       default:
        ?>
        <div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">Extremely Obese</h3>
  </div>
  <div class="panel-body">
    <?php echo $uppertext; ?>
  </div>
</div>
        <?php
       		//echo $uppertext;
        break;                              
      }
      ?>      
      
</div>
<?php endif;?>
        </div>
        
        <div class="col-md-2">
            
        </div>
    </div>
</div>

<?php include 'app/footer.php';?>
