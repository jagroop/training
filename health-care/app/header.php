<html>
    <head>
        <title>iCare</title>
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta charset="utf-8">
       <link rel="stylesheet" href="assets/css/bootstrap.css">
       <link rel="stylesheet" href="assets/css/c3.css">
       <link rel="stylesheet" href="assets/font_awesome/css/font-awesome.css">
       <link rel="stylesheet" href="assets/css/style.css">
       <script src="assets/js/googleJquery.js"></script>
    </head>
    <body>
        <?php session_start();?>
    <header>
        <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php"><i class="fa fa-heartbeat fa-lg"></i> <b>iCare</b></a>      
    </div>
      <div class="nav navbar-nav navbar-right">
       <?php if(empty($_SESSION['user_id'])):?>
       <h4><a href="login.php" class="btn btn-success btn-sm"><i class="fa fa-sign-in"></i> Login / Register</a></h4>
       <?php endif;?>
       
       <?php if(!empty($_SESSION['user_id'])):?>
      <h4><a href="dashboard.php?uid=<?php echo $_SESSION['user_id'];?>" style="text-decoration: none;"><span class="label label-default"><i class="fa fa-user"></i> <?php echo $_SESSION['user_name'];?></span></a>
       <a href="logout.php" class="btn btn-success btn-sm"><i class="fa fa-sign-out"></i> Logout</a></h4>
       <?php endif;?>
      </div>
  </div>
</nav>
    </header>