<footer>
    
</footer>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/validator.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
});
</script>
<script>
var x = document.getElementById("geolocation");
var icon = document.getElementById("searchIcon");
function getLocation() {
	icon.innerHTML = "<span class='text-success'><i class='fa fa-spinner fa-pulse fa-2x'></i></span>";
	x.innerHTML = "please wait...";
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition,showError);
    } else {
        x.innerHTML = "<b><span class='text-danger'><i class='fa fa-globe'></i> Geolocation is not supported by this browser.</span></b>";
    }
}
var refresh = document.getElementById('refreshLocation');
function refreshLocation() {	
	refresh.innerHTML = "<i class='text-primary fa fa-refresh fa-spin fa-2x'></i>";	
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition,showError);
    } else { 
        refresh.innerHTML = "<b><span class='text-danger'><i class='fa fa-globe'></i> Geolocation is not supported by this browser.</span></b>";
    }
}
function showPosition(position) {
	var lat = position.coords.latitude;
	var lon = position.coords.longitude;
	  $.ajax({
      url: 'longAndLat.php',      
      type: 'get',
      data: {'location': lat+","+lon},
      success: function(data){
		 if (data == "") {
			refresh.innerHTML = "<b><span class='text-danger'><i class='fa fa-map-marker'></i> Unable to Identify Your Location</span></b>";
			return;
		 }
        window.location = "<?php echo (!empty($_SESSION['ExploreEmptyRedirect'])) ? $_SESSION['ExploreEmptyRedirect'] : 'finder.php';?>";
      }	  
   });
    	
}
function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "<b><span class='text-danger'><i class='fa fa-ban'></i> You denied the request for Geolocation</span></b>"
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "<b><span class='text-danger'><i class='fa fa-globe'></i> No internet Connection</span></b>"
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}
</script>
<script>
	function ifood(){
		if($('#foodname').val() != ""){
			$('#ifood').prop('class','fa fa-spinner fa-pulse');
		}
	}
	function imed(){
		if($('#medname').val() != ""){
			$('#imed').prop('class','fa fa-spinner fa-pulse');
		}
	}
	function ifinder(){
		if($('#findwhat').val() != ""){
			$('#ifinder').prop('class','fa fa-spinner fa-pulse');
		}
	}
	function ifit(){		
			$('#stayfitMod').html("<span class='text-success'><i class='fa fa-spinner fa-pulse fa-2x'></i></span>");
			$('#stayfitModhead').html("Please Wait");
			$('#stayfitModpara').html("Fetching Data...");
	}
	function spin(str) {
		$('#'+str).html("<span class='text-success'><i class='fa fa-spinner fa-pulse fa-2x'></i></span>");
	}
	function exploreMore() {
		$('#exploreMore').html("<span class='text-success'><i class='fa fa-spinner fa-pulse fa-5x'></i></span>");
	}
</script>
</body>
</html>