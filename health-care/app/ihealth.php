<?php
require 'DB_Controller.php';
class ihealth extends DB_Controller {
    //time ago function
    public function time_ago($date)
		 
		 {
		 
		 if(empty($date)) {
		 
		 return "No date provided";
		 
		 }
		 
		 $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		 
		 $lengths = array("60","60","24","7","4.35","12","10");
		 
		 $now = time();
		 
		 $unix_date = strtotime($date);
		 
		 // check validity of date
		 
		 if(empty($unix_date)) {
		 
		 return "Invalid date";
		 
		 }
		 
		 // is it future date or past date
		 
		 if($now > $unix_date) {
		 
		 $difference = $now - $unix_date;
		 
		 $tense = "ago";
		 
		 } else {
		 
		 $difference = $unix_date - $now;
		 $tense = "from now";}
		 
		 for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
		 
		 $difference /= $lengths[$j];
		 
		 }
		 
		 $difference = round($difference);
		 
		 if($difference != 1) {
		 
		 $periods[$j].= "s";
		 
		 }
		 
		 return "$difference $periods[$j] {$tense}";
		 
		 }
    //user login function
    public function login($e,$p){
		$p = sha1($p);
        $q = "SELECT id,name FROM user WHERE email='$e' AND password='$p'";
        $query = mysql_query($q);
        $row = mysql_num_rows($query);
        if($row > 0){
            $result = mysql_fetch_assoc($query);
            $_SESSION['user_id'] = $result['id'];
            $_SESSION['user_name'] = $result['name'];
			if(isset($_SESSION['current_url'])){
            header("Location:".$_SESSION['current_url']);
			}
			else {
			header("Location:index.php");
			}
        }
        else{
            return 'wrong';
        }
    }
	// register user function
	public function register_user($user_data){
		extract($user_data);
		$a = "SELECT email FROM user WHERE email = '$email'";
        $b = mysql_query($a);
        $rows = mysql_num_rows($b);
        if($rows > 0){
            return 'already';        
        }
		$password = sha1($password);
        $q = "INSERT INTO user VALUES('','$name','$email','$password')";
        $sql = mysql_query($q);
        if($sql) {
            return 'success';
        }
        else {
            return 'failure';
        }
	}
    //save your calculated bmi
    public function save_bmi($bmi_val){
    $uid = $_SESSION['user_id'];
    $date = date('Y-m-d');
    if(empty($uid)) {
        header("Location:login.php?bummer");
    }    
    else{    
    $q = "INSERT INTO bmi (user_id,bmi,date) VALUES('$uid','$bmi_val',NOW())";
    $query = mysql_query($q);
    if($query){
        return 'success';
    } else{
        return 'error';
    }
    }
    }
    
    //save your calculated bp
    public function save_bp($data){        
    extract($data);
    //echo $map." ".$state; die;
    $uid = $_SESSION['user_id'];
    $date = date('Y-m-d H:i:s');
    if(empty($uid)) {        
        header("Location:login.php?bummer");
    }
    if(empty($map) && empty($state)){
        return 'error';
    }
    else{    
    $q = "INSERT INTO bp (user_id,map,state,date) VALUES('$uid','$map','$state',NOW())";
    $query = mysql_query($q);
    if($query){
        return 'success';
    } else{
        return 'error';
    }
    }
    }
    
    //get bmi's of a user
    public function bmis($uid){
        $q = "SELECT * FROM bmi WHERE user_id='$uid'";
        $sql = mysql_query($q);
        while($r = mysql_fetch_assoc($sql)){
            $bmis[]=$r;
        }
        return $bmis;
    }
    
    //get bp reports of a user
    public function bps($uid){
        $q = "SELECT * FROM bp WHERE user_id='$uid'";
        $sql = mysql_query($q);
        while($r = mysql_fetch_assoc($sql)){
            $bps[]=$r;
        }
        return $bps;
    }
	//check user Fav
	
	public function checkUserFav($pid,$uid){
		$q = "SELECT id FROM favo WHERE place_id = '$pid' AND uid = '$uid'";
		$sql = mysql_query($q);
		$row = mysql_num_rows($sql);
		if($row > 0){
			return "added";
		}		
	}
	//add to favorites
	public function addToFav($uid,$pname,$address,$phn,$pid,$searchType,$favo,$action){		
		if($action == "add"){			
		$q = "INSERT INTO favo (uid,place_name,address,phone,place_id,search_type,favo,date_added)
		      VALUES('$uid','$pname','$address','$phn','$pid','$searchType','$favo',NOW())";		
		$sql = mysql_query($q);
		if($sql){
			return "added";
		}
		else{
			return "error";
		}
		}
		elseif($action == "del"){			
		$q = "DELETE FROM favo
		      WHERE uid = '$uid' AND place_id = '$pid'";
		$sql = mysql_query($q);
		if($sql){
			return "deleted";
		}
		else{
			return "error";
		}
		}
	}
	
	//get My favorites
	public function getMyFav($uid){
		$q = "SELECT * FROM favo WHERE uid = '$uid' ORDER BY date_added DESC";
		$sql = mysql_query($q);
		while ($rec = mysql_fetch_assoc($sql)){
		if (!isset($res[ $rec["search_type"] ])) {
		   $res[ $rec["search_type"] ] = array();       
		  }
		$res[ $rec["search_type"] ][] = array_slice($rec, 1);
        }
		return $res;
	}
	
	//del Myfav
	public function delFav($placeId){
		$q = "DELETE FROM favo WHERE place_id = '$placeId'";
		$sql = mysql_query($q);
		if($sql){
			return "deleted";
		}
		else{
			return "error";
		}
	}
	//add fav med or del
	public function addFavMed($id,$uid,$name,$manuf,$cat,$price,$action){
		if($action == "add"){
			$q = "INSERT INTO favo_med (med_id,user_id,med_name,manuf,price,type,date_added) VALUES ('$id','$uid','$name','$manuf','$price','$cat',NOW())";
		if(mysql_query($q)){
			return "added";
		}
		else {
			return "error";
		}
		} elseif($action == "del"){
		$q = "DELETE FROM favo
		WHERE uid = '$uid' AND med_id = '$id'";
		$sql = mysql_query($q);
		if($sql){
			return "deleted";
		}
		else{
			return "error";
		}
		}
		
	}
	//check user fav Med
	public function userFavMed($medId,$uid){
		$q = "SELECT id FROM favo_med WHERE med_id = '$medId' AND user_id = '$uid'";
		$sql = mysql_query($q);
		$row = mysql_num_rows($sql);
		if($row > 0){
			return "added";
		}
	}
	//get User Fav Meds
	public function getUserMeds($uid){
		$q = "SELECT * FROM favo_med WHERE user_id = '$uid' ORDER BY date_added DESC";
		$sql = mysql_query($q);
		while ($rec = mysql_fetch_assoc($sql)){
		if (!isset($res[ $rec["type"] ])) {
		   $res[ $rec["type"] ] = array();       
		  }
		$res[ $rec["type"] ][] = array_slice($rec, 1);
        }
		return $res;
	}
}