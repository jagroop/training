<?php
$uid = (int)$_GET['uid'];
include 'app/header.php';
require 'app/ihealth.php';
$ihealth = new ihealth;
if(empty($_SESSION['user_id']) || empty($uid)){
    header("Location:index.php");
}
$bmis = $ihealth->bmis($uid);
$bps = $ihealth->bps($uid);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
  <li class="active"><a href="#bmi" data-toggle="tab" aria-expanded="true"><i class="fa fa-book"></i> BMI Record</a></li>
  
  <li class=""><a href="#bp" data-toggle="tab" aria-expanded="true"><i class="fa fa-heartbeat"></i> BP</a></li>
</ul>
<div id="myTabContent" class="tab-content">
  <div class="tab-pane fade active in" id="bmi">
    <div class="row">
        <div class="col-md-6">
            <link rel="stylesheet" href="assets/css/datatable.css">
            <link rel="stylesheet" href="assets/css/datatable.responsive.css">
                    <table class="table table-striped table-hover display" id="printBmi">
        <thead>
            <tr>
                <th>#</th>
                <th><i class="fa fa-male"></i> BMI</th>
                <th><i class="fa fa-calendar"></i> Date</th>                
            </tr>
        </thead>
        
        <tbody>
            
            <?php            
            foreach($bmis as $bmi):
            $bmiArray[] = $bmi['bmi'];
            $bmidateArray[] = $ihealth->time_ago($bmi['date']);
            ?>
            
           <tr>
                <td><?php echo ++$counter;?></td>
                <td>
                    <?php
                    $bmi_reading = $bmi['bmi'];               
                    ?>
                    <h5><span class="label label-<?php if($bmi_reading<=18.5) {echo "primary";} elseif($bmi_reading>18.5 and $bmi_reading<=24.9){echo "success";} elseif($bmi_reading>=25 and $bmi_reading<=29.9){echo "default";}elseif($bmi_reading>=30) {echo "danger";}else {echo "default";}?>"><?php echo $bmi_reading;?></span></h5>                
                </td>
                <td><?php echo date("jS F Y",strtotime($bmi['date']));?></td>                
            </tr>

            <?php endforeach;            
            ?>
        </tbody>
    </table>

        </div>
        
        <div class="col-md-6">
           <br>
            <div class="row">
                <div class="col-sm-2">
                    
                </div>
                
                <div class="col-sm-8">
                    <ul class="list-group">
                        <b>
                    <li class="list-group-item">
                         <h5>Under Weight
                     <span style="float: right;" class="label label-primary"><=18.5</span></h5>                      
                    </li>
                    
                    <li class="list-group-item">
                         <h5>Normal(Healthy)
                     <span style="float: right;" class="label label-success">>18.5 and <=24.9</span></h5>                      
                    </li>
                    
                    <li class="list-group-item">
                        <h5>Overweight
                      <span style="float: right;" class="label label-default">>=25 and <=29.9</span></h5>                      
                    </li>
                    
                    <li class="list-group-item">
                       <h5> Extremely Obese
                      <span style="float: right;" class="label label-danger">>=30</span></h5>                      
                    </li>
                    </b>
                  </ul>
                    
                </div>
                
                <div class="col-sm-2">
                    <img id="BMIPrintIcon" class="img-circle" src="assets/PNG/printer.png" height="40" width="40" alt="">
                </div>
            </div>
          
        </div>
    </div>
    <hr size="5"/>
    <!--graphical representation of bmi-->
    <h4><span class="label label-primary"><i class="fa fa-line-chart"></i> Graphical Representation</span></h4>
    <div id="bmigraph"></div>
    
        <!--graphical representation of bmi-->
  </div>
  <div class="tab-pane fade" id="bp">
    <div class="row">
        <div class="col-md-6">
                <table class="table table-striped table-hover display" id="printBP">
        <thead>
            <tr>
                <th>#</th>
                <th>M.A.P(mmHg)</th>
                <th>State</th>
                <th>Date</th>
            </tr>
        </thead>
        
        <tbody>
            <?php foreach($bps as $bp):
            $bpArray[] = $bp['map'];
            $bpdateArray[] = $ihealth->time_ago($bp['date']);
            ?>
             <tr class="<?php if($bp['state'] == "High") echo "danger"; elseif($bp['state'] == "Normal") echo "success"; elseif($bp['state'] == "Low") echo "info";?>">
                <td><span class="text-primary"><?php echo ++$foo;?></span></td>
                <td><?php echo $bp['map'];?></td>
                <td><?php echo $bp['state'];?></td>
                <td><?php echo date("jS F Y",strtotime($bp['date']));?><?php //echo $ihealth->time_ago($bp['date']);?></td>
            </tr>
             <?php endforeach;?>
        </tbody>
    </table>
        </div>
        
        <div class="col-md-6">
            
                <div class="row">
                    <div class="col-md-2">
                        
                    </div>
                    <div class="col-md-8">                        
                        <h3 class="text-danger"><i class="fa fa-square"></i> High</h3>
                        <h3 class="text-success"><i class="fa fa-square"></i> Normal</h3>
                        <h3 class="text-info"><i class="fa fa-square"></i> Low</h3>
                        
                    </div>
                    
                    <div class="col-md-2">
                        <img id="BPPrintIcon" class="img-circle" src="assets/PNG/printer.png" height="40" width="40" alt="">
                    </div>
                </div>               
            
        </div>       
    </div>
    <hr size="3"/>
    <!--graphical representation of bp-->
    <h4><span class="label label-primary"><i class="fa fa-line-chart"></i> Graphical Representation</span></h4>
                <div id="bpgraph"></div>
                
                <!--graphical representation of bp ends here-->
  </div>
</div>
        </div>
    </div>
</div>
<script src="assets/js/c3.js"></script>
<script src="assets/js/d3.min.js" charset="utf-8"></script>
<script src="assets/js/datatable.min.js"></script>
<script src="assets/js/dataTable.responsive.js"></script>
<script>           
    $(document).ready(function() {
    $('table.display').dataTable({
         responsive: true
        });
    });
</script>
<script>     

      var bmi = c3.generate({
        bindto: '#bmigraph',
        data: {
          columns: [
			['bmi', <?php echo "'" . implode("','", $bmiArray) . "'";?>]            
          ]
        },
    //    grid: {
    //    x: {
    //        show: true
    //    },
    //    y: {
    //        show: true
    //    }
    //},
    zoom: {
        enabled: true
    },
        axis: {
          x: {
            categories: [<?php echo "'" . implode("','", $bmidateArray) . "'";?>],
            type: 'categorized'
          }
        }
      });
      
      
      var bp = c3.generate({
        bindto: '#bpgraph',
        data: {
          columns: [
			['bp', <?php echo "'" . implode("','", $bpArray) . "'";?>]            
          ]
        },
        zoom: {
        enabled: true
        },
        axis: {
          x: {
            categories: [<?php echo "'" . implode("','", $bpdateArray) . "'";?>],
            type: 'categorized'
          }
        }
      });

      

    </script>
<script>
    function printData(str)
{
   var divToPrint=document.getElementById(str);
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}

$('#BMIPrintIcon').on('click',function(){
    //document.getElementById('printBmi').style.border = "1px solid #000";
printData('printBmi');
})

$('#BPPrintIcon').on('click',function(){
printData('printBP');
})
</script>
<?php include 'app/footer.php';?>