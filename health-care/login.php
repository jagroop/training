<?php
include 'app/header.php';
require 'app/ihealth.php';
$ihealth = new ihealth;
if(isset($_SESSION['user_id'])){
    header("Location:".$_SESSION['current_url']);
    return;
}
if(isset($_POST['login'])){
    $login = $ihealth->login($_POST['email'],$_POST['password'],$_SESSION['data']);
    if($login == 'wrong'){ ?>
    
         <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>ERROR</strong>&nbsp;&nbsp;<b><u>Wrong email or password</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>
    <?php }
    elseif($login == 'blocked'){ ?>
      <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Oops!</strong>&nbsp;&nbsp;<b><u>You are blocked by Admin</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>
   <?php }  
    
}
if(isset($_POST['register'])){
    $user_data = array(
                       'name' => $_POST['name'],
                       'email' => $_POST['email'],
                       'password' => $_POST['password']
                       );
    $register = $ihealth->register_user($user_data);
    if($register == 'success'){ ?>
        <div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Success</strong>&nbsp;&nbsp;<b><u>You are successfully Registered !!</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
      <?php }
       elseif($register == 'failure'){ ?>
        <div class="alert alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Oh snap!</strong> <a href="#" class="alert-link">something went wrong</a> and try submitting again.
      </div>
       <?php }
       elseif($register == 'already'){ ?>
        <div class="alert alert-dismissible alert-info">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Email alredy registerd</strong> Try <a href="#" class="alert-link">another one</a>
</div>
       <?php }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            
        </div>
        
        <div class="col-md-8">
        <?php if(isset($_GET['bummer'])):?>
       <h4 class=text-danger><b>You have to login first in order to do that !</b></h4>       
       <?php endif;?>
<ul class="nav nav-tabs">
  <li class="active"><a href="#login" data-toggle="tab"><i class="fa fa-sign-in"></i> Login</a></li>
  <li><a href="#register" data-toggle="tab"><i class="fa fa-pencil-square-o"></i> Register</a></li>    
</ul>
<div id="myTabContent" class="tab-content">
  <div class="tab-pane fade active in" id="login">
    <br><br>
    <div class="row">
    <div class="col-md-5">
    <form action="" method="post">
        <input type="email" name="email" class="form-control" placeholder="email@example.com" required><br><br>
        <input type="password" name="password" class="form-control" placeholder="password" required><br><br>
        <input type="submit" name="login" class="btn btn-success" value="Login">
    </form>
    </div>    
    <div class="col-md-7">
    </div>
    </div>
  </div>
  <div class="tab-pane fade" id="register">
    <br><br>
    <div class="row">
    <div class="col-md-5">
    <form data-toggle="validator" role="form" action="" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="inputName" class="control-label">Name</label>
    <input type="text" name="name" class="form-control" id="inputName" placeholder="name" data-error="Please enter your name" required>
    <div class="help-block with-errors"></div>
  </div>
    <div class="form-group">
    <label for="inputEmail" class="control-label">Email</label>
    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" data-error="oops, that email address is invalid" required>
    <div class="help-block with-errors"></div>
  </div>    
    <div class="form-group">
        <label for="inputPassword" class="control-label">Password</label>
        <div class="form-group">
            <input type="password" name="password" data-minlength="6" class="form-control" id="inputPassword" placeholder="Password" required>
      <span class="help-block">Minimum of 6 characters</span>
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, password does not match" placeholder="Confirm Password" required>
      <div class="help-block with-errors"></div>
        </div>
    </div>              
    <div class="form-group">
    <input type="submit" name="register" class="btn btn-success" value="Register">
    </div>
    </form>   
    </div>    
    <div class="col-md-7">
        
    </div>
    </div>
  </div>  
</div> 
    </div>        </div>
        
        <div class="col-md-2">
            
        </div>
    </div>
</div>
<?php include 'app/footer.php';?>