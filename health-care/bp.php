<?php
include 'app/header.php';
require 'app/ihealth.php';
$ihealth = new ihealth;
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['current_url'] = $current_link;
if(isset($_POST['save_bp'])){    
    $bpdata = array(
                    'map' => $_POST['map'],
                    'state' => $_POST['state']
                    );
   $save = $ihealth->save_bp($bpdata);
   	if($save == 'success'){ ?>
		         <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Your</strong>&nbsp;&nbsp;<b><u>BP Report successfully saved</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>

<?php } elseif($save == 'already'){ ?>
	         <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-primary">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>You have</strong>&nbsp;&nbsp;<b><u>already saved your BP report for today</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>

 <?php } elseif($save == 'error'){
	?>
		         <div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Erorr</strong>&nbsp;&nbsp;<b><u>occured while saving the BP Report</u></b>
        </div>
  </div>
  <div class="col-md-8">
    
  </div>
</div>
  
</div>

	<?php
 }

}

if(isset($_POST['check_bp'])){
$age = (int)$_POST['age'];
$sbp = (int)$_POST['sbp'];
$dbp = (int)$_POST['dbp'];
$_SESSION['age'] = $age;
$_SESSION['sbp'] = $sbp;
$_SESSION['dbp'] = $dbp;
$map = round(($sbp+2*$dbp)/3); // mean artierical pressure mmHg
$status = "1";
if($age>=1 and $age<=5){
    if($map>=63 and $map<75){
       $bp="Low";
    }
    if($map>=75 and $map<89){
        $bp="Normal";
    }
    if($map>=89){
        $bp="High";
    }
}
if($age>5 and $age<=13){
    if($map>=70 and $map<82){
        $bp="Low";
    }
    if($map>82 and $map<92){
        $bp="Normal";
    }
    if($map>=92){
        $bp="High";
    }
}
if($age>13 and $age<=19){
    if($map>=84 and $map<90){
        $bp="Low";
    }
    if($map>90 and $map<94){
        $bp="Normal";
    }
    if($map>=94){
        $bp="High";
    }
}
if($age>19 and $age<=24){
    if($map>=86 and $map<93){
        $bp="Low";
    }
    if($map>93 and $map<99){
        $bp="Normal";
    }
    if($map>=99){
        $bp="High";
    }
}
if($age>24 and $age<=29){
    if($map>=87 and $map<94){
        $bp="Low";
    }
    if($map>94 and $map<100){
        $bp="Normal";
    }
    if($map>=100){
        $bp="High";
    }
}
if($age>29 and $age<=34){
    if($map>=88 and $map<95){
        $bp="Low";
    }
    if($map>95 and $map<101){
        $bp="Normal";
    }
    if($map>=101){
        $bp="High";
    }
}
if($age>34 and $age<=39){
    if($map>=89 and $map<96){
        $bp="Low";
    }
    if($map>96 and $map<102){
        $bp="Normal";
    }
    if($map>=102){
        $bp="High";
    }
}
if($age>39 and $age<=44){
    if($map>=90 and $map<97){
        $bp="Low";
    }
    if($map>97 and $map<104){
        $bp="Normal";
    }
    if($map>=104){
        $bp="High";
    }
}
if($age>44 and $age<=49){
    if($map>=92 and $map<98){
        $bp="Low";
    }
    if($map>98 and $map<105){
        $bp="Normal";
    }
    if($map>=105){
        $bp="High";
    }
}
if($age>49 and $age<=54){
    if($map>=93 and $map<99){
        $bp="Low";
    }
    if($map>99 and $map<107){
        $bp="Normal";
    }
    if($map>=107){
        $bp="High";
    }
}
if($age>54 and $age<=59){
    if($map>=94 and $map<101){
        $bp="Low";
    }
    if($map>101 and $map<108){
        $bp="Normal";
    }
    if($map>=108){
        $bp="High";
    }
}
if($age>59 and $age<=64){
    if($map>=96 and $map<103){
        $bp="Low";
    }
    if($map>103 and $map<110){
        $bp="Normal";
    }
    if($map>=110){
        $bp="High";
    }
}
if($bp != "Low" && $bp != "Normal" && $bp != "High" && $bp == ""){
	$bp = "Error";
	//echo "please provide acc"; die;
}
}
?>
<div class="container-fluid">
<div class="row">
  <div class="col-md-3">
  
  </div>
  
  <div class="col-md-6">
<form class="form-horizontal" action="" method="post">
  <fieldset>
    <div class="form-group">
      <label for="select" class="col-lg-2 control-label">Age</label>
      <div class="col-sm-8">
        <input type="number" class="form-control" name="age" value="<?php if(isset($_SESSION['age'])) echo $_SESSION['age']; ?>" placeholder="1-80 years" min="1" max="80" required>         
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword" class="col-lg-2 control-label">BP Reading</label>
      <div class="row">
      <div class="col-xs-4">
        <input type="number" name="sbp" class="form-control" id="inputHeight" value="<?php if(isset($_SESSION['sbp'])) echo $_SESSION['sbp']; ?>" placeholder="Systolic pressure (Top number)" max="150" min="75" required>
      </div>
      <div class="col-xs-4">
        <input type="number" name="dbp" class="form-control" id="inputHeight" value="<?php if(isset($_SESSION['dbp'])) echo $_SESSION['dbp']; ?>" placeholder="Diastolic pressure (Bottom number)" max="100" min="50" required>
      </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">        
        <button type="submit" class="btn btn-success btn-sm" name="check_bp"><i class="fa fa-heartbeat"></i> Check</button>
      </div>
    </div>
  </fieldset>
</form>  </div>
  
  <div class="col-md-3">
  <?php if(isset($_POST['check_bp']) && $bp != "Error") :?>  
  <form action="" method="post">
    <input type="hidden" name="map" value="<?php echo $map;?>">
    <input type="hidden" name="state" value="<?php echo $bp;?>">
    <button type="submit" class="btn btn-primary" name="save_bp"><i class="fa fa-floppy-o"></i> Save</button>
  </form>
  <?php endif;?>
  </div>
</div>

<!--result-->
<div class="row">
    <div class="col-md-3">
        
    </div>
    
    <div class="col-md-6">
        <?php
        if(isset($_POST['check_bp']) && !empty($bp)){			
         switch($bp){
            case 'Low':
                ?>
                <!--Incase if bp is LOW-->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                      <h3 class="panel-title"><i class="fa fa-meh-o"></i> Low Blood Pressure</h3>
                    </div>
                    <div class="panel-body">
                      <h4 class="text-primary">Your Blood pressure is Below Normal Range</h4>
                      Mean Arterial Pressure(MAP): <span class="label label-primary"><?php echo $map;?></span> mmHg
                    </div>
                </div>
                
                <!--//Incase if bp is LOW-->
                <?php
                break;
            case 'Normal':
                ?>
                <!--Incase if bp is Normal-->
                <div class="panel panel-success">
                    <div class="panel-heading">
                      <h3 class="panel-title"><i class="fa fa-smile-o"></i> Normal Blood Pressure</h3>
                    </div>
                    <div class="panel-body">
                      <h4 class="text-success">Nothing to Worry Your Blood pressure is Under Normal Range</h4>
                      Mean Arterial Pressure(MAP): <span class="label label-success"><?php echo $map;?></span> mmHg
                    </div>
                </div>
                
                <!--//Incase if bp is Normal-->
                <?php
                break;
            case 'High':
                ?>
                <!--Incase if bp is High-->
                <div class="panel panel-danger">
                    <div class="panel-heading">
                      <h3 class="panel-title"><i class="fa fa-frown-o"></i> High Blood Pressure</h3>
                    </div>
                    <div class="panel-body">
                      <h4 class="text-danger">Your Blood pressure is Above Normal Range</h4>
                      Mean Arterial Pressure(MAP): <span class="label label-danger"><?php echo $map;?></span> mmHg
                    </div>
                </div>
                
                <!--//Incase if bp is High-->
                <?php
                break;
            case 'Error':
                ?>
				
				<!--Incase of error-->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                      <h3 class="panel-title"><i class="fa fa-ban"></i> Please Provide Accurate Data</h3>
                    </div>
                    <div class="panel-body">
                      <h4 class="text-danger">SBP and DBP should be Accurate According to your Age Range</h4>                      
                    </div>
                </div>
                
                <!--//Incase error-->
				
				<?php
                break;
			
         }
        }
        ?>
    </div>
    
    <div class="col-md-3">
        
    </div>
    
</div>
<!-- result end  -->
</div>
<?php include 'app/footer.php';?>