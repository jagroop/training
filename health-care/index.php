<?php include 'app/header.php';
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['current_url'] = $current_link;
?>
<style>
    body{        
        background-color: #f6f6f6;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            
        </div>

        <div class="col-md-8">
           <div class="row">
            <div class="col-xs-3 col-sm-4">
                <!--panel-->
            <a href="bmi.php" style="text-decoration: none;">
                <div class="panel bmi mini-box">
                <span class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/weight.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Height & Weight</p>
                    <p class="text-muted"><span data-i18n="New users">Calculate Your BMI</span></p>
                </div>
            </div>
            </a>
            <!--panel ends-->
            </div>
            <div class="col-xs-3 col-sm-4">
                <!--panel-->
            <a href="bp.php" style="text-decoration: none;">
                <div class="panel mini-box">
                <span class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/heart.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Blood Pressure</p>
                    <p class="text-muted"><span data-i18n="New users">Calculate Your BP</span></p>
                </div>
            </div>
            </a>
            <!--panel ends-->
            </div>
            <div class="col-xs-3 col-sm-4">
                <!--panel-->
            <a href="food.php" style="text-decoration: none;">
                <div class="panel mini-box">
                <span class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/food.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Food & Nutrition</p>
                    <p class="text-muted"><span data-i18n="New users">Know Your Food</span></p>
                </div>
            </div>
            </a>
            <!--panel ends-->
            </div>
            <div class="col-xs-3 col-sm-4">
               <!--panel-->
            <a href="stayfit.php" onclick="ifit()" style="text-decoration: none;">
                <div class="panel mini-box">
                <span class="box-icon bg-info">
                    <div id="stayfitMod">
                    <img class="img-circle" src="assets/PNG/muscle.png" height="70" width="70" alt="">
                    </div>
                </span>
                <div class="box-info">
                    <p id="stayfitModhead" class="size-h2">Stay Fit</p>
                    <p class="text-muted"><span id="stayfitModpara" data-i18n="New users">Feeds to Keep you Fit</span></p>
                </div>
            </div>
            </a>
            <!--panel ends-->
           </div>
            <div class="col-xs-3 col-sm-4">
                 <!--panel-->
            <a href="meds.php" style="text-decoration: none;">
                <div class="panel mini-box">
                <span class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/meds.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <p class="size-h2">Medicines</p>
                    <p class="text-muted"><span data-i18n="New users">Dig Info about Meds</span></p>
                </div>
            </div>
            </a>
            <!--panel ends-->
            </div>
           <div class="col-xs-3 col-sm-4">
                <!--panel-->
            <a <?php if(empty($_SESSION['location'])) echo "onclick='getLocation()' href='javascript:void(0)'"; else echo "href='finder.php'";?> data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Allow your browser to get your location otherwise Search Feature will not work properly !!" style="text-decoration: none;">
                <div class="panel mini-box">
                <span class="box-icon bg-info">
                    <div id="searchIcon">
                    <img class="img-circle" src="assets/PNG/search.png" height="70" width="70" alt="">
                    </div>
                </span>
                <div class="box-info">
                    <p class="size-h2">Search</p>
                    <p id="geolocation" class="text-muted"><span data-i18n="New users">iCare Search tool</span></p>
                </div>
            </div>
            </a>
            <!--panel ends-->
           </div>
           </div>
        </div>
        
        <div class="col-md-2">
            
        </div>
    </div>
</div>
<?php include 'app/footer.php';?>