<?php include 'app/header.php';
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['current_url'] = $current_link;
if(isset($_REQUEST['clearAllHistory'])){
    unset($_SESSION['recent_med_brand']);
}
?>
<style>
    body{
        /*background-image: url("assets/images/bg.jpg");*/
        background-color: #f6f6f6;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
        
        </div>
        
        <div class="col-md-6">
        
            <!--meds form--><form action="" method="get">
            <div class="row">
                 <h4 class="text-primary"><b>&nbsp;&nbsp;<img class="img-circle" src="assets/PNG/meds.png" height="40" width="40" alt=""> Enter Medicine Name</b></h4>
                <div class="col-sm-10">                   
                    <input list="meds" id="medname" name="q" value="<?php if(isset($_GET['q'])){echo $_GET['q'];}?>" autofocus="on" autocomplete="off" class="form-control" onkeyup="suggestions(this.value);" placeholder="For e.g. Disprin" required>
                    <datalist id="meds">
                       <option value="ben">
                        <option value="asd">
                            <option value="quen">
                    </datalist>
                </div>
                
                <div class="col-sm-2">
                    <button type="submit" value="Medicine" class="btn btn-success btn-sm" name="search" onclick="imed()"><b><i class="fa fa-search" id="imed"></i> Search</b></button>
                </div>
            </div>
        </form><!--meds form-->
            
        
        <div class="col-md-3">
        
        </div>
        <?php if(!isset($_GET['search']) && isset($_SESSION['recent_med_brand'])):?>
<!--Recent Meds Search--><div class="panel mini-box">
                <span class="box-icon bg-info">
                    <img class="img-circle" src="assets/PNG/meds.png" height="70" width="70" alt="">
                </span>
                <div class="box-info">
                    <div style="float: right;">
                        <a href="?clearAllHistory=RecentlysearchedMeds">clear</a>
                    </div>
                    <p class="size-h2">Recently Searched Medicines</p>
                    <hr>
                    <?php foreach(array_unique($_SESSION['recent_med_brand']) as $recentMed): ?><br>
                    <a href="http://localhost/ihealth/meds.php?q=<?php echo urlencode($recentMed);?>&search=Medicine"><?= $recentMed;?></a>
                    <?php endforeach;?>
                </div>
</div><!--Recent Meds Search-->
<?php endif;?>
    </div>
    
</div>
    
    <?php
    if(isset($_GET['search']) && !empty($_GET['q'])){
      $medGet = urlencode(trim($_GET['q']));      
      $url = "http://www.truemd.in/api/medicine_details/?id={$medGet}&key=API_KEY";
      $raw = file_get_contents($url);
      $med = json_decode($raw,TRUE);
      echo "<pre>";
      print_r($med); die;
      if(empty($med['response']['medicine'])){
        $url = "http://www.truemd.in/api/medicine_suggestions/?id={$medGet}&key=API_KEY";
        $raw = file_get_contents($url);
        $med = json_decode($raw,TRUE);
        if(empty($med['response']['suggestions'])){
           ?>
           <!--NO meds were found-->
           <div class="row">
                <div class="col-md-2">
                
                </div>
                
                <div class="col-md-8">
                <h3 class="text-danger"><b><i class="fa fa-meh-o"></i> No Medicines found</b></h3>
                </div>
                
                <div class="col-md-2">
                
                </div>
            </div>
           <!--NO meds were found-->
           <?php
        } else {
        ?>
        
        <div class="row">
            <div class="col-md-2">
            
            </div>
            
            <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-medkit"></i> <?php echo count($med['response']['suggestions']);?> Suggestions found</div>
                <div class="panel-body">
                  <?php foreach($med['response']['suggestions'] as $suggestion):?>
                  <h4 class="text-primary"><a href="http://localhost/ihealth/meds.php?q=<?php echo urlencode($suggestion['suggestion']);?>&search=Medicine"><?php echo $suggestion['suggestion'];?></a></h4>
                  <?php endforeach;?>
                </div>
            </div>
            </div>
            
            <div class="col-md-2">
            
            </div>
        </div>
        <!--NO meds were found-->
   <?php }  } else{
    
       $_SESSION['recent_med_brand'][] = $med['response']['medicine']['brand'];
         ?>
         
         <!--Getting Alternatives-->
         <?php
         $alternativesUrl = "http://www.truemd.in/api/medicine_alternatives/?id=".urlencode(trim($med['response']['medicine']['brand']))."&key=API_KEY";
         $alternativesRaw = file_get_contents($alternativesUrl);
         $alternatives = json_decode($alternativesRaw,TRUE);
         //echo "<pre>";
         //print_r($alternatives['response']['medicine_alternatives']);
         ?>
         <!--//Getting Alternatives-->
         <div class="row">
            <div class="col-md-2">
            
            </div>
            
            <div class="col-md-8">
            <!--Medicines Details-->
            <div class="panel panel-default">
                <div class="panel-heading"><h4><span class="label label-info"><i class="fa fa-medkit"></i> <?php echo $med['response']['medicine']['brand'];?></span></h4></div>
                <div class="panel-body">
            <table style="font-size: inherit;" class="table table-striped table-hover ">
                <tr>
                    <td class="text-primary">Manufacturer:</td>
                    <td><?php echo $med['response']['medicine']['manufacturer'];?></td>        
                </tr>
                <tr>
                    <td class="text-primary">Package Price:</td>
                    <td><?php echo $med['response']['medicine']['package_price'];?>&nbsp;<i class="fa fa-inr"></i></td>        
                </tr>
                <tr>
                    <td class="text-primary">Package Qty:</td>
                    <td><?php echo $med['response']['medicine']['package_qty'];?>&nbsp;<?php echo $med['response']['medicine']['package_type'];?></td>        
                </tr>
                <tr>
                    <td class="text-primary">Unit Price:</td>
                    <td><?php echo $med['response']['medicine']['unit_price'];?>&nbsp;<i class="fa fa-inr"></i></td>        
                </tr>
                <tr>
                    <td class="text-primary">Unit Qty:</td>
                    <td><?php echo $med['response']['medicine']['unit_qty'];?>&nbsp;<?php echo $med['response']['medicine']['unit_type'];?></td>        
                </tr>                
            </table>

                </div>
            </div>
            <hr size="3"/>
            
            <h4><span class="label label-info"><i class="fa fa-flask"></i> Composition</span></h4>
            <div class="well">
              <?php
              if(!empty($med['response']['constituents'])){                
                foreach($med['response']['constituents'] as $constituent){
                    ?>         
                    <?php echo "<h3 class='label label-default'>".$constituent['name']."</h3>&nbsp;(".$constituent['strength'].") +";
                }
                ?>
            </div>            
            <div class="panel panel-default">
                <div class="panel-heading"><h4><span class="label label-info"><i class="fa fa-flask"></i> Alternatives</span></h4></div>
                <div class="panel-body">
                  <?php foreach($alternatives['response']['medicine_alternatives'] as $alternative):?>
                  <a class="btn btn-link" href="http://localhost/ihealth/meds.php?q=<?php echo urlencode(trim($alternative['brand']));?>&search=Medicine" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $alternative['manufacturer'];?> | <?php echo $alternative['category'];?> | <?php echo $alternative['package_price'];?> Rs."><?php echo $alternative['brand'];?></a> | 
                  <?php endforeach;?>
                </div>
            </div>
                <?php
              }
              ?>            
         <!--Medicines Details-->
            </div>
            
            <div class="col-md-2">
            <?php if(isset($_SESSION['user_id'])):            
            $medId = trim($med['response']['medicine']['generic_id']);
            $mednmae = urlencode($med['response']['medicine']['brand']);
            $manuf = urlencode($med['response']['medicine']['manufacturer']);
            $cat = trim($med['response']['medicine']['category']);
            $packPrice = trim($med['response']['medicine']['package_price']);
               /*
                * check user Favorates Meds Here ==>
                * By using User Id & Med ID
                */
                require 'app/ihealth.php';
                $ihealth = new ihealth;
                $checkFav = $ihealth->userFavMed($medId,$_SESSION['user_id']);
            ?>            
                <a id="anchoeAction" href="javascript:void(0);" name="<?= ($checkFav == "added") ? 'del' : 'add' ;?>" onclick="favMed('<?= $medId;?>','<?= $mednmae;?>','<?= $manuf;?>','<?= $cat;?>','<?= $packPrice;?>',this.name);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php if(!isset($_SESSION['user_id'])){ echo "Please Login First";} elseif ($checkFav == "added") {echo "Remove From Favorite";} else {echo "Add to favorite";}?>">
                <i id="addTofavId" class="text-danger fa <?= ($checkFav == "added") ? 'fa-heart fa-2x' : 'fa-heart-o fa-2x' ;?>"></i>
                </a>            
            <?php endif;?>
            </div>
        </div>
         <?php
      }
    }
    ?>
</div>
<script>
    function suggestions(str) {
       if (str.length == 0) { 
        document.getElementById("meds").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("meds").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "suggestMed.php?med=" + str, true);
        xmlhttp.send();
    } 
    }
    function favMed(id,name,manu,cat,price,action){
       // alert("Id = "+id+"Name = "+name+"manu = "+manu+"cat = "+cat+"Price = "+price+"action = "+action);
        $.ajax({
            url:'ops.php',
            type:'post',
            cache:false,
            data:{'favmed':'doadd','id':id,'name':name,'manu':manu,'cat':cat,'price':price,'Medaction':action},            
            success: function (data){                
                data = $.trim(data);       
            if (data == 'added') {       
            $("#addTofavId").prop('class', 'text-danger fa fa-heart fa-2x');
            $("#anchoeAction").prop('name', 'del');      
                $("#anchoeAction").tooltip('hide')          
               .tooltip('fixTitle')
               .tooltip('hide')
               .attr('data-original-title', 'Remove from favorite');
            }
            if (data == 'deleted') {       
            $("#addTofavId").prop('class', 'text-danger fa fa-heart-o fa-2x');
            $("#anchoeAction").prop('name', 'add');
            $("#anchoeAction").tooltip('hide')          
               .tooltip('fixTitle')
               .tooltip('hide')
               .attr('data-original-title', 'Add to favorite');       
            }
            if (data == 'error') {
              alert('error');
            }
            }            
            });
    }
</script>
<?php include 'app/footer.php';?>
