<?php include 'app/header.php';
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['current_url'] = $current_link;
 $file = "http://www.bodybuilding.com/rss/articles/training";
                      $xml = simplexml_load_file($file);                   
                      $json_encode = json_encode($xml);
                      $trainings = json_decode($json_encode,TRUE);                      
                      if(empty($trainings)){
                        ?>
                        <style>
                            body {
                                background-color: #f6f6f6;
                            }
                        </style>
                           <div class="row">
                                <div class="col-md-4">
                                        
                                </div>
                                
                                <div class="col-md-4">
                              <a href="stayfit.php" style="text-decoration: none;">
                            <div class="panel mini-box">
                                <span class="box-icon bg-info">                                    
                                    <span class="text-info"><i class="fa fa-exclamation-circle fa-2x"></i></span>                                    
                                </span>
                                <div class="box-info">
                                    <p id="stayfitModhead" class="size-h2"><span class="text-danger">Unable To Fetch Feeds</span></p>
                                    <p class="text-muted"><span id="stayfitModpara" data-i18n="New users">possible reasons <i class="fa fa-hand-o-down"></i></span></p>
                                    
                                    <ul>                                        
                                        <li class="list-group-item">
                                            <i class="fa fa-clock-o"></i> Request Timed Out
                                        </li>
                                    </ul>
                                </div>
                            </div>

                              </a>
                                </div>
                                
                                <div class="col-md-4">
                                        
                                </div>
                           </div>
                        <?php exit;
                      }
?>
<style>
    body{
        /*background-image: url("assets/images/bg.jpg");*/
        background-color: #ffffff;
    }
</style>
<!--container--><div class="container-fluid">
<!-- first row--><div class="row">
        <div class="col-md-3">            
            <div class="panel panel-default">
                <div class="panel-heading"><b><img class="img-circle" src="assets/PNG/ex.png" height="40" width="40" alt=""> Training</b></div>
                <div class="panel-body">
                  <!--Training-->                      
                      <?php foreach($trainings['channel']['item'] as $training):?>
                      <a href="<?php echo $training['link'];?>" style="text-decoration: none;"><h4 class="text-success"><i class="fa fa-rss"></i> <?php echo $training['title'];?></h4></a>
                      <p class="text-muted"><?php echo $training['description'];?></p>
                      <hr size="5"/>
                      <?php endforeach;?>
                      <!--Training-->
                </div>
            </div>        
        </div>
        
        <div class="col-md-3">
                         <div class="panel panel-default">
                <div class="panel-heading"><b><img class="img-circle" src="assets/PNG/food.png" height="40" width="40" alt=""> Nutrition</b></div>
                <div class="panel-body">
                  <!--Training-->
                      <?php
                      $file = "http://www.bodybuilding.com/rss/articles/nutrition";
                      $xml = simplexml_load_file($file);
                      $json_encode = json_encode($xml);
                      $trainings = json_decode($json_encode,TRUE);                     
                      ?>
                      <?php foreach($trainings['channel']['item'] as $training):?>
                      <a href="<?php echo $training['link'];?>" style="text-decoration: none;"><h4 class="text-primary"><i class="fa fa-rss"></i> <?php echo $training['title'];?></h4></a>
                      <p class="text-muted"><?php echo $training['description'];?></p>
                      <hr size="5"/>
                      <?php endforeach;?>
                      <!--Training-->
                </div>
            </div>        

        </div>
        
        <div class="col-md-3">
                                      <div class="panel panel-default">
                <div class="panel-heading"><b><img class="img-circle" src="assets/PNG/music.png" height="40" width="40" alt=""> Motivation</b></div>
                <div class="panel-body">
                  <!--Training-->
                      <?php
                      $file = "http://www.bodybuilding.com/rss/articles/mind";
                      $xml = simplexml_load_file($file);
                      $json_encode = json_encode($xml);
                      $trainings = json_decode($json_encode,TRUE);                     
                      ?>
                      <?php foreach($trainings['channel']['item'] as $training):?>
                      <a href="<?php echo $training['link'];?>" style="text-decoration: none;"><h4 class="text-info"><i class="fa fa-rss"></i> <?php echo $training['title'];?></h4></a>
                      <p class="text-muted"><?php echo $training['description'];?></p>
                      <hr size="5"/>
                      <?php endforeach;?>
                      <!--Training-->
                </div>
            </div>        

        </div>
        
        <div class="col-md-3">
             <div class="panel panel-default">
                <div class="panel-heading"><b><img class="img-circle" src="assets/PNG/sports.png" height="40" width="40" alt=""> Sports</b></div>
                <div class="panel-body">
                  <!--Training-->
                      <?php
                      $file = "http://www.bodybuilding.com/rss/articles/sports";
                      $xml = simplexml_load_file($file);
                      $json_encode = json_encode($xml);
                      $trainings = json_decode($json_encode,TRUE);                     
                      ?>
                      <?php foreach($trainings['channel']['item'] as $training):?>
                      <a href="<?php echo $training['link'];?>" style="text-decoration: none;"><h4 class="text-danger"><i class="fa fa-rss"></i> <?php echo $training['title'];?></h4></a>
                      <p class="text-muted"><?php echo $training['description'];?></p>
                      <hr size="5"/>
                      <?php endforeach;?>
                      <!--Training-->
                </div>
            </div>        

        </div>
    </div><!-- first row-->
</div><!--container-->
<?php include 'app/footer.php';?>