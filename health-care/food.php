<?php include 'app/header.php';
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['current_url'] = $current_link;
?>
<div class="container-fluid">
    <div class="row">
  <div class="col-md-2">
  
  </div>
  
  <div class="col-md-8">
  <div class="well">
    <form data-toggle="validator" role="form" action="" method="get">
        <div class="form-group">
    <h4 class="text-primary "><b>Enter Food Name:</b></h4>
    <input type="search" name="q" class="form-control" id="foodname" autofocus="on" autocomplete="off" value="<?php if(!empty($_GET['q'])){echo $_GET['q'];}?>" placeholder="For e.g. cake, icecream, apple..." data-error="Please enter food name" required>
    <div class="help-block with-errors"></div>
            <button id="btnfood" type="submit" class="btn btn-success" onclick="ifood()" name="search" value="FoodsNutrients"><b><i class="fa fa-search" id="ifood"></i> Search</b></button>

  </div>
    </form>
  </div>
  </div>
  
  <div class="col-md-2">
  
  </div>
</div>
    <?php
if(isset($_GET['search']))
{    
    $food_name = trim(strtolower($_GET['q']));
    $bar = urlencode($food_name." food");
    $url = "http://api.wolframalpha.com/v2/query?input={$bar}&appid=APP_ID";
    $xml = simplexml_load_file($url);
    $json = json_encode($xml);
    $food_details = json_decode($json,TRUE);
    //echo "<pre>"; print_r($food_details); die;
    $dataType = $food_details['@attributes']['datatypes'];
    $success = $food_details['@attributes']['success'];
    if (stripos($dataType,"Food") !== false && $success =='true') {
        /* We need only pod key(array key) data to extract the information about the food => $food_details['pod'] */
       //echo"<pre>";
       //print_r($food_details['pod']);
       ?>
       <div class="row">
          <div class="col-md-2">
          
          </div>
          
          <div class="col-md-8">
           
            <h3><span class="label label-default">Search Results:</span></h3>
          <!--search results-->
          <?php foreach($food_details['pod'] as $food):?>
          
          <h5 class="text-primary"><?php echo $food['@attributes']['title'];?>:</h5>          
          <img class="img-responsive" src="<?php echo $food['subpod']['img']['@attributes']['src'];?>" width="<?php echo $food['subpod']['img']['@attributes']['width'];?>" height="<?php echo $food['subpod']['img']['@attributes']['height'];?>">
          <hr size="3"/>
          <?php endforeach;?>
          <!--search result ends-->
            
          </div>
          
          <div class="col-md-2">
          
          </div>
       </div>
       <?php
    }
    else {
        ?>
        <div class="well">
             <h2 class="text-primary"><i class="fa fa-meh-o"></i> No results found</h2>
        </div>
        <?php
    }
}
?>
</div>
<?php include 'app/footer.php';?>
