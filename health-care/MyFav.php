<?php
include ('app/header.php');
require ('app/ihealth.php');
$ihealth = new ihealth;
$uid = (int)$_GET['uid'];
if(empty($_SESSION['user_id']) || empty($uid)){
    header("Location:finder.php");
}
$favos = $ihealth->getMyFav($uid);
//echo "<pre>";
//print_r($favos); die;
?>
<style>
    body{
        background-color: #f6f6f6;
    }
</style>
<div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
      <!--handy Searches-->       
       <div class="panel mini-box">
         <span id="hpNearby" class="box-icon bg-info">
             <img class="img-circle" src="assets/PNG/click.png" height="70" width="70" alt="">
         </span>
         <div class="box-info">
             <p class="size-h2">Handy Search</p>
             <hr>
             <?php
             if(empty($favos['lookupfor'])){
                ?>
                <span>No Favorites Added Yet</span>
                <?php
              } else {
             foreach($favos['lookupfor'] as $handys):
             if (!isset($res[ $handys["favo"] ])) {
                $res[ $handys["favo"] ] = array();       
               }
              $res[ $handys["favo"] ][] = array_slice($handys, 1);
              endforeach;             
              foreach($res as $type => $value):
              ?>
              <br>
              <span class="size-h2 label label-info"><?= $type;?>:</span>
              <br><br>
              <?php
              foreach($value as $handy):              
              $uniqueId = rand(11,1000);
              ?>
              <div id="handyMAtrix<?= $uniqueId;?>">
                <div style="float: right;">
                <a id="handyMAtrix<?= $uniqueId;?>" onclick="delFav('<?= $handy['place_id'];?>',this.id);" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="text-decoration: none;">
                    <i class="fa fa-trash-o fa-lg"></i>
                </a>
              </div>               
            <!--Favorite-->
            <a href="http://localhost/ihealth/finder.php?place_details=<?= $handy['place_id'];?>&searchType=lookupfor&favo=<?= $type;?>" style="text-decoration: none;">
                <div class="panel mini-box">
                <span class="box-icon bg-info">
                    
                </span>
                <div class="box-info">
                    <p class="size-h2"><i class="text-info fa fa-star"></i> <?= urldecode($handy['place_name']);?></p>
                    <p class="text-muted"><span data-i18n="New users"><i class="fa fa-map-marker"></i> <?= urldecode($handy['address']);?></span></p>
                    <?php if(!empty($handy['phone'])):?>
                    <i class="fa fa-phone"></i> <?= urldecode($handy['phone']);?>
                    <?php endif;?>
                    <div class="text-muted" style="float:right;"><?= $ihealth->time_ago($handy['date_added']);?></div>
                </div>                
            </div>
            </a>             
             <!-- //Favorite-->
              <hr>
              </div>
              <?php
              endforeach;
              endforeach;
              }
              ?>
         </div>
       </div>

       <!--handy Searches-->
      </div>
      
      <div class="col-md-6">
       <!--manual searches-->       
      <div class="panel mini-box">
         <span id="hpNearby" class="box-icon bg-info">
             <img class="img-circle" src="assets/PNG/search.png" height="70" width="70" alt="">
         </span>
         <div class="box-info">
             <p class="size-h2">Manual Search</p>
             <hr>
             <?php
              if(empty($favos['q'])){
                ?>
                <span>No Favorites Added Yet</span>
                <?php
              } else{
                foreach($favos['q'] as $handysq):
             if (!isset($resq[ $handysq["favo"] ])) {
                $resq[ $handysq["favo"] ] = array();       
               }
              $resq[ $handysq["favo"] ][] = array_slice($handysq, 1);
              endforeach;              
              foreach($resq as $typeq => $valueq):
              ?>
              <br>
              <span class="size-h2 label label-info"><?= $typeq;?>:</span>
              <br>
              <?php
              foreach($valueq as $handyq):
              $uniqueIdq = rand(110,1000);
              ?>
              <div id="qNinja<?= $uniqueIdq;?>">
              <div style="float: right;">
                <a id="qNinja<?= $uniqueIdq;?>" onclick="delFav('<?= $handyq['place_id'];?>',this.id);" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="text-decoration: none;">
                    <i class="fa fa-trash-o fa-lg"></i>
                </a>
              </div>
            <a href="http://localhost/ihealth/finder.php?place_details=<?= $handyq['place_id'];?>&searchType=lookupfor&favo=<?= $typeq;?>" style="text-decoration: none;">
                <div class="panel mini-box">
                <span class="box-icon bg-info">
                    
                </span>
                <div class="box-info">
                    <p class="size-h2"><i class="text-info fa fa-star"></i> <?= urldecode($handyq['place_name']);?></p>
                    <p class="text-muted"><span data-i18n="New users"><i class="fa fa-map-marker"></i> <?= urldecode($handyq['address']);?></span></p>
                    <?php if(!empty($handyq['phone'])):?>
                    <i class="fa fa-phone"></i> <?= urldecode($handyq['phone']);?>
                    <?php endif;?>
                    <div class="text-muted" style="float:right;"><?= $ihealth->time_ago($handyq['date_added']);?></div>
                </div>
            </div>
            </a>             

              <hr>
              </div>
              <?php
              endforeach;
              endforeach;
              }
              ?>
         </div>
       </div>

       <!--manual searches-->
      </div>     
    </div>
</div>
<script>
   function delFav(placeId,UniqueId) {    
      $.ajax({
      url: 'ops.php',
      cache: false,
      type: 'post',
      data: {'placeId': placeId,'delFav':'HandyMatrix'},     
      success: function(data){        
       data = $.trim(data);      
       if (data == 'deleted') {       
       $("#"+UniqueId).html('');              
       }
       if (data == 'error') {
         alert('error');
       }
      }
   });
   return false;
   }
</script>
<?php include ('app/footer.php');?>