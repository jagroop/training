-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 19, 2016 at 02:11 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.6.16-4+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ihealth`
--

-- --------------------------------------------------------

--
-- Table structure for table `bmi`
--

CREATE TABLE IF NOT EXISTS `bmi` (
  `bmi_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `bmi` decimal(5,1) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`bmi_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bmi`
--

INSERT INTO `bmi` (`bmi_id`, `user_id`, `bmi`, `date`) VALUES
(1, 1, 22.1, '2015-08-28 15:00:14'),
(2, 1, 20.1, '2015-08-29 15:00:35'),
(3, 1, 22.2, '2015-08-31 15:00:56'),
(4, 1, 29.5, '2015-09-01 13:00:27'),
(5, 1, 28.0, '2015-09-03 10:00:55'),
(6, 1, 29.3, '2015-09-03 12:16:33');

-- --------------------------------------------------------

--
-- Table structure for table `bp`
--

CREATE TABLE IF NOT EXISTS `bp` (
  `bp_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `map` varchar(10) NOT NULL,
  `state` varchar(20) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`bp_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `bp`
--

INSERT INTO `bp` (`bp_id`, `user_id`, `map`, `state`, `date`) VALUES
(1, 1, '127', 'High', '2015-04-23 15:37:07'),
(2, 1, '127', 'High', '2015-04-25 14:23:09'),
(3, 1, '100', 'High', '2015-05-03 13:13:19'),
(4, 1, '87', 'Low', '2015-05-20 18:10:23'),
(5, 1, '107', 'High', '2015-06-02 14:01:11'),
(6, 1, '98', 'Normal', '2015-06-05 12:01:43'),
(7, 1, '132', 'High', '2015-07-23 11:05:47'),
(8, 1, '108', 'High', '2015-07-25 17:33:53'),
(9, 1, '103', 'High', '2015-09-01 11:23:01'),
(10, 1, '97', 'Normal', '2015-09-03 11:48:28'),
(11, 1, '97', 'Normal', '2015-09-03 11:50:09');

-- --------------------------------------------------------

--
-- Table structure for table `favo`
--

CREATE TABLE IF NOT EXISTS `favo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(50) NOT NULL,
  `place_name` varchar(80) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `place_id` varchar(100) NOT NULL,
  `search_type` varchar(30) NOT NULL,
  `favo` varchar(30) NOT NULL,
  `date_added` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `favo`
--

INSERT INTO `favo` (`id`, `uid`, `place_name`, `address`, `phone`, `place_id`, `search_type`, `favo`, `date_added`) VALUES
(7, 1, 'Rotary+Blood+Bank', 'Plot+No+4%2C+Dakshin+Marg%2C+Sector+37A%2C+Chandigarh', '0172+464+7737', 'ChIJWQfO5urtDzkRGbvupOZVBJc', 'q', 'blood', '2015-09-02 16:31:17'),
(10, 1, 'Krishna+Medicose', 'Shop+No+25%2C+Sector+27+D%2C+Near+Ramgadiya+Bhawan%2C+Chandigarh', '0172+507+6085', 'ChIJAQAAQB7tDzkRvHYo4b23qHY', 'lookupfor', 'pharmacy', '2015-09-02 16:36:02'),
(13, 1, 'Burn+Gym+%26+Spa', 'SCO+58%2C+Sector+-+11%2C+Panchkula', '0172+500+8801', 'ChIJHRHGy1-TDzkRBT3HNixTCrU', 'lookupfor', 'spa', '2015-09-02 16:38:54'),
(14, 1, 'Sangam+Neuro+Hospital', '%23+619%2C+Sector+18-B%2C+Chandigarh', '0172+270+8877', 'ChIJQ2G2YAXtDzkR5hyfQ6VBYpg', 'q', 'neurologist', '2015-09-02 16:39:29'),
(15, 2, 'Sangam+Neuro+Hospital', '%23+619%2C+Sector+18-B%2C+Chandigarh', '0172+270+8877', 'ChIJQ2G2YAXtDzkR5hyfQ6VBYpg', 'q', 'neurologist', '2015-09-02 16:42:30'),
(22, 1, 'Lyra+Laboratories+Private+Limited', 'Industrial+Area+Phase+I%2C+Chandigarh', '098761+04156', 'ChIJ_____-rsDzkRugSe54mf8lk', 'lookupfor', 'gym', '2015-09-02 16:59:56'),
(23, 1, 'Panchkula+Dog+Clinic', 'Sco+16%2C+Sector-8%2C+Panchkula%2C+Panchkula%2C+Chandigarh', '098142+37626', 'ChIJVVVVFUGTDzkRreHd2StImMg', 'lookupfor', 'veterinary_care', '2015-09-02 17:03:39'),
(24, 1, 'XS+sports+Gallery', 'SCO+101+C+OPP+DPS+SCHOOL%2C+Sector+40B%2C+Chandigarh', '0172+464+7484', 'ChIJ5Yy8CubtDzkRpITXA8zGb_I', 'lookupfor', 'gym', '2015-09-02 17:16:07'),
(34, 1, 'Khane+Khaas', '16%2C+sector+4%2C+Panchkula', '0172+258+2929', 'ChIJ1Xju2mSTDzkRoMGJsMedBGY', 'q', 'indian', '2015-09-03 14:36:39'),
(35, 1, 'Leena+Mogre+Fitness', 'Sco+No.+62-63%2C+Madhya+Marg%2C+Sector+8C%2C+Near+Hotel+Icon%2C+Chandigarh', '0172+507+6491', 'ChIJkUPyjxHtDzkRAJ4IbzAtUEc', 'lookupfor', 'gym', '2015-09-03 14:41:52'),
(37, 1, 'Arora+Medicine+Centre', 'Booth+No+16%2C+Secto+7-c%2C+Sector-26%2C+Sector-26%2C+Chandigarh', '0172+279+4155', 'ChIJ____vyPtDzkRoFGr0r4Wj60', 'lookupfor', 'pharmacy', '2015-09-03 15:28:43'),
(38, 1, 'Phoenix+hospital', 'SCO+8%2CSector+16%2C+Panchkula', '0172+505+4321', 'ChIJAQAAwFQYDTkRAp0B2ct42_A', 'lookupfor', 'hospital', '2015-09-03 15:55:20'),
(41, 1, 'Score', 'S.C.O+177+%26+178%2C+Sector+-+8C%2C+Madhya+Marg%2C+Chandigarh', '0172+466+4004', 'ChIJWxfdgw3tDzkRI_GT7CncPuw', 'lookupfor', 'food', '2015-09-10 09:52:37'),
(42, 1, 'Hair+Raiserz', 'Sector+19%2C+Chandigarh', '0172+465+6583', 'ChIJEXWVKgXtDzkRIAvgdAptpF8', 'lookupfor', 'spa', '2015-09-10 09:54:42'),
(45, 4, 'Score', 'S.C.O+177+%26+178%2C+Sector+-+8C%2C+Madhya+Marg%2C+Chandigarh', '0172+466+4004', 'ChIJWxfdgw3tDzkRI_GT7CncPuw', 'lookupfor', 'food', '2015-09-14 18:07:23'),
(50, 1, 'Takkar+Dental+Clinic', 'House+No.496%2C+Sector-9%2C+Panchkula', '0172+256+4496', 'ChIJHXrNbkOTDzkRyCVuS3ENdeg', 'lookupfor', 'dentist', '2015-09-28 10:29:12');

-- --------------------------------------------------------

--
-- Table structure for table `favo_med`
--

CREATE TABLE IF NOT EXISTS `favo_med` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `med_id` int(12) NOT NULL,
  `med_name` varchar(70) NOT NULL,
  `manuf` varchar(70) NOT NULL,
  `price` varchar(12) NOT NULL,
  `type` varchar(12) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `favo_med`
--

INSERT INTO `favo_med` (`id`, `user_id`, `med_id`, `med_name`, `manuf`, `price`, `type`, `date_added`) VALUES
(1, 1, 64007, 'Disprin+%28350+%26+105+%26+35%29', 'Reckitt+%26+Benckiser+%28India%29+Ltd.', '9', 'Tablet', '2015-09-01 04:08:14'),
(2, 1, 118679, '3G+Mouthwash', 'Vital+Helesys+Pvt+Ltd', '70', 'Mouth Wash/G', '2015-09-02 05:18:12'),
(3, 1, 26617, 'A+quin+%28300+mg%29', 'Allure+Remedies+Pvt+Ltd', '49', 'Tablet', '2015-09-03 06:12:08');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(70) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `id_3` (`id`),
  KEY `id_4` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
