<?php
session_start();
unset($_SESSION['user_id']);
unset($_SESSION['user_name']);
if(isset($_SESSION['current_url'])){
header("Location:".$_SESSION['current_url']);
}
else {
header("Location:index.php");
}