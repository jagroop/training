 <?php          

$a = realpath(dirname(__FILE__));            

require ($a . '/adaptivePayments/vendor/autoload.php');
$payRequest = new PayRequest();


$receiver = array();
$receiver[0] = new Receiver();
$receiver[0]->amount = 1.00; 
$receiver[0]->email = "r1@paypal.com"; // 
//$receiver[0]->primary = "true";

$receiver[1] = new Receiver();
$receiver[1]->amount = 2.00; 
$receiver[1]->email = "r2@paypal.com"; 
$receiver[1]->primary = "true";


$receiverList = new ReceiverList($receiver);

$payRequest->receiverList = $receiverList;

$requestEnvelope = new RequestEnvelope("en_US");
$payRequest->requestEnvelope = $requestEnvelope;
$payRequest->actionType = "PAY";
$payRequest->cancelUrl = "http://localhost/dev/taxi_paypal/cancel_payment.php?failure=true";
$payRequest->returnUrl = "http://localhost/dev/taxi_paypal/paypal_success.php/?success=true";
$payRequest->currencyCode = "USD";
$payRequest->ipnNotificationUrl = "http://replaceIpnUrl.com";

$sdkConfig = array(
"mode" => "sandbox",
"acct1.UserName" => "XXXXXXXXXXX",
"acct1.Password" => "XXXXX",
"acct1.Signature" => "XXXXX",
"acct1.AppId" => "XXXXXX"
);           


$adaptivePaymentsService = new AdaptivePaymentsService($sdkConfig);
$payResponse = $adaptivePaymentsService->Pay($payRequest);
//echo "<pre>";
//print_r($payResponse);		 die;
$appkey = $payResponse->payKey;
$checkstatus = $payResponse->responseEnvelope->ack;
$url = 'https://www.sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=' . $appkey;
//https://www.sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=AP-34G869913F731270D
header("Location:".$url);