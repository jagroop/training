# Slim3 scaffold
Web Version

## dependencies used

- [Slim ^3.0](https://github.com/slimphp/Slim#slim-framework)
- [Slim Twig](https://github.com/slimphp/Twig-View#slim-framework-twig-view)
- [Laravel Eloquent](https://github.com/illuminate/database#illuminate-database)
