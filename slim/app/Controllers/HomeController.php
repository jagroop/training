<?php

namespace App\Controllers;

//use App\Models\User;
use Slim\Views\Twig as View;

class HomeController extends Controller {

	public function index($request, $response) {

		//$this->db->table('users')->get();

		//$user = User::all();

		return $this->view->render($response, 'home.twig');
	}
}
